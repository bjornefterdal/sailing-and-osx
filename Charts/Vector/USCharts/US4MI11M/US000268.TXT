CAUTION:
Due to periodic high water conditions in the Great Lakes, some features charted as visible at Low Water Datum may be submerged, particularly in the near shore areas. Mariners should proceed with caution.

NOTE D
Mariners are warned that numerous uncharted stakes and fishing structures, some submerged, may exist in the area of this chart. Such structures are not charted unless known to be permanent.

NOTE:
Gas pipelines and wells contain natural gas under pressure and damage to these installations would create an immediate fire hazard. Vessels anchoring in Lake Erie should do so with caution after noting the underwater, and therefore concealed, positions of all oil and gas wells, pipelines, submarine cables and other installations.

CAUTION:
Mariners are advised that oil and gas drilling towers are temporarily established in various parts of Lake Erie. These towers exhibit a Quick Flashing White Light and each is equipped with an automatic fog signal sounding one blast of 2 seconds duration followed by 18 seconds of silence.
