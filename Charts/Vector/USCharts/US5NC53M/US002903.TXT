NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NC53M - CAPE HENRY TO PAMLICO SOUND INCLUDING ALBEMARLE SOUND

INDEX:
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
ADMINISTRATION AREA
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
CAUTION - DREDGED AREAS
CAUTION - DUCK BLINDS
CAUTION - RIPRAP
CAUTION - WARNINGS CONCERNING LARGE VESSEL
RULES OF THE ROAD (ABRIDGED)
PUBLIC BOATING INSTRUCTION PROGRAMS
CAUTION - SMALL CRAFTS
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - OREGON INLET
CAUTION - LIMITATIONS
CAUTION - SUBMARINE PIPELINES AND CABLES
MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE
NOAA WEATHER RADIO BROADCASTS
BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS
DUMPING SITES
HURRICANES AND TROPICAL STORMS
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, U.S. Coast Guard, and National Geospatial-Intelligence Agency.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 3&4. Additions or revisions to Chapter 2 are published in the Notices to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 5th Coast Guard District in Portsmouth, Virginia, or at the Office of the District Engineer, Corps of Engineers in Norfolk Wilmington, North Carolina.

Refer to charted regulation section numbers.

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area.  This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well to its bed and subsoil.  For more information, please refer to the Coast Pilot.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 4 for important supplemental information.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION - DUCK BLINDS
Numerous duck blinds, stakes, piles, pipes, and floating fish nets exist in the water area of this chart.

Mariners are warned that numerous areas adjacent to the shoreline are foul with trees. Snags are not charted because they frequently change in position.


CAUTION - RIPRAP
Mariners are warned to stay clear of the protective riprap surrounding navigational light structures.


CAUTION - WARNINGS CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall not impede the passage of a vessel that can navigate only within a narrow channel or fairway. Large vessels may appear to move slowly due to their large size but actually transit at speeds in excess of 12 knots, requiring a great distance in which to maneuver or stop. A large vessel's superstructure may block the wind with the result that sailboats and sailboards may unexpectedly find themselves unable to maneuver. Bow and stem waves can be hazardous to small vessels. Large vessels may not be able to see small craft close to their bows.


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases. Sailing vessels and motorboats less than sixty-five feet in length shall not hamper, in a narrow channel, the safe passage of a vessel which can navigate only inside that channel.

A motorboat being overtaken has the right-of-way. Motorboats approaching head to head or nearly so should pass port to port.

When motorboats approach each other at right angles or obliquely, the boat on the right has the right-of-way in most cases.

Motorboats must keep to the right in narrow channels when safe and practicable.

Mariners are urged to become familiar with the complete text of the Rules of the Road in U.S. Coast Guard publication "Navigation Rules."


PUBLIC BOATING INSTRUCTION PROGRAMS
The United States Power Squadron (USPS) and U.S. Coast Guard Auxiliary (USCGAUX), national organizations of boatmen, conduct extensive boating instruction programs in communities throughout the United States. For information regarding these educational courses, contact the following sources:
USPS - Local Squadron Commander or USPS Headquarters, Post Office Box 30423, Raleigh, N.C. 27612, 919-821-0281.

USCGAUX - 5th Coast Guard District, Federal Building, 431 Crawford St, Portsmouth, VA 23704-5004, 804-393-6486 or USCG Headquarters (G-BAU), Washington D.C. 20593-0001.


CAUTION - Small Craft
Small craft should stay clear of large commercial and government vessels even if small craft have the right-of-way. All craft should avoid areas where the skin divers flag, a red square with a diagonal white stripe, is displayed.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


CAUTION - OREGON INLET
The aids in Oregon Inlet, Oregon Inlet Channel to junction of Old House Channel, and buoys in Walter Slough, Old House Channel and Davis Channel are not charted because they are moved frequently. Consult Local Notice to Mariners, 5th Coast Guard District at http://www.navcen.uscg.gov/Inm/d5/default.htm for the latest positions of aids to navigation. Hydrography in Oregon Inlet is not shown due to its continually shifting nature. The most recent hydrographic survey information, centerline waypoints and a centerline controlling depth are available from the United States Army Corps of Engineers, Wilmington District, at 910-251-4411 and http://www.saw.usace.army.mil/nav. Shoaler depths can be expected off the centerline.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and sub-marine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE
CITY				TELEPHONE NUMBER	OFFICE HOURS
Baltimore, MD/			*(703) 260-0107		24 hours daily
   Washington, DC			
Wakefield, VA			*(757) 899-4200		24 hours daily
Newport, NC			*(252) 223-5737		24 hours daily

*Recorded


NOAA WEATHER RADIO BROADCASTS
CITY			STATION		FREQ.		BROADCAST TIMES
Norfolk, VA		KHB-37		162.55		24 hours daily
Mamie, NC               WWH-26          162.425         24 hours daily
Windsor, NC             WNG-537         162.525         24 hours daily
Cape Hatteras, NC	KIG-77		162.475		24 hours daily
New Bern, NC 		KEC-84		162.40		24 hours daily


BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS
CITY			STATION		FREQ. (kHz)	BROADCAST TIMES - CST		SPECIAL WARNING
Hampton Roads, VA	NMN 80		2670 (A3H)	+8:33am, 9:03pm			On receipt
			(USCG)
Cape Hatteras, NC	NMN 13		2670 (A3H)	+8:03am, 8:33pm			On receipt
			(USCG)

+Broadcast one hour later during Daylight Saving Time.
 Distress calls for small craft are made on 2182 kHz or
 channel 16 (156.80 MHz) VHF.

DUMPING SITES
Regulations for Ocean Dumping Sites are contained in 40 CFR, Parts 220-229. Additional information concerning the regulations and requirements for use of the sites may be obtained from the Environmental Protection Agency (EPA). See U.S. Coast Pilots appendix for addresses of EPA offices. Dumping subsequent to the survey dates may have reduced the depths shown.

CAUTION - HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.

Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.

Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


End of file