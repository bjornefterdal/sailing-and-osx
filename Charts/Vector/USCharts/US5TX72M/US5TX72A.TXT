NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION


US5TX72E - SABINE PASS AND LAKE

INDEX:
CAUTION - DREDGED AREAS
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
SUPPLEMENTAL INFORMATION
WARNING - PRUDENT MARINER
NOTE A
CAUTION - SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
POLLUTION REPORTS 	  	
NOTE E
CAUTION - GAS AND OIL WELL STRUCTURES
CAUTION - HURRICANES AND TROPICAL STORMS
AIDS TO NAVIGATION
AUTHORITIES
INTRACOASTAL WATERWAY
MINERAL DEVELOPMENT STRUCTURES
RADAR REFLECTORS
ADDITIONAL INFORMATION
TIDAL INFORMATION


NOTES:
CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners. 

CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important 
supplemental information.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 	
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 5. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
8th Coast Guard District in New Orleans, LA, or at the office 
of the District Engineer, Corps of Engineers in Galveston, TX. 
Refer to charted regulation section numbers.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.

NOAA WEATHER RADIO BROADCASTS 
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Lake Charles, LA      KHB-42 	 162.400 MHz  
Beaumont, TX 	      WXK-28 	 162.475 MHz

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

NOTE E
It is recommended that vessels transiting the 
Intracoastal Waterway make a S�CURIT� call on 
VFH-FM Channel 13 prior to entering the Sabine-
Neches Canal at the Neches River, Sabine River, 
and Port Arthur Canal.

CAUTION - GAS AND OIL WELL STRUCTURES
Uncharted platforms, gas and oil well struc-
tures, pipes, piles and stakes can exist within 
the limits of this chart.

CAUTION - HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may 
cause considerable damage to marine structures, aids to 
navigation and moored vessels, resulting in submerged debris 
in unknown locations.
Charted soundings, channel depths and shoreline may not 
reflect actual conditions following these storms. Fixed aids to 
navigation may have been damaged or destroyed. Buoys may 
have been moved from their charted positions, damaged, sunk, 
extinguished or otherwise made inoperative. Mariners should 
not rely upon the position or operation of an aid to navigation. 
Wrecks and submerged obstructions may have been displaced 
from charted locations. Pipelines may have become uncovered 
or moved. 
Mariners are urged to exercise extreme caution and are 
requested to report aids to navigation discrepancies and 
hazards to navigation to the nearest United States Coast Guard 
unit.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.

INTRACOASTAL WATERWAY
The project depth is 3.6 Meters/12 feet from New Orleans, 
LA, to Aransas Pass, TX.
The controlling depths are published peri-
odically in the U.S. Coast Guard Local Notice 
to Mariners.

MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals are required for fixed mineral
development structures, subject to approval by the District
Commander, U.S. Coast Guard (33 CFR 67).

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids 
to navigation. Individual radar reflector identification on 
these aids has been omitted from this chart.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

TIDAL INFORMATION
No tidal observations are available for the
area covered by this chart. 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov


END OF FILE
