NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK2GE - POINT ELRINGTON TO CAPE RESURRECTION

INDEX
AUTHORITIES
CAUTION - DREDGED AREAS
WARNING - PRUDENT MARINER
POLLUTION REPORTS
CAUTION � TEMPORARY CHANGES
AIDS TO NAVIGATION
SUPPLEMENTAL INFORMATION
NOAA WEATHER RADIO BROADCASTS
OFFSHORE VESSEL TRAFFIC MANAGEMENT RECOMMENDATIONS
CAUTION - LIMITATIONS
CAUTION - MARINERS
NOTE A
CAUTION - SUBMARINE PIPELINES AND CABLES
ADDITIONAL INFORMATION
TIDAL INFORMATION 



AUTHORITIES
Hydrography and Topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological
Survey and U.S. Coast Guard.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION � TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated.  See 
Local Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation are
replaced by other types or removed. For details see U.S. Coast Guard Light List.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.

NOAA WEATHER RADIO BROADCASTS
The National Weather Service stations listed 
below provide continuous weather broadcasts.
The reception range is typically 20 to 40 miles
from the antenna site, but can be as much as 
100 nautical miles for stations at high elevations.

Rugged I,Ak        WNG-526         162.425 MHZ
Naked I,AK         WNG-530         162.500 MHZ
Point Pigot, AK    KZZ-93          162.450 MHZ
Cape Hinchinbrook  TBD             162.525 MHZ	
Potato point AK    WNG-527         162.425 MHz
Seward, AK         KEC-81	   162.55 MHz
Whittier, AK       KXI-29	   162.40 MHz


OFFSHORE VESSEL TRAFFIC MANAGEMENT RECOMMENDATIONS
Based on the West Coast Offshore Vessel Traffic 
Risk Management Project, which was co-sponsored by 
the Pacific States/British Columbia Oil Spill Task 
Force and U.S. Coast Guard Pacific Area, it is recom-
mended that, where no other traffic management areas 
exist such as Traffic Separation Schemes, Vessel Traffic 
Services, or recommended routes, vessels 300 gross 
tons or larger transiting along the coast anywhere be-
tween Cook Inlet and San Diego should voluntarily stay 
a minimum distance of 25 nautical miles offshore. It is 
also recommended that tank ships laden with persis-
tent petroleum products and transiting along the coast 
between Cook Inlet and San Diego should voluntarily 
stay a minimum distance of 50 nautical miles offshore. 
Vessels transiting short distances between adjacent 
ports should seek routing guidance as needed from the 
local Captain of the Port or VTS authority for that area. 
This recommendation is intended to reduce the 
event of a vessel casualty. 


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.

CAUTION - MARINERS 
Mariners are urged to use caution  when
navigating in the area of this chart due to
possible changes in depths and shorelines as a
result of the earthquake of march 27, 1964.

NOTE A
Navigation regulations are published in
Chapter 2, U.S. Coast Pilot 9. Additions or
revisions to Chapter 2 are published in the
Notice to Mariners. Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska, or at the Office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov


END OF FILE



