NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3AK3BM - CORONATION ISLAND TO LISIANSKI STRAIT:


INDEX:


AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
SUPPLEMENTAL INFORMATION
CAUTION  TEMPORARY CHANGES
WARNING  PRUDENT MARINER
ADDITIONAL INFORMATION
NOTE A
AUTHORITIES
CAUTION  SUBMARINE PIPELINES AND CABLES
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION 
COLREGS
OIL SPILL TASK FORCE
FISHERY LIMIT


NOTES:


AIDS TO NAVIGATION 
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION  USE OF RADIO SIGNALS (LIMITATIONS) 
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


CAUTION  TEMPORARY CHANGES 
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


WARNING  PRUDENT MARINER 
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION 
Additional information can be obtained at www.nauticalcharts.noaa.gov.


NOTE A 
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5.  Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 17th Coast Guard District in Juneau, Alaska, or at the Office of the District Engineer, Corps of Engineers in Anchorage, Alaska. 
Refer to charted regulation section numbers.


AUTHORITIES 
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the U.S. Coast Guard and Geological Survey.


CAUTION  SUBMARINE PIPELINES AND CABLES 
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


RADAR REFLECTORS 
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


NOAA WEATHER RADIO BROADCASTS 
The NOAA Weather Radio Station listed below provides continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Althorp Peak, AK			KZZ-86		162.425 MHz
Mt. Robert Barron, AK		KZZ-87		162.450 MHz
Mt. McArthur, AK			KZZ-95		162.525 MHz
Sukkwan I, AK				KZZ-89		162.425 MHz
Cape Fanshaw, AK			KZZ-88		162.425 MHz
Zarembo I, AK				KZZ-91		162.450 MHz
Craig, AK						KXI-80		162.475 MHz
Juneau, AK					WXJ-25		162.550 MHz
Sitka, AK						WXJ-80		162.550 MHz


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov


COLREGS 
International Regulations for Preventing Collisions at Sea, 1972 
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


OIL SPILL TASK FORCE
The U.S. Coast Guard and the Pacific States/British Columbia Oil Spill Task Force endorse a system of voluntary measures and minimum distances from shore for certain commercial vessels transiting along the coast anywhere between Cook Inlet, Alaska and San Diego, California.  See U.S. Coast Pilot 8, Chapter 3 for details.


FISHERY LIMIT
Fishery limit is the limit of the State of Alaska's fishery management authority (except for crabs) in accordance with Section 306(a) of the Fishery Conservation and Management Act, where that limit is seaward of the territorial sea.


END OF FILE