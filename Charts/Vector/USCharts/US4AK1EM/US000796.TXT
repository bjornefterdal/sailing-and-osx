Oil exploration and production operations are being conducted in the waters of Cook Inlet. 
Drilling vessels and movable and permanent platforms are being used. Only permanent platforms
are charted. Mariners are urged to exercise caution when transiting the area.