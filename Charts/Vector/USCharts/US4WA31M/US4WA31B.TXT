The areas NWR (National Wildlife Refuge) are closed to the public to protect breeding  colonies of seabirds, endangered and threatened species, and marine mammals.

Boaters are requested to stay at least 200 yards away from these islands to avoid disturbance to these animals.