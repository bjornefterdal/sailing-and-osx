NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5ME31E - APPROACHES TO BLUE HILL BAY

INDEX:
AUTHORITIES
POLLUTION REPORTS
CAUTION - LIMITATIONS
SUPPLEMENTAL INFORMATION
NOTE A
AIDS TO NAVIGATION
CAUTION - TEMPORARY CHANGES
CAUTION - SUBMARINE PIPELINES AND CABLES
NOAA VHF-FM WEATHER BROADCASTS
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS

NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the corps of Engineers, and U.S. 
Coast Guard.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117. 
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 1 for important 
supplemental information.


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 1. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
Concord, MA.
Refer to charted regulation section numbers.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service station listed 
below provides continuous marine weather broad-
casts. The range of reception is variable, but for 
most stations is usually 20 to 40 miles from the 
antenna site.

Ellsworth, ME	  KEC-93	162.40 MHz


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these on these aids has been 
omitted from this chart.


END OF FILE