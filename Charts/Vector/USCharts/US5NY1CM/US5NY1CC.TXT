The vertical clearance of the
Verrazano-Narrows Suspension Bridge varies in
height from the center of the span toward the
bridge towers. For the middle 609.6
meters/2000 horizontal feet the vertical
clearance is 60.3 meters/198 feet. At the
center of the span the maximum vertical
clearance is 65.5 meters/215 feet. At the
bridge piers the vertical clearance is 55.8
meters/183 feet.