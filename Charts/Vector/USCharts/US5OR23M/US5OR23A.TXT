NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5OR23M - COLUMBIA RIVER ALDERDALE TO BLALOCK ISLANDS


INDEX:
AIDS TO NAVIGATION
NOTE A
AUTHORITIES
WARNING - PRUDENT MARINER
CAUTION - TEMPORARY CHANGES
CAUTION
RADAR REFLECTORS
NOAA VHF-FM WEATHER BROADCASTS
SUPPLEMENTAL INFORMATION
POLLUTION REPORTS
ADMINISTRATION AREA
TIDAL INFORMATION
ADDITIONAL INFORMATION
SOUNDINGS AND CLEARANCES


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7.  Additions or revisions to Chapter 2 are pub-
lished in the Notices to Mariners.  Information concerning
the regulations may be obtained at the Office of the Commander, 
13th Coast Guard District in Seattle, Washington or at the
Office of the District Engineer, Corps of Engineers in
Seattle, Washington.
Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and topography by the National
Ocean Service, Coast Survey, with additional
data from the Corps of Engineers and U.S
Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated.  See
Local Notice to Mariners.


CAUTION
The depths of water have been determined from conditions
existing prior to the filling of the pool.  Shallower depths than
charted may exist, particularly near the shoreline.
No soundings are available in areas depicted by depth curves,
except in isolated cases.


RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation.  Individual radar
reflector identification on these aids has been
omitted from this chart.


NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service station listed
below provides continuous marine weather broad-
casts.  The range of reception is variable, but for
most stations is usually 20 to 40 miles from the
antenna site.
Pendleton, OR		WXL-95		162.550 MHz


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important
supplemental information.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an
Administration Area. This area covers land, internal waters, and
territorial sea. The territorial sea is a maritime zone which the United
States exercises sovereignty extending to the airspace as well as to its
bed and subsoil. For more information, please refer to the Coast Pilot.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

SOUNDINGS AND CLEARANCES
Soundings and clearances of bridges and overhead cables between John Jay Dam
and McNAry Dam in Lake Umatilla refer to normal pool elevation which is 265
feet above mean sea level.


END OF FILE