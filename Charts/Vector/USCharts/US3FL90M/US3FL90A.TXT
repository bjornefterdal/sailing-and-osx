NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3FL90M - FLORIDA KEYS SOMBRERO KEY TO DRY TORTUGAS

INDEX:
AIDS TO NAVIGATION
AUTHORITIES
WARNING - PRUDENT MARINER
NOTE A
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
CAUTION - SUBMARINE PIPELINES AND CABLES
POLLUTION REPORTS
NOAA WEATHER RADIO BROADCASTS
COLREGS. 80.740
HURRICANES AND TROPICAL STORMS
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.



AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the U.S. Coast Guard, and National Geospatial-Intelligence Agency.
 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilots 4 & 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 7th Coast Guard District in Miami, Florida, or at the Office of the District Engineer, Corps of Engineers in Jacksonville, Florida.
Refer to charted regulation section numbers.



CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.  

CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.  Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging or trawling.  Covered wells may be marked by lighted or unlighted buoys.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


NNOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts.  The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.
Teatable Key, FL	WWG-60		162.450 MHz
Key West, FL	        WXJ-95		162.400 MHz


COLREGS, 80.740
International Regulations for Preventing Collisions at Sea, 1972.  
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.  
Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation.
Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved. 
Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit. 


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.art.


TIDAL INFORMATION
For tidal information see NOS tide table publication or go to http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE
