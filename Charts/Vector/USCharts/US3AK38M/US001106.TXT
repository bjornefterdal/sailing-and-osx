Significant shoaling has been found within one-quarter nautical mile
of the glaciers at the head of Disenchantment Bay as presently charted.
Mariners are urged to navigate with extreme caution as some depths 
found are up to 20 fathoms/120 feet shoaler than charted and will continue to
change in the future.