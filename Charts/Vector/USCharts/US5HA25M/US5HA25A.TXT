NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5HA25E - MAALAEA BAY

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
NOTE A
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION
NOAA WEATHER RADIO BROADCASTS
COLREGS, 80.1410
RADAR REFLECTORS
ADMINSTRATION AREA


SYMBOLS AND ABBREVIATIONS: For Symbols and Abbreviations see Chart no. 1.

AUTHORITIES: Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the U.S. Coast Guard.

NOTE A: Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office of the District Engineer, Corps of Engineers in Honolulu, Hawaii.
Refer to Code of Federal Regulations section numbers. 

WARNING � PRUDENT MARINER: The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS: Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: Consult U.S. Coast Pilot 7 for important supplemental information.

CAUTION: Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.

CAUTION: Limitations on the use of certain other radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.

NOAA WEATHER RADIO BROADCASTS: The National Weather Service station listed below provides continuous weather broadcasts.  The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Mt Haleakala, HI		KBA-99		162.40 MHz

COLREGS, 80.1410: International Regulations for Preventing Collisions at Sea, 1972.
The entire area falls seaward of the COLREGS Demarcation Line.

RADAR REFLECTORS: Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector identification on these aids has been omitted.

ADMINSTRATION AREA: The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.



END OF FILE