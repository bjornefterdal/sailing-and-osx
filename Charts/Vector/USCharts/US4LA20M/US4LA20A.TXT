NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4LA20M - MORGAN CITY TO PORT ALLEN

INDEX:
NOTE A
ROUTE ABBREVIATIONS
SUPPLEMENTAL INFORMATION
AUTHORITIES
POLLUTION REPORTS
AIDS TO NAVIGATION
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES and CABLES
CAUTION - HURRICANES AND TROPICAL STORMS
RADAR REFLECTORS
NOTE
NOAA WEATHER RADIO BROADCASTS
MARINE WEATHER FORECASTS NATIONAL WEATHER SERVICE
BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS
WARNING - PRUDENT MARINER
CAUTION - WARNING CONCERNING LARGE VESSELS
CAUTION - LIMITATION
CAUTION - SMALL CRAFT
INTRACOASTAL WATERWAY
MINERAL DEVELOPMENT STRUCTURES
CAUTION
RULES OF THE ROAD (ABRIDGED)
PUBLIC BOATING INSTRUCTION PROGRAMS
TIDAL INFORMATION
ADDITIONAL INFORMATION
ADMINISTRATION AREA

NOTES:
NOTE A
Navigation regulations are published in Chapter 2. U.S.
Coast Pilot 5. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners, Information concerning the
regulations may be obtained at the Office of the Commander,
8th Coast Guard District in New Orleans, LA or at the Office
of the District Engineer, Corps of Engineers in New Orleans,
LA.
Refer to charted regulation section numbers. 


ROUTE ABBREVIATIONS
(IW) Intracoastal Waterway, Carrabelle, FL to Brownsville, TX
(AR) Atchafalaya River route
(MP) Morgan City to Port Allen, alternate route
(LR) Morgan City to Port Allen, landside route


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important
supplemental information.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the
National Response Center via 1-800-424-8802 (toll free), or
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


CAUTION - TEMPORARY 
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


CAUTION - DREDGED AREAS
Improved channels are
subject to shoaling, particularly at the edges.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.


CAUTION - HURRICANES AND TROPICAL STORMS
Hurricanes, and tropical storms and other major storms may cause
considerable damage to marine structures, aids to navigation and moored
vessels, resulting in submerged debris in unknown locations.  
Charted soundings, channel depths and shoreline may not reflect actual
conditions following these storms. Fixed aids to navigation may have been
damaged or destroyed. Buoys may have been moved from their charted
positions, damaged, sunk, extinguished or otherwise made inoperative.
Mariners should not rely upon the position or operation of an aid to
navigation. Wrecks and submerged obstructions may have been displaced
from charted locations. Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to
report aids to navigation discrepancies and hazards to navigation to the
nearest United States Coast Guard unit.


RADAR REFLECTORS
Radar Reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.


NOTE
The Berwick Lock monitors VHF-FM
Channel 12 and operates from 0600-
2200 daily during flood season. The
gates are open the remainder of the
year.


NOAA WEATHER RADIO BROADCASTS
CITY		      STATION      FREQ. (MHz)	      BROADCAST TIMES
New Orleans, LA       KHB-43       162.55 		24 hours daily
Baton Rouge, LA       KHB-46       162.40 		24 hours daily
Morgan City, LA       KIH-23       162.475 		24 hours daily


MARINE WEATHER FORECASTS 
NATIONAL WEATHER SERVICE    TELEPHONE NUMBER            OFFICE HOURS
New Orleans, LA             *(504) 522-7330 		8:00AM-4:00PM M - F
Lake Charles, LA 	    *(337) 477-5285  		24 hours daily
*Recorded


BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS
 BY MARINE RADIOTELEPHONE STATIONS
CITY			STATION		FREQ.			BROADCAST TIMES-CST	     SPECIAL WARNING
New Orleans, LA		NMG		2670 kHz		4:35, 6:35, 10:35 & 11:50AM  On receipt
		       (USCG)					4:35 & 11:50PM 
					157.1 MHz		4:50 & 10:50AM 4:50PM         On receipt
Grand Isle, LA		NMG-15		157.1 MHz		4:35 & 10:35AM 4:35PM        On receipt
Berwick, LA		NMG-37		157.1 MHz		4:00 & 10:00AM 4:00PM        On receipt

Distress calls for small craft are made on 2182 KHz or 
channel 16(156.80 MHz) VHF.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.


CAUTION - WARNING CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall
not impede the passage of a vessel that can navigate only
within a narrow channel or fairway. Large vessels may
appear to move slowly due to their large size but actually
transit at speeds in excess of 12 knots, requiring a great
distance in which to maneuver or stop. A large vessel's
superstructure may block the wind with the result that
sailboats and sailboards may unexpectedly find themselves
unable to maneuver. Bow and stern waves can be hazardous
to small vessels. Large vessels may not be able to see small
craft close to their bows.


CAUTION - LIMITATION
Limitation on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


CAUTION - SMALL CRAFT
Small craft should stay clear of large com-
mercial and government vessels even if small
craft have the right-of-way.


INTRACOASTAL WATERWAY
The U.S. Aids to Navigation System is de-
signed for use with nautical charts, and the exact
meaning of an aid to navigation may not be clear
unless the appropriate chart is consulted.
Aids to navigation marking the Intracoastal
Waterway exhibit unique yellow symbols to
distinguish them from aids marking other water-
ways.
When following the Intracoastal Waterway
westward from Carrabelle, FL to Brownsville, TX,
aids with yellow triangles should be kept on the
starboard side of the vessel and aids with yellow
squares should be kept on the port side of the
vessel.
A horizontal yellow band provides no lateral
information, but simply identifies aids to navi-
gation as marking the Intracoastal Waterway.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals
are required for fixed mineral development
structures shown, subject to ap-
proval by the District Commander, U.S.Coast
Guard (33 CFR 67).


CAUTION
This chart has been corrected from the Notice to Mariners (NM) published
weekly by the National Geospatial-Intelligence Agency and the Local Notice to
Mariners (LNM) issued periodically by each U.S. Coast Guard district to the
dates shown in the lower left hand corner. Chart updates corrected from Notice to
Mariners published after the dates shown in the lower left hand corner are available at
nauticalcharts.noaa.gov.


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases.
Sailing vessels and motorboats less than sixty-five feet in
length shall not hamper, in a narrow channel, the safe
passage of a vessel which can navigate only inside that
channel.
A motorboat being overtaken has the right-of-way.
Motorboats approaching head to head or nearly so should
pass port to port.
When motorboats approach each other at right angles or
obliquely, the boat on the right has the right-of-way in most
cases.
Motorboats must keep to the right in narrow channels when
safe and practicable.
Mariners are urged to become familiar with the complete text
of the Rules of the Road in U.S. Coast Guard publication
"Navigation Rules."


PUBLIC BOATING INSTRUCTION PROGRAMS
The United States Power Squadrons (USPS) and U.S. Coast Guard Auxiliary
(USCGAUX), national organizations of boatmen, conduct extensive boating in-
struction programs in communities throughout the United States. For information
regarding these educational courses contact the following sources:
USPS - Local Squadron Commander or USPS Headquarters, 1504 Blue Ridge
Road, Raleigh, NC 27607, 888-367-8777
USCGAUX - COMMANDER (OAX) Eighth Coast Guard District. Hale Boggs
Federal Building, Suite 1126, 500 Poydras Street, New Orleans, LA 70130,
800-524-8835 or USCG Headquarters, office of the chief Director (G-OCX), 2100
Second Street, SW, Washington, DC 20593.


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to
http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.
 
ADMINISTRATION AREA
This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone over which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.


END OF FILE