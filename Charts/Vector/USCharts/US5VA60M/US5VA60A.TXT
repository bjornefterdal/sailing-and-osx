NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5VA60M - YORK RIVER YORKTOWN AND VICINITY

INDEX:
NOTE A
WARNING - PRUDENT MARINER
CAUTION - SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
CAUTION � TEMPORARY CHANGES
CAUTION - DREDGED AREAS
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
POLLUTION REPORTS
AUTHORITIES
RADAR REFLECTORS
ADDITIONAL INFORMATION
TIDAL INFORMATION
ADMINISTRATION AREA


NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 3. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
5th Coast Guard District in Portsmouth, Virginia or at the 
Office of the District Engineer, Corps of Engineers in 
Norfolk, Virginia.
Refer to charted regulation section numbers.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40
nautical miles from the antenna Site, but can be
as much as 100 nautical miles for stations at
high elevations.

Norfolk, VA       	KHB-37       162.550 MHz
Heathsville, VA         WXM-57       162.400 MHz


CAUTION � TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 3 for important 
supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.

ADMINISTRATION AREA
This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone over which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.

END OF FILE