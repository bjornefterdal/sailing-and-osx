NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK36M - PORT FREDERICK - HOONAH HARBOR AND VICINITY; INIAN COVE; ELFIN COVE

INDEX:
AIDS TO NAVIGATION
CAUTION - TEMPORARY CHANGES
NOTE A
CAUTION - LIMITATIONS
POLLUTION REPORTS
VESSEL TRANSITING
NOAA WEATHER RADIO BROADCASTS
SUPPLEMENTAL INFORMATION
ADMINISTRATION AREA
CAUTION - SHOALING
COLREGS, 80.1705 (see note A)
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners.


NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 8. Additions or 
revisions to Chapter 2 are published in the 
Notice to Mariners. Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District 
Engineer, Corps of Engineers in Anchorage, 
Alaska.
Refer to charted regulation section numbers.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


VESSEL TRANSITING
The U.S. Coast Guard and the Pacific States/British Columbia Oil Spill 
Task Force endorce a system of voluntary measures and minimum 
distances from shore for certain commercial vessels transiting along 
the coast anywhere between Cook Inlet, Alaska and San Diego, 
California. See U.S. Coast Pilots 8 and 9, Chapter 3 for details.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Althorp Peak, AK	KZZ-86		162.425 MHz
Mt. Robert Barron, AK	KZZ-87		162.450 MHz
Juneau, AK		WXJ-25		162.550 MHz


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 8 for important 
supplemental information.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. 
This area covers land, internal waters, and territorial sea.  The territorial sea is 
a maritime zone which the United States exercises sovereignty extending to the 
airspace as well as to its bed and subsoil.  For more information, please 
refer to the Coast Pilot.


CAUTION - SHOALING
Shoalings amounting to as much as 1.8 meters/6 feet have been 
disclosed in several critical shoal areas.  It is 
probable that the Alaska Earthquake of July 10, 1958 
created these shoalings and others not yet discovered.  
Mariners are urged to use caution when navigating over or 
near critical depths.


COLREGS, 80.1705 (see note A) 
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGES Demarcation Line.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the U.S. Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids 
to navigation. Individual radar reflector identification on 
these aids has been omitted from this chart.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE