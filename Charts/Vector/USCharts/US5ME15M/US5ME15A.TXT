NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5ME15M - KENNEBEC AND SHEEPSCOT RIVER ENTRANCES

INDEX:
POLLUTION REPORTS
CAUTION - TEMPORARY CHANGES
AIDS TO NAVIGATION
NOAA WEATHER RADIO BROADCASTS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE A
AUTHORITIES
WARNING - PRUDENT MARINER
RECOMMENDED VESSEL ROUTE
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION
Administration Area

NOTES:
POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.  During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed. For details see U.S. Coast Guard Light List.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

NOAA WEATHER RADIO BROADCASTS
The NOAA WEATHER RADIO stations listed below provide continuous weather broadcasts.  The reception range is typically 20 to 40
nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Dresden, ME.    	WZM-60		162.475 MHz
Portland, ME.   	KDO-95		162.550MHz

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where
pipelines and cables may exist, and when anchoring, dragging, or trawling.
Covered wells may be marked by lighted or unlighted buoys.

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 1. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 1st Coast Guard District in Boston, MA or at the Office of the District Engineer, Corps of Engineers in Concord, MA.
Refer to charted regulation section numbers.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

Administration Area
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil.  For more information, please refer to the Coast Pilot.


END OF FILE
