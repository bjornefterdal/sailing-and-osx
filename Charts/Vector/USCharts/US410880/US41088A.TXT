NATIONAL GEOSPATIAL-INTELLIGENCE AGENCY
US410880 - HAITI - APPROACH TO PORT-AU-PRINCE
 
INDEX:
GLOSSARY
SURVEY NOTE
NOTE
CAUTION 1
CAUTION 2
CAUTION 3

NOTES: 
GLOSSARY:
Anse ... cove
Baie ... bay, cove
Canal ... marine channel
Caye ... island
Cite ... section of populated place
Ile ... island
Ilot ... island, rock in water
Lagon ... lake bed
Morne ... hill, mountain
Pointe ... point
Rade ... roadstead, anchorage
Ravine ... stream
Recifs ... reefs
Riviere ... stream

SURVEY NOTE:
Reconnaissance surveys were conducted in January, 2010 to evaluate earthquake impact on Port-au-Prince harbor. These surveys should be considered preliminary in nature. Surveyed areas may be subject to change as recovery efforts progress. Mariners should contact local authorities for the latest information.

NOTE:
Many areas of Port-au-Prince are under reclamation due to the January 2010 earthquake. These projects are ongoing. Not all areas are marked in this cell. Other areas have extensive land reclamation works and berthing improvements reported (2007). Consult local authorities for additional information.

CAUTION 1:
Lights in Haiti have been reported extinguished or unreliable and buoys are frequently out of position.

CAUTION 2:
Detailed information has been omitted or generalized in areas covered by larger scale cell. In such areas only the principal aids to navigation are maintained in this cell by Notice to Mariners.

CAUTION 3:
Due to the earthquake of January, 2010, navaids and landmarks may be unreliable. There may be floating debris, oil spills, and underwater obstructions that may not be shown in this cell. Not all ruins and dangers are marked or known. The mariner must use extreme caution when navigating in the area of this cell.

END OF FILE
