NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5OH21M - LORAIN HARBOR, OHIO


INDEX:
PLANE OF REFERENCE OF THIS CHART
BRIDGE AND OVERHEAD CABLE CLEARANCES
NOTE - A
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
RADAR REFLECTORS
CAUTION - TEMPORARY
CAUTION - LIMITATIONS
WARNING - PRUDENT MARINER
CAUTION - PERIODIC HIGH WATER CONDITIONS
CAUTION - POTABLE WATER INTAKE
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
SAILING DIRECTIONS
POLLUTION REPORTS
NOAA VHF-FM WEATHER BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION



PLANE OF REFERENCE OF THIS CHART
(Low Water Datum) Depths are referred to the sloping surface of the river when
Lake Erie is at elevation 569.2 feet.
Referred to mean water level at Rimouski, Quebec, International Great Lakes Datum (1985).


BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water Datum, bridge and overhead clearances are
reduced correspondingly.
For clearances see U.S. Coast Pilot 6.


NOTE - A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.
Additions or revisions to Chapter 2 are published in the Notice to Mariners.
Information concerning the regulations may be obtained at the Office of the
Commander, 9th Coast Guard District in Cleveland, Ohio, or at the office of
the District Engineer, Corps of Engineers in Buffalo, New York.
Refer to charted regulation section numbers.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids 
to navigation.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation.
Individual radar reflector identification on these aids has been omitted from 
this chart.


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated on this chart.
See Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation 
are replaced by other types or removed.
For details see  U.S. Coast Guard Light List.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be 
found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence 
Agency (NGA) Publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject 
to error and should be used with caution.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation,
particularly on floating aids.
See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.


CAUTION - PERIODIC HIGH WATER CONDITIONS
Due to periodic high water conditions in the Great Lakes, some features charted as 
visible at Low Water Datum may be submerged, particularly in the near shore areas.  
Mariners should proceed with caution.


CAUTION - POTABLE WATER INTAKE
Vessels operating in fresh water lakes or rivers shall not discharge sewage, or 
ballast, or blige water within such areas adjacent to domestic water intakes as 
are disgnated by the Commissioner of Food and Drugs (21 CFR 1250.93).
Consult U.S. Coast Pilot 6 for important supplement information.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within 
the area of this chart. Not all submarine pipelines and submarine cables are 
required to be buried, and those that were originally buried may have become 
exposed. Mariners should use extreme caution when operating vessels in depths 
of water comparable to their draft in areas where pipelines and cables may exist, 
and when anchoring, dragging or trawling.
Covered wells may be marked by lighted or unlighted buoys.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with 
additional data from the Corps of Engineers, Geological survey, and U.S. Coast 
Guard.


SAILING DIRECTIONS
Bearings of sailing courses are true and distances given thereon are in statute 
miles between points of departure.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response 
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard 
facility if telephone communication is impossible (33 CFR 153).


NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service station listed below provides continuous marine 
weather broadcasts. The range of reception is variable, but for most stations 
is usually 20 to 40 miles from the antenna site.

Cleveland, OH           KHB-59          162.550 MHz
Sandusky, OH            KHB-97          162.400 MHz


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE
