Shoaling amounting to as much as 1.8 meters/6 feet has 
been disclosed in several critical shoal areas,
from Cross Sd. to Excursion Inlet. It is probable
that the Alaska Earthquake of July 10, 1958
created these shoalings and others not yet
discovered. Mariners are urged to use caution
when navigating over or near critical depths.