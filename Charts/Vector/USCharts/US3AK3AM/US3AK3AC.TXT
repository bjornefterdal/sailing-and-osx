The mud flat in front of Taku Glacier is expanding rapidly
to the southwest. 1997 survey data reveal that extensive
shoaling has occurred from Jaw Point to Davidson Point
and is expected to continue. Passage through this area
should not be attempted without local knowledge.
