The U.S. Coast Guard has established an alternate route for vessels 
transiting between the Intracoastal Waterway and the Houston Ship Channel. 
The alternate route, shown in green tint, is marked with aids to navigation 
from Bolivar Peninsula Buoy 20 to Houston Ship Channel Light 28. This route 
is intended to be one-way for vessels proceeding northbound from the 
Intracoastal Waterway to the Houston Ship Channel. The alternate route is not 
regularly maintained and has no associated project depth. Mariners should 
proceed with caution. Southbound traffic is requested to proceed south to 
Houston Ship Channel Buoy 26, then east to Bolivar Point. Houston Traffic 
requests that all vessels proceeding northbound in the alternate route conduct a 
securite broadcast of their intentions prior to entering into the Houston Ship 
Channel. 

