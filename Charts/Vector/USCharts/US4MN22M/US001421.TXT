SAILING DERECTIONS: Bearings of sailing courses are true and distances given thereon 
are in statute miles between points of departure.
Sailing courses and limits indicated in magenta are recommended by the Lake Carriers 
Association and the Canadian Shipowners Association.
