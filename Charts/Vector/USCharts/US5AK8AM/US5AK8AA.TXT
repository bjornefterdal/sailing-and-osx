NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK8AM - SAINT GEORGE ISLAND:


INDEX:


AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
SUPPLEMENTAL INFORMATION
CAUTION  TEMPORARY CHANGES
WARNING  PRUDENT MARINER
ADDITIONAL INFORMATION
NOTE A
AUTHORITIES
RADAR REFLECTORS
TIDAL INFORMATION
COLREGS


NOTES:


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important supplemental information.


CAUTION  TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


WARNING  PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids.  See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 9.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 17th Coast Guard District in Juneau, Alaska, or at the Office of the District Engineer, Corps of Engineers in Anchorage, Alaska.
Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, and U.S. Coast Guard.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


COLREGS
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


END OF FILE