NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3CA69E - POINT DUME TO PURISIMA POINT


INDEX:
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
POLLUTION REPORTS
WARNING - PRUDENT MARINER
CAUTION - TEMPORARY CHANGES
RADAR REFLECTORS
CAUTION - LIMITATIONS
NOAA WEATHER RADIO BROADCASTS
MINERAL DEVELOPMENT STRUCTURES
CAUTION-SUBMARINE PIPELINES AND CABLES
SUBMERGED SUBMARINE OPERATIONS
ADDITIONAL INFORMATION
TIDAL INFORMATION


NOTES:
AUTHORITIES 
Hydrography and topography by the National
Ocean Service, Coast Survey with additional
data from the Geological Survey and U.S. Coast
Guard.


AIDS TO NAVIGATION 
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


NOTE A 
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7.  Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners.  Information concerning the
regulations may be obtained at the Office of the Commander,
11th Coast Guard District in Alameda, California or at the
Office of the District Engineer, Corps of Engineers in
Los Angeles, California.
  Refer to charted regulation section numbers.


POLLUTION REPORTS 
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR
153).


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids
to navigation.  Individual radar reflector identification on
these aids has been omitted from this chart.


CAUTION - LIMITATIONS 
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

San Luis Obispo, CA		KIH-31	     162.550 MHz
Santa Barbara, CA		KIH-34	     162.400 MHz
Santa Barbara Marine, CA 	WWF-62	     162.475 MHz


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals are required for fixed mineral
development structures, subject to approval by the District
Commander, U.S. Coast Guard (33 CFR 67).


CAUTION-SUBMARINE PIPELINES AND CABLES 
Additional uncharted submarine pipelines and
submarine cables may exist within the area of.  
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed.  Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.


SUBMERGED SUBMARINE OPERATIONS
Submerged submarine operations are
conducted at various times in the waters contained.
Proceed with caution.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


END OF FILE