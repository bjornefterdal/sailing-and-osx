The area within a 4-mile radius of Limetree 
Bay Channel Entrance Lighted Buoy 2 is constantly 
congested with very large tank vessels. All vessels 
are advised to avoid loaded tank vessels and use 
extreme caution in and near this 4-mile area.
See U.S. Coast Pilot 5 for additional information.
