NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5GA09M - SAINT ANDREW SOUND AND SATILLA RIVER

INDEX:
NOTE A
AUTHORITIES
ADMINISTRATION AREA
AIDS TO NAVIGATION
WARNING - PRUDENT MARINER
NOAA WEATHER RADIO BROADCASTS
CAUTION - TEMPORARY CHANGES
CAUTION - DREDGED AREAS
POLLUTION REPORTS
CAUTION - SUBMARINE PIPELINES AND CABLES
HURRICANES AND TROPICAL STORMS
RADAR REFLECTORS
CAUTION - LIMITATIONS
TIDAL INFORMATION
ADDITIONAL INFORMATION
SUPPLEMENTAL INFORMATION

NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 4. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 7th Coast Guard District in Miami, Florida, or at the Office of the District Engineer, Corps of Engineers in Savannah, Georgia.

Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, U.S. Coast Guard, and National Geospatial-Intelligence Agency.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area.  This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well to its bed and subsoil.  For more information, please refer to the Coast Pilot.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.



WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.



NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts.  The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Jacksonville, FL  KHB-39    162.550 MHz
Waycross, GA	  WXK-75    162.475 MHz
Brunswick, GA	  WWH-39    162.425 MHz



CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling.  Covered wells may be marked by lighted or unlighted buoys.

 

HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.

Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation.  Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.

Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.

Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 4 for important supplemental information.



END OF FILE