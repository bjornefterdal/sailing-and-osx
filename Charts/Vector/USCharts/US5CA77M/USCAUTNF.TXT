Boat Basin Restricted Area - This area is reserved exclusively for use by vessels 
owned or operated by the Federal Government. Permission to enter the area must be 
obtained from the  Commanding General, U.S. Marine Corps Base, Camp Pendleton, 
California, or by such agencies as he may designate.

