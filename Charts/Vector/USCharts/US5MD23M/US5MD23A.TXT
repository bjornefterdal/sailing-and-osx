NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5MD23E - HONGA, NANTICOKE, WICOMICO RIVERS AND FISHING BAY

INDEX:
AUTHORITIES	
AIDS TO NAVIGATION
NOTE A	
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
CAUTION - MARINERS
CAUTION - FISH TRAP AREAS AND STRUCTURES
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - BASCULE BRIDGE CLEARANCES
SMALL CRAFT WARNINGS
CAUTION - DREDGED AREAS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 3. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
5th Coast Guard District in Portsmouth, Virginia or at the 
Office of the District Engineer, Corps of Engineers in 
Baltimore, Maryland.
Refer to Charted Regulations section numbers. 


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 3 for important 
supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - MARINERS
Mariners are warned to stay clear 
of the protective riprap surrounding 
navigational light structures.


CAUTION - FISH TRAP AREAS AND STRUCTURES
Mariners are warned that numerous uncharted duck blinds and 
fishing structures, some submerged, may exist in the fish trap areas.
Such structures are not charted unless known to be permanent.
Regulations to assure clear passage to and through dredged and 
natural channels, and to established landings, are prescribed by the 
Corps of Engineers in the Code of Federal Regulations.
Definite limits of fish trap areas have been established in some 
areas.
Where definite limits have not been prescribed, the location of 
fishing structures is restricted only by the regulations.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts.
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations 
at high elevations.

Salisbury, MD	 KEC-92		162.475 MHz
Heathsville, VA	 WXM-57		162.40 MHz
Lewes, DE	 WXJ-94		162.55 MHz
 
  	 	
CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.

CAUTION - BASCULE BRIDGE CLEARANCES
For bascule bridges, whose spans do not open to a 
full upright or vertical position, unlimited clearance 
is not available for the entire charted horizontal clearance.

SMALL CRAFT WARNINGS
During the boating season small-craft 
warnings will be displayed from sunrise to 
sunset on Maryland Marine Police Cruisers 
while underway in Maryland waters of the 
Chesapeake Bay and tributaries.


CAUTION - DREDGED AREAS
Improved channels are
subject to shoaling, particularly at the edges.


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.
 

END OF FILE