NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5MI52E - WAUGOSHANCE POINT TO SEUL CHOIX POINT


INDEX:
NOTE A
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
CAUTION � TEMPORARY CHANGES
CAUTION - LIMITATIONS
WARNING - PRUDENT MARINER
NOTE D
NOTE Z
CAUTION - LOW WATER DATUM
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES AND CABLES
SAILING DIRECTIONS
AUTHORITIES
POLLUTION REPORTS
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION
PLANE OF REFERENCE

NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.
Additions or revisions to Chapter 2 are published in the Notices to Mariners.
Information concerning the regulations may be obtained at the Office of the
Commander, 9th Coast Guard District in Cleveland, Ohio, or at the Ofice of the
District Engineer, Corps of Engineers in Detroit, Michigan.
Refer to charted regulation section numbers.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning
aids to navigation.



CAUTION � TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated.
See Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation 
are replaced by other types or removed.
For details see  U.S. Coast Guard Light List.


CAUTION - LIMITATIONS
Limitations on the use of certain other radio signals as aids to marine navigation can be 
found in the U.S. Coast Guard Light Lists and National Imagery and Mapping Agency Publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject 
to error and should be used with caution.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation,
particularly on floating aids.
See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.


NOTE D
Mariners are warned that numerous uncharted stakes and fishing structures, some 
submerged, may exist in the area of this chart. Such structures are not charted 
unless known to be permanent.

NOTE Z
Michigan waters of Lakes Michigan, Huron, Superior, Erie and St. Clair, all waterways connected thereto, and all inland lakes are designated as a No-Discharge Zone (NDZ). This chart falls entirely within the limits of a No-Discharge Zone (NDZ). Under the Clean Water Act, Section 312, all vessels operating within a No-Discharging Zone (NDZ) are completely prohibited from discharging any sewage, treated or untreated, into the waters. Commercial vessel sewage shall include graywater. All vessels with an installed marine sanitation device (MSD) that are navigating, moored, anchored, or docked within a NDZ must have the MSD disabled to prevent the overboard discharge of sewage (treated or untreated) or install a holding tank. Regulations for the NDZ are contained in the U.S. Coast Pilot. Additional information concerning the regulations and requirements may be obtained from the Environmental Protection Agency (EPA) web site: http://www.epa.gov/owow/oceans/vessel_sewage/vsdnozone.html.  


CAUTION - LOW WATER DATUM
Due to periodic high water conditions in the Great Lakes, some features charted as 
visible at Low Water Datum may be submerged, particularly in the near shore areas.  
Mariners should proceed with caution.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within 
the area of this chart. Not all submarine pipelines and submarine cables are 
required to be buried, and those that were originally buried may have become 
exposed. Mariners should use extreme caution when operating vessels in depths 
of water comparable to their draft in areas where pipelines and cables may exist, 
and when anchoring, dragging or trawling.
Covered wells may be marked by lighted or unlighted buoys.


SAILING DIRECTIONS
Bearings of sailing courses are true and distances given thereon are in Nautical 
miles between points of departure.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with 
additional data from the Corps of Engineers, Geological survey, and U.S. Coast 
Guard.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response 
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard 
facility if telephone communication is impossible (33 CFR 153).


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception
range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles
for stations at high elevations.

Newberry, MI		WNG-576		162.45 MHz
Sault Ste Marie, MI	KIG-74		162.55 MHz



TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.gov

PLANE OF REFERENCE 
(Low Water Datum).......577.5ft
Referred to mean water level at Rimouski, Quebec, International Great Lakes Datum (1985)

END OF FILE
