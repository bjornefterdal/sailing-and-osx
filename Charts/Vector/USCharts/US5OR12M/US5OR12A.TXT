NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5OR12E - COLUMBIA RIVER, HARRINGTON POINT TO CRIMS ISLAND

INDEX:
SOUNDING DATUM
AUTHORITIES
AIDS TO NAVIGATION
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
WARNING - PRUDENT MARINER
ADDITIONAL INFORMATION
NOTE A
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - DREDGED AREAS
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
STATUTE MILES - COLUMBIA RIVER
TIDAL INFORMATION
ADMINISTRATION AREA


NOTES:

SOUNDING DATUM
Soundings and Clearances of Bridges and Overhead Cables at
Columbia River Datum.
(Mean Lower Low Water During Lowest River Stages)


AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers and U.S. 
Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication is impossible (33 CFR 
153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important 
supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
13th Coast Guard District in Seattle, Washington, or at the 
Office of the District Engineer, Corps of Engineers in 
Seattle, Washington. 
Refer to charted regulation section numbers.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling. 
Covered wells may be marked by lighted or 
unlighted buoys.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio Stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Astoria, OR	 KEC-91		162.400 MHz
Neahkahnie, OR	 WWF-94		162.425 MHz
Tillamook, OR	 WWF-95		162.475 MHz
Olympia, WA	 WXM-62		162.475 MHz
Portland, OR	 KIG-98		162.550 MHz


STATUTE MILES - COLUMBIA RIVER
Mileage distances along the Columbia River are 
in Statute Miles eastward from the mouth. 
Tables for converting statute miles to International 
Nautical miles are given in Coast Pilot 7.


TIDAL INFORMATION
Soundings and clearances of bridges and overhead cables at Columbia River 
Datum (Mean lower low water during lowest river stages). The diurnal range 
of the tide during low river stages is 2.1 meters / 6.9 feet at Three Tree Point, 
2.0 meters / 6.4 feet at Cathlamet and 1.7 meters / 5.5 feet at Eagle Creek. 
The range becomes progressively small with higher stages of the river. 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov


ADMINISTRATION AREA
This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone
over which the United States exercises sovereignty extending to the airspace as well as to its bed
and subsoil. For more information, please refer to the Coast Pilot.

END OF FILE
