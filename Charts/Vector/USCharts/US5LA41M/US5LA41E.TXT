NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5LA41E - LOOP DEEPWATER PORT

INDEX:
NOTE A
AIDS TO NAVIGATION
CAUTION - TEMPORARY
WARNING - PRUDENT MARINER
CAUTION - SUBMARINE PIPELINES AND CABLES
MINERAL DEVELOPMENT STRUCTURES
POLLUTION REPORTS
CAUTION - HURRICANES AND TROPICAL STORMS
CAUTION - CURRENTS
AUTHORITIES
NOAA WEATHER RADIO BROADCASTS
COLREGS, 80.01 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS

NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 5. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
8th Coast Guard District in New Orleans, LA., or at the Office
of the District Engineer, Corps of Engineers in New Orleans,
LA.
Refer to charted regulation section numbers.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.

CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals
are required for fixed mineral development
structures shown on this chart, subject to ap-
proval by the District Commander, U.S. Coast
Guard (33 CFR 67).

POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).

CAUTION - HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to
marine structures, aids to navigation and moored vessels, resulting in submerged debris in 
unknown locations.
Charted soundings, channel depths and shoreline may not reflect actual conditions
following these storms. Fixed aids to navigation may have been damaged or destroyed.
Buoys may have been moved from charted positions, damaged, sunk, extinguished or
otherwise made inoperative. Mariners should not rely upon the position or operation of
an aid to navigation. Wrecks and submerged obstructions may have been displaced from
charted locations. Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to report aids to
navigation discrepancies and hazards to navigation to the nearest United States Coast
Guard unit.

CAUTION - CURRENTS
Strong, variable direction currents due to Mis-
sissippi River run-off may be encountered within
the limits of this chart.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the Department of the Interior and the
U.S.Coast Guard.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed
below provides continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Buras, LA		WXL-41		162.475MHz

COLREGS, 80.01 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been 
omitted from this chart.


END OF FILE
