NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4MI90 - STONY LAKE TO POINT BETSIE

INDEX:
PLANE OF REFERENCE
AIDS TO NAVIGATION
SYMBOLS AND ABBREVIATIONS
BRIDGE AND OVERHEAD CABLE CLEARANCES 
AUTHORITIES
NOTE A
CAUTION - POTABLE WATER INTAKE
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION
CAUTION
RADAR REFLECTORS
NOAA VHF-FM WEATHER BROADCASTS
SAILING DIRECTIONS
TIDAL INFORMATION
ADDITIONAL INFORMATION

PLANE OF REFERENCE 
(Low Water Datum)..........176m/ 577.5 ft.: Referred to mean water level at Rimouski,  Quebec, International Great Lakes Datum (1985).

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

SYMBOLS AND ABBREVATIONS
For complete list of symbols and abbreviations see Chart no. 1.

BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water Datum, bridge and overhead clearances are reduced correspondingly.  For clearances see the U.S. Coast Pilot 6.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard. 

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 9th Coast Guard District in Cleveland, Ohio or at the Office of the District Engineer, Corps of Engineers in Detroit, Michigan. Refer to charted regulation section numbers.

CAUTION - POTABLE WATER INTAKE
Vessels operating in fresh water lakes or rivers shall not discharge sewage, or ballast, or bilge water within such areas adjacent to domestic water intakes as are designated by the Commissioner of Food and Drugs (21 CFR 1250.93).  Consult U.S. Coast Pilot 6 for important supplemental information.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area.  Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging or trawling. Covered wells may be marked by lighted or unlighted buoys.

CAUTION
Improved channels are subject to shoaling, particularly at the edges.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids.  See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response 
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.

CAUTION
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners. During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed.  For details see U.S. Coast Guard Light List.

CAUTION
Due to periodic high water conditions in the Great Lakes, some features charted as visible at Low Water Datum may be submerged, particularly in the near shore areas.  Mariners should proceed with caution.

CAUTION
Limitations on the use of certain radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearing to commercial broadcasting stations are subject to error and should be used with caution.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector identification on these aids has been omitted.

NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service stations listed below provide continuous marine weather broadcasts.  
The range of reception is variable, but for most stations is usually 20 to 40 miles from the antenna site.

Hesperia, MI.		WWF-36		162.475 MHz 
Sister Bay, WI		WXN-69		162.425 MHz
Traverse City, MI       KIH-22          162.400 MHz
Sheboygan, WI           WWG-91          162.425 MHz

SAILING DIRECTIONS
Bearings of sailing courses are true and distances given thereon are in statute miles between points of departure.

TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

END OF FILE 