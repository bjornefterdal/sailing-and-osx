For bascule bridges, whose spans don not open to a full upright or vertical position, unlimited vertical clearance is not available for the entire charted horizontal clearance.
