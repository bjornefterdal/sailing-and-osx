NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US119004 - HAWAI'IAN ISLANDS


INDEX:
AIDS TO NAVIGATION
CAUTION - TEMPORARY
NOAA WEATHER RADIO BROADCASTS
POLLUTION REPORTS
CAUTION - LIMITATIONS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE A
AUTHORITIES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
ADDITIONAL INFORMATION
TIDAL INFORMATION


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts.  The reception range is 
typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations 
at high elevations.

Kulani Cone, HI	 KBA-99		162.55 MHz
South Point, HI	 KBA-99		162.55 MHz
Mt Haleakala, HI KBA-99		162.40 MHz
Hawaii Kai, HI	 KBA-99		162.40 MHz
Mt Kaala, HI	 KBA-99		162.55 MHz
Kokee, HI	 KBA-99		162.40 MHz


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), 
or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light 
Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and should be used with caution. 


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart.  Not all 
submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have 
become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their 
draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may 
be marked by lighted or unlighted buoys.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or revisions to Chapter 2 are 
published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the 
Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office of the District Engineer, Corps of Engineers 
in Honolulu, Hawaii. Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the U.S. Coast Guard, 
Geological Survey, and National Geospatial-Intelligence Agency.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on 
these aids has been omitted from this chart.


ADDITIONAL INFORMATION 
Additional information can be obtained at www.nauticalcharts.noaa.gov


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


END OF FILE