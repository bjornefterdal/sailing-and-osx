NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5PR50M - NAVASSA ISLAND


INDEX:


AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
SUPPLEMENTAL INFORMATION
CAUTION  TEMPORARY CHANGES
WARNING  PRUDENT MARINER
ADDITIONAL INFORMATION
NOTE A
AUTHORITIES
HURRICANES AND TROPICAL STORMS
RADAR REFLECTORS
CAUTION  CURRENT
TIDAL INFORMATION


NOTES:


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


CAUTION  TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


WARNING  PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 7th Coast Guard District in Miami, Florida, or at the Office of the District Engineer, Corps of Engineers in Jacksonville, Florida. Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the U.S. Coast Guard.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations. Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved. Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


CAUTION - CURRENT
The current generally sets to the northwest. The wind seldom blows from the westward.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov. 

END OF FILE