Deep draft vessels entering and departing Frenchman Bay and Bar Harbor 
are requested to remain within the Recommended Vessel Route. Two-way traffic is 
possible within all parts of the designated areas. Other vessels, while not excluded, 
should exercise caution in these areas and monitor VHF channel 16 or 13 for 
information concerning vessels transiting these areas. See U.S. Coast Pilot 1, 
Chapter 6.