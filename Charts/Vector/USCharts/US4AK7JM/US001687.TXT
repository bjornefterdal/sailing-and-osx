NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK7JE - TANAGA ISLAND TO UNALGA ISLAND

INDEX:
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
NOTE A
COLREGS, 80.1705 (see note A)
CAUTION - LIMITATIONS
CAUTION � TEMPORARY 
POLLUTION REPORTS
CAUTION - NUMEROUS UNCHARTED ROCKS
WARNING - PRUDENT MARINER
AUTHORITIES
TIDAL INFORMATION
ADDITIONAL INFORMATION


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important
supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to 
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 9. Additions or revisions to Chapter 2 are 
published in the Notices to Mariners. Information concerning 
the regulations may be obtained at the Office of the
Commander, 17th Coast Guard District in Juneau, Alaska, 
or at the Office of the District Engineer, Corps of Engineers 
in Anchorage, Alaska. 
Refer to charted regulation section numbers.


COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at
Sea,1972.The entire area of this chart falls seaward of the 
COLREGS Demarcation Line.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and 
should be used with caution.


CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See  
Local Notice to Mariners.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication is impossible (33 CFR 
153).


CAUTION - NUMEROUS UNCHARTED ROCKS
Numerous uncharted rocks may exist shoreward of the 10 meter curve.


CAUTION - EXTREMELY HEAVY TIDE
Extremely heavy tide rips and strong currents,
which at times make control of vessels difficult,
may be encountered in the passages between
the North Pacific Ocean and the Bering Sea.
See Tidal Current Tables for supplemental
information.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to
navigation, particularly on floating aids. See U.S. Coast Guard Light
List and U.S. Coast pilot for details.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey.


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE