NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION
US5MI32E - DETROIT RIVER

INDEX:

NOTE - A
PLANE OF REFERENCE
CLEARANCES
SUPPLEMENTAL INFORMATION
AUTHORITIES
CAUTION - MARINE RADIOBEACONS
POLLUTION REPORTS
AIDS TO NAVIGATION
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES and CABLES
CAUTION - UNCHARTED STAKES AND FISHING STRUCTURES
CAUTION - POTABLE WATER INTAKE
CAUTION - HIGH WATER CONDITIONS
NOAA VHF-FM WEATHER BROADCASTS
WARNING - PRUDENT MARINER
NOTE Z - NO-DISCHARGE ZONE, 40 CFR 140
CAUTION
RACING BUOYS
RADAR REFLECTORS
ADDITIONAL INFORMATION

NOTE - A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.
Additions or revisions to Chapter 2 are published in the Notice to Mariners.
Information concerning the regulations may be obtained at the Office of the Commander,
9th Coast Guard District in Cleveland, Ohio, or at the Office of the District Engineer,
Corps of Engineers in Detroit, Michigan.
Refer to charted regulation section numbers.

PLANE OF REFERENCE
PLANE OF REFERENCE OF THIS CHART (Low Water Datum). Depths are referred to the
sloping surface of the river when Lake Huron is at elevation 577.5 feet and Lake
St. Clair is at elevation 572.3 feet.
Referred to mean water level at Rimouski, Quebec, International Great Lakes Datum
(1985).

CLEARANCES
BRIDGE AND OVERHEAD CABLE CLEARANCES. When the water surface is above Low Water
Datum, bridge and overhead clearances are reduced correspondingly. For clearances
see U.S. Coast Pilot 6.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with
additional data from the Corps of Engineers, Geological Survey, U.S. Coast Guard,
and Canadian authorities.

CAUTION - MARINE RADIOBEACONS
Limitations on the use of certain other radio signals as aids to marine navigation can be 
found in the U.S. Coast Guard Light Lists and NaNational Geospatial-Intelligence Agency
publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject to error
and should be used with caution.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone
communication is impossible (33 CFR 153).

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to
navigation. See Canadian List of Lights, Buoys and Fog Signals for information not
included in the U.S. Coast Guard Light List.

CAUTION - TEMPORARY 
Temporary changes or defects in aids to navigation are not indicated on this chart.
See Notice to Mariners. During some winter months or when endangered by ice, certain
aids to navigation are replaced by other types or removed. For details see U.S. 
Coast Guard Light List.

CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.

CAUTION - SUBMARINE PIPELINES and CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the 
area of this chart. Not all submarine pipelines and submarine cables are required 
to be buried, and those that were originally buried may have become exposed.  
Mariners should use extreme caution when operating vessels in depths of water 
comparable to their draft in areas where pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted or unlighted buoys.

CAUTION - UNCHARTED STAKES AND FISHING STRUCTURES
Mariners are warned that numerous uncharted stakes and fishing structures,
some submerged, mat exist in the area of this chart. Such structures are not
charted unless known to be permanent.

CAUTION - POTABLE WATER INTAKE
Vessels operating in fresh water lakes or rivers shall not discharge sewage, or ballast,
or bilge water within such areas adjacent to domestic water intakes as are designated by
the Commissioner of Food and Drugs (21 CFR 1250.93). Consult U.S. Coast Pilot 6 for
important supplemental information.

CAUTION - HIGH WATER CONDITIONS
Due to periodic high water conditions in the Great Lakes, some features charted
as visible at Low Water Datum may be submerged, particularly in the near shore areas.
Mariners should proceed with caution.

NOAA VHF-FM WEATHER  BROADCASTS
 The National Weather Service stations listed below provide continuous weather 
broadcasts. The reception range is typically 20 to 40 miles from the antenna site,
but can be as much as 100 nautical miles for stations at high elevations.

Detroit, MI      KEC-63      162.55 MHz

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.

NOTE Z - NO-DISCHARGE ZONE, 40 CFR 140
Michigan waters of Lakes Michigan, Huron, Superior, Erie and St. Clair, all water ways connected
thereto, and all inland lakes are designated as a No-Discharge Zone (NDZ). This chart falls entirely
within the limits of a No-Discharge Zone (NDZ). Under the Clean Water Act, Section 312, all vessels 
operating within a No-Discharge Zone (NDZ) are completely prohibited from discharging any sewage,
treated or untreated, into the waters. Commercial vessel sewage shall include graywater. All vessels
with an installed marine sanitation device (MSD) that are navvigating, moored, anchored, or docked 
within a NDZ must have the MSD disabled to prevent the overboard discharge of sewage (treated or untreated)
or install a holding tank. Regulations for the NDZ are contained in the U.S. Coast Pilot. Additional 
information concerning the regulations and requirements may be obtained from the Environmental Protection 
Agency (EPA) web site: http://www.epa.gov/owow/oceans/vessel_sewage/vsdnozone.html.

CAUTION
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard
Light Lists and National Geospatial-Intelligence Agency Publicaiton 117.
Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used 
with caution.

RACING BUOYS
Racing buoys within the limits of this chart are not shown hereon. Information may 
be obtained from the U.S. Coast Guard District Offices as racing and other privately 
maintained buoys are not all listed in the U.S. Coast Guard Light List.

RADAR REFLECTORS
Radar Reflectors have been placed on many floating aids to navigation. Individual 
radar reflector identification on these aids has been omitted from this chart.

ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.gov


END OF FILE
