Navigation regulations are published in Chapter 2, U.S. Coast Pilot 4.  
Additions or revisions to Chapter 2 are published in the Notices to Mariners.  
Information concerning the regulations may be obtained at the Office of the 
Commander, 7th Coast Guard District in Miami, Florida, or at the Office of the 
District Engineer, Corps of Engineers in Jacksonville, Florida.
Refer to charted regulation section numbers.

The Inland Navigational Rules Act of 1980 is in effect for vessels transiting this area. The
seaward boundaries of this area are the COLREGS demarcation lines. In the area
seaward of the COLREGS demarcation lines, vessels are governed by COLREGS:
International Regulations for Preventing Collisions at Sea, 1972.  The COLREGS
demarcation line is defined in 33 CFR 80.735c.