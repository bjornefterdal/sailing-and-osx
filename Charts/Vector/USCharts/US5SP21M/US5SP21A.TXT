NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5SP21E - KINGMAN REEF

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
AIDS TO NAVIGATION
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
PROHIBITED AREA
CAUTION
CAUTION


SYMBOLS AND ABBREVIATIONS: For Symbols and Abbreviations see Chart no. 1.

AUTHORITIES: Hydrography and topography by the National Geospatial-Intelligence Agency, with additional data from the National 0cean Service, Coast Survey.

AIDS TO NAVIGATION: Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.  

WARNING � PRUDENT MARINER: The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS: Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: Consult U.S. Coast Pilot 7 for important supplemental information.

PROHIBITED AREA: Kingman Reef is a Naval Defensive Sea Area and Airspace Reservation.  Regulations are published in National Geospatial-Intelligence Agency Hydrographic Center Pub. 126.

CAUTION: Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.

CAUTION: Pending further surveys, the utmost caution should be exercised in using this anchorage.


END OF FILE
