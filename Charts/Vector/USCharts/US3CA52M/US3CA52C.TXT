IMO AMENDED TRAFFIC SEPARETION SCHEME
Portions of the traffic separation scheme shown on this chart
have been amended by the IMO. See IMO COLREG.2/Circ.64.
Please be advised that these portions have been revised by
the United States Coast Guard and that the corresponding
changes have not been updated in the Code of Federal
Regulations(33 CFR part 167). There are differences between
the two traffic separation schemes and caution is advised.

TRAFFIC SEPARATION SCHEME
One-way traffic lanes overprinted on this chart are 
recommended for use by all vessels traveling between the 
points involved. They have been designed to aid in the 
prevention of collisions at the approaches to San Francisco 
Bay but are not intended in any way to supersede or alter 
the applicable Rules of the Road. Separation Zones are 
intended to separate inbound and outbound traffic and 
to be free of ship traffic. Separation Zones should not be 
used except for crossing purposes. Mariners are requested 
to stay outside the circular separation zone centered on the 
San Francisco Approach Lighted Horn Buoy SF. When 
crossing traffic lanes and separation zones use extreme 
caution.
