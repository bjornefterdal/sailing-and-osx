NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK52M - UGASHIK BAY TO EGEGIK BAY

INDEX:
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
PRELIMINARY DATA 2015 EDITION
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
COLREGS, 80.1705 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES
Hydrography and topography by the National
Ocean Service, Coast Survey, with additional
data from the Corps of Engineers, Geological
Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to 
navigation.  

NOTE A
Navigation regulations are published in
Chapter 2, U.S. Coast Pilot 9.  Additions or
revisions to Chapter 2 are published in the
Notice to Mariners.  Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska, or at the Office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers. 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid
to navigation, particularly on floating aids. See U.S. Coast
Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility
if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important supplemental information.

PRELIMINARY DATA 2015 EDITION
Most of the data in this ENC is considered to be of marginal quality for safe navigation at this scale.
Many of the depths were taken by leadline in the early 1900's, so uncharted shoals are
likely in this area. Navigators should use this ENC with extreme caution and report
discrepancies or hazards to the Chief, Marine Chart Division (N/CS2), National Ocean
Service, NOAA, Silver Spring, Maryland 20910-3282.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation.  Individual radar
reflector identification on these aids has been
omitted from this chart.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed
below provides continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Tuklung Mt, AK		WNG-525		162.425 MHz

COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE