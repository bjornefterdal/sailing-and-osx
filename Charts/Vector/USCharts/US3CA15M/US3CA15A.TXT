NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3CA15E - POINT ARENA TO TRINIDAD HEAD

INDEX:
AUTHORITIES
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
NOTE A
CAUTION - TEMPORARY CHANGES
NOAA WEATHER RADIO BROADCASTS
WARNING - PRUDENT MARINER
AIDS TO NAVIGATION
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - LIMITATIONS
RADAR REFLECTORS
TIDAL INFORMATION
VESSEL TRANSITING
ADDITIONAL INFORMATION

NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the U.S. Coast Guard.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the
National Response Center via 1-800-424-8802 (toll free), or
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important
supplemental information.

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners.  Information concerning the
regulations may be obtained at the Office of the Commander,
11th Coast Guard District in Alameda, California or at the
office of District Engineer, Corps of Engineers in
San Franscisco, California.
Refer to charted regulation section numbers.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.
Eureka, CA		KEC-82	162.400 MHz WX2
Point Arena, CA		KIH-30	162.550 MHz WX1

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information
concerning aids to navigation.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart.  Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed.  Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

CAUTION - LIMITATIONS
Only marine radiobeacons have been cali-
brated for surface use. Limitation on the use
of certain other radio signals as aids to marine
navigation can be found in the U.S. Coast Guard
Light List and National Geospatial-Intelligence
Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

RADAR REFLECTORS
Radar Reflectors have been placed on many floating aids
to navigation. Individual radar reflector identification on
these aids has been omitted from this chart.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to
http://co-ops.nos.noaa.gov.

VESSEL TRANSITING
The U.S. Coast Guard and the Pacific States/British Columbia Oil Spill
Task Force endorse a system of voluntary measures and minimum
distances from shore for certain commercial vessels transiting along the coast anywhere between Cook Inlet, Alaska and San Diego,
California. See U.S. Coast Pilot 7, Chapter 3 for details.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE