NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5TX25M - CARLOS BAY TO REDFISH BAY

INDEX:
AUTHORITIES	
AIDS TO NAVIGATION
NOTE A
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
CAUTION - Improved channel
CAUTION - PILES AND PLATFORMS
CAUTION - SMALL CRAFT
CAUTION - WARNING CONCERNING LARGE VESSELS
RADAR REFLECTORS
HURRICANES AND TROPICAL STORMS
RULES OF THE ROAD
MARINE WEATHER FORECASTS NATIONAL WEATHER SERVICE
NOAA WEATHER RADIO BROADCASTS
BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS
SUBMARINE PIPELINES AND CABLES
INTRACOASTAL WATERWAY AIDS
MINERAL DEVELOPMENT STRUCTURES
TIDAL INFORMATION
ADDITIONAL INFORMATION
ADMINISTRATION AREA

NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps ofEngineers, Geological Survey, and U.S. Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.
 

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 8th Coast Guard District in New Orleans, LA or at the Office of the District Engineer, Corps of Engineers in Galveston, TX.
Refer to charted regulation section numbers. 


WARNING � PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. 
Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.



CAUTION - Improved channel
Improved channels are subject to shoaling, particularly at the edges.


CAUTION - PILES AND PLATFORMS
Stakes, piles and platforms, some submerged, may exist between charted piling and platforms along the maintained channels. Piles and platforms are not charted where they interfere with a light symbol.


CAUTION - SMALL CRAFT
Small craft should stay clear of large commercial and government vessels even if small craft have right-of-way.
All craft should avoid areas where the skin divers flag, a red square with a diagonal white stripe, is displayed.


CAUTION - WARNING CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall not impede the passage of a vessel that can navigate only within a narrow channel or fairway. Large vessels may appear to move slowly due to their large size but actually transit at speeds in excess of 12 knots, requiring a great distance in which to maneuver or stop. A large vessel's superstructure may block the wind with the result that sailboats and sailboards may unexpectedly find themselves unable to maneuver. Bow and stern waves can be hazardous to small vessels.  Large vessels may not be able to see small craft close to their bows.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.
Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms.  Fixed aids to navigation may have been damaged or destroyed.  Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative.  Mariners should not rely upon the position or operation of an aid to navigation.  Wrecks and submerged obstructions may have been displaced from charted locations.  Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


RULES OF THE ROAD
Motorless craft have the right-of-way in almost all cases.  Sailing vessels and motorboats less than sixty-five feet in length shall not hamper, in a narrow channel, the safepassage of a vessel which can navigate only inside that channel.
A motorboat being overtaken has the right-of-way. Motorboats approaching head to head or nearly so should pass port to port.
When motorboats approach each other at right angles or obliquely, the boat on the right has the right-of-way in most cases.
Motorboats must keep to the right in narrow channels when safe and practicable.
Motorboats are urged to become familiar with the complete text of the Rules of the Road in U.S. Coast Guard publication "Navigation Rules".


MARINE WEATHER FORECASTS 
NATIONAL WEATHER SERVICE
CITY			TELEPHONE NUMBERS	             OFFICE HOURS
Corpus Christi, TX	 (361) 289-0959		       8:00 AM-5:00 PM (Mon.-Fri.)
		        *(361) 289-0753		

*Recording (24 hours daily)



NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Corpus Christi, TX	     KHB-41         162.55 MHz		
Port O'Connor, TX	     WXL-26        162.475 MHz	

			

BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS

CITY		     STATION	    FREQ.            BROADCAST TIMES-CST		 SPECIAL WARNING
Port Isabel, TX	     NCH	    2670 kHz	     4:40, 6:40 & 10:40 AM 4:40 PM	  On receipt
				    157.1 MHz	     4:45, 6:45 & 10:45 AM 4:45 PM        On receipt
Port Aransas, TX     NOY-3	    2670 kHz         4:30, 6:30 & 10:30 AM 4:30 PM	  On receipt
						     4:40 & 6:40 AM 4:40 PM	          On receipt		
Corpus Christi, TX   NOY-8	    2670 kHz	     4:40, 6:40 & 10:40 AM & 4:40 PM	
Port Isabel, TX	     NOY-8	    2670 kHz	     4:40, 6:40 & 10:40 AM & 4:40 PM
Port Isabel, TX	     NOY-8	    157.1 MHz	     5:00, 11:00 AM & 5:00 PM
Robstown, TX	     NOY-8	    157.1 MHz  	     5:00, 11:00 AM & 5:00 PM

Distress calls for small craft are made on 2182 kHz or channels 16 (156.80 MHz) VHF.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling.  
Covered wells may be marked by lighted or unlighted buoys.



INTRACOASTAL WATERWAY AIDS
The U.S. Aids to Navigation System is designed for use with nautical charts, and the exact meaning of an aid to navigation may not be clear unless the appropriate chart is consulted.
Aids to navigation marking the Intracoastal Waterway exhibit unique yellow symbols to distinguish them from aids marking other water-ways
When following the Intracoastal Waterway westward form the Carrabelle, FL to Brownsville, TX, aids with yellow triangles should be kept on the starboard side of the vessel and aids with yellow squares should be kept on the port side of the vessel.
A horizontal yellow band provides no lateral information, but simply identifies aids to navigation as marking the Intracoastal Waterway.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals are required for fixed mineral development structures shown, subject to approval by the District Commander, U.S. Coast Guard (33 CFR 67).




TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea. �The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. �For more information, please refer to the Coast Pilot.


END OF FILE
