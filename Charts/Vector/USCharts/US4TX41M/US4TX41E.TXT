NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4TX41E- SAN LUIS PASS TO EAST MATAGORDA BAY

INDEX:
AIDS TO NAVIGATION
AUTHORTIES
CAUTION
CAUTION
CAUTION
CAUTION- HURRICANES AND TROPICAL STORMS
CAUTION - SUBMARINE PIPELINES and CABLES
NOAA VHF-FM WEATHER BROADCASTS
POLLUTION REPORTS
RADAR REFLECTORS
SUPPLEMENTAL INFORMATION
MINERAL DEVELOPMENT STRUCTURES
WARNING-PRUDENT MARINER
ADDITIONAL INFORMATION
TIDAL INFORMATION

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information 
concerning aids to navigation.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


CAUTION  
Limitations on the use of certain other radio signals as aids to 
marine navigation can be found in the U.S. Coast Guard Light Lists 
and National Geospatial Intelligence Agency Publication 117.

Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


CAUTION
Temorary changes or defects in aids to navigation are not indicated 
on this chart.  See Notice to Mariners.


CAUTION
The positions of the oil well drilling structures are approximate only.  
All such structures existing within the area of this chart may not be 
indicated.


CAUTION - HURRICANES AND TROPICAL STORMS 
Hurricanes, tropical storms and other major storms may 
cause considerable damage to marine structures, aids to navigation and moored vessels, resulting 
in submerged debris in unknown locations. Charted soundings, channel depths and shoreline may 
not reflect actual conditions following these storms. Fixed aids to navigation may have been 
damaged or destroyed. Buoys may have been moved from charted positions, damaged, sunk extinguished 
or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to 
navigation. Wrecks and submerged obstructions may have been displaced from charted locations. 
Pipelines may have become uncovered or moved. Mariners are urged to exercise extreme caution and 
are requested to report aids to navigation discrepancies and hazards to navigation to the nearest 
United States Coast Guard unit.


CAUTION - SUBMARINE PIPELINES and CABLES
Additional uncharted submarine pipelines and submarine cables may 
exist within the area of this chart.  Not all submarine pipelines 
and sub- marine cables are required to be buried, and those that 
were originally buried may have become exposed.  Mariners should 
use extreme caution when operating vessels in depths of water comparable 
to their draft in areas where pipelines and cables may exist, and 
when anchoring, dragging or trawling.

Covered wells may be marked by lighted or unlighted buoys.


NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service stations listed below provide continuous 
marine weather broad- casts.  The range of reception is variable, but 
for most stations is usually 20 to 40 miles from the antenna site.

Galveston, TX           KHB-40       162.55 MHz
Bay City, TX            WWG-40       162.425 MHz


POLLUTION REPORTS
Report all spills of oil and hazardous sub- stances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest 
U.S. Coast Guard facility if telephone communication is impossible 
(33 CFR 153).


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation.  
Individual radar reflector indentification on these aids has been 
omitted from this chart.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals are required for fixed 
mineral development structures shown on this chart, subject to approval 
by the District Commander, U.S. Coast Guard (33 CFR 67).

WARNING
The prudent mariner will not rely soley on any single aid to navigation, 
particularly on floating aids. See U.S. Coast Guard Light List and U.S. 
Coast Pilot for details.

ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.noaa.gov.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication of go to http://co-ops.nos.noaa.gov

END OF FILE


