NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK5JM - ALINCHAK BAY:

INDEX:
AIDS TO NAVIGATION
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
WARNING - PRUDENT MARINER
ADDITIONAL INFORMATION
NOTE A
AUTHORITIES
CAUTION - TEMPORARY
CAUTION - LIMITATIONS
CAUTION
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCAST
LOCAL MAGNETIC DISTURBANCE
TIDAL INFORMATION
ADMINISTRATION AREA
COLREGS, 80.1705 


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the 
nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important supplemental information.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light 
List and U.S. Coast Pilot for details.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 8. Additions or revisions to Chapter 2 are published in the 
Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 17th Coast Guard 
District in Juneau, Alaska, or at the Office of the District Engineer, Corps of Engineers in Anchorage, Alaska.
Refer to charted regulation section numbers.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, 
Geological Survey, U.S. Coast Guard, and National Geospatial-Intelligence Agency.

CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject 
to error and should be used with caution.

CAUTION
Tidal observations conducted by the National Ocean Service since the earthquake of March 27, 1964 indicate bottom subsidence 
at the following locations: 
Subsidence in (feet)

Uganik Bay	-3.7
Kodiak		-5.8

Mariners are cautioned to expect shoaling or deepening for the areas listed. Tidal observations at this time are at selected sites 
and the magnitude of the changes except at these sites is not known.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids 
has been omitted from this chart.

NOAA WEATHER RADIO BROADCASTS    
The NOAA Weather Radio stations listed below provides continuous weather broadcasts.  The reception range is typically 20 to 
40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Raspberry I, AK	KZZ-90	       162.425 MHz
Kodiak, AK	WXJ-78	       162.55 MHz
Homer, AK	WXJ-24	       162.40 MHz

LOCAL MAGNETIC DISTURBANCE
Differences of as much as 3� from the normal variation have been observed in the inshore waters of this chart.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADMINISTRATION AREA
The entire extent of this ENC cell falls with the limits of an Administration Area.  The area covers land, internal waters, and
and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as 
well as as to its bed and subsoil.  For more information, please refer to the Coast Pilot.

COLREGS, 80.1705 
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


END OF FILE