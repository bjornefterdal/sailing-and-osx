NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK28M - LATOUCHE PASSAGE TO WHALE BAY

INDEX:
AIDS TO NAVIGATION
WARNING - PRUDENT MARINER
CAUTION - TEMPORARY CHANGES
RADAR REFLECTORS
SUPPLEMENTAL INFORMATION
AUTHORITIES
CAUTION - SIGNIFIGANT CHANGES
CAUTION - SUBMARINE PIPELINES AND CABLES
COLREGS, 80.1705 (see Note A)
NOTE A
NOAA WEATHER RADIO BROADCASTS
OIL SPILL TASK FORCE
POLLUTION REPORTS
UPLIFT DUE TO EARTHQUAKE OF 1964
ADMINISTRATION AREA
CAUTION - MONTAGUE ISLAND UPLIFT (NOTE B)
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids
to navigation. Individual radar reflector identification on
these aids has been omitted from this chart.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important
supplemental information.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the U.S. Coast Guard and Geological 
Survey.


CAUTION - SIGNIFIGANT CHANGES
Significant changes in depths and shoreline
may have occurred in the area of this chart as a
result of the earthquake of March 27, 1964. 
Mariners are urged to use extreme caution when
navigating in the area of this chart as the magnitude
of change is not known.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.  
Covered wells may be marked by lighted or
unlighted buoys.


COLREGS, 80.1705 (see Note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS demarcation line.


NOTE A
Navigation regulations are published in
Chapter 2, U.S. Coast Pilot 9.  Additions or
revisions to chapter 2 are published in the
Notices to Mariners.  Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska, or at the office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Rugged I, AK       	WNG-526   	162.425 MHz
Naked I, AK         	WNG-530    	162.500 MHz
Point Pigot, AK     	KZZ-93      	162.450 MHz
Cape Hinchinbrook  	WNG-532      	162.525 MHz
Potato Point, AK      	WNG-527    	162.425 MHz
Seward, AK          	KEC-81       	162.550 MHz
Whittier, AK      	KXI-29       	162.400 MHz
East Point, AK          WNG-530         162.500 MHZ 


OIL SPILL TASK FORCE
The U.S. Coast Guard and the Pacific States/British 
Columbia Oil Spill Task Force endorse a system of voluntary measures and 
minimum distances from shore for certain commercial vessels transiting along 
the coast anywhere between Cook Inlet, Alaska, and San Diego, California.  
See U.S. Coast Pilot 8 or 9, Chapter 3 for details.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


UPLIFT DUE TO EARTHQUAKE OF 1964
Significant changes in depths and shoreline may have occurred in 
the area of this chart as a result of the earthquake of March 27, 1964. 
Tidal observations since the earthquake indicate bottom uplift of 
+7.0 feet / 2.13 m  at Sawmill Bay. Mariners are urged to use extreme 
caution when navigating in the area of this chart as the magnitude of 
change except at this site is not known. Shoreline and hydrography in 
the vicinity of Sawmill Bay has been revised from 1966 surveys.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. 
This area covers land, internal waters, and territorial sea.  The territorial sea is 
a maritime zone which the United States exercises sovereignty extending to the 
airspace as well as to its bed and subsoil.  For more information, please 
refer to the Coast Pilot.


CAUTION - MONTAGUE ISLAND UPLIFT (NOTE B)
The earthquake of 1964 caused uplifts as much as 32 feet 
in the vicinity of Montague Island.  Thus, these waters may be shoaler 
than charted.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


End of file