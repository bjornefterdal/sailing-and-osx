Mariners should use caution as military craft may be operating within the area. For further
information  consult the U.S. Coast Guard Local Notice to Mariners.
