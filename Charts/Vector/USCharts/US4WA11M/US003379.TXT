The U.S. Coast Guard operates a mandatory Vessel Traffic 
Services (VTS) system in the Puget Sound area.  Vessel 
operating procedures and designated radiotelephone frequen-
cies are published in 33 CFR 161, the U.S. Coast Pilot, and/
or the VTS User's Manual. The entire area of the chart falls 
within the Vessel Traffic Services (VTS) system.
