U.S. Government property prohibited to the public. Area is 
dangerous due to live undetonated explosives. Fishing, trawling
or anchoring within a 274.3 meters/300 yard radius of the RUINS is dangerous
due to possible recovery of aircraft practice bombs containing
explosives.

