NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5HA55M - HONOLULU HARBOR

INDEX:
AUTHORITIES
CAUTION - SUBMARINE PIPELINES AND CABLES
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
CAUTION - DREDGED AREAS
AIDS TO NAVIGATION
POLLUTION REPORTS
NOAA WEATHER RADIO BROADCASTS
NOTE A
WARNING - PRUDENT MARINER
RADAR REFLECTORS
ADDITIONAL INFORMATION
TIDAL INFORMATION
ADMINISTRATIVE AREA

NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, 
Coast Survey, with additional data from the Corps of Engineers 
and U.S. Coast Guard.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines
and submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed.  Mariners should use extreme 
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted 
or unlighted buoys.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important
supplemental information.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See  
Notice to Mariners.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National 
Geospatial-intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

CAUTION - DREDGED AREAS
Improved channels are
subject to shoaling, particularly at the edges.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List
for supplemental information concerning aids to 
navigation.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR
153).

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.
O'ahu		KBA-99		162.550 MHz
Hawai'i		KBA-99		162.550 MHz
Maui		KBA-99		162.400 MHz
Kaua'i		KBA-99		162.400 MHz


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notices to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
14th Coast Guard District in Honolulu, Hawaii, or at the
Office of the District Engineer, Corps of Engineers in
Honolulu, Hawaii.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these has been
omitted from this chart.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov.

ADMINISTRATIVE AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers 
land, internal waters, and territorial sea. The territorial sea is a maritime zone which the United
States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more 
information, please refer to the Coast Pilot.


END OF FILE