Vessels weighing less than 1600 gross tons or tugs with tows
weighing less than 1600 gross tons are not permitted to enter or 
cross over Pinole Shoal dredged channel.