Unexploded projectiles exist in the waterways 
east of the Intracoastal Waterway from Bear Inlet 
to Onslow Beach Bridge. 

Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 4. Additions or 
revisions to Chapter 2 are published in the 
Notices to Mariners. Information concerning 
the regulations may be obtained at the Office 
of the Commander, 5th Coast Guard District in 
Portsmouth, VA, or at the Office of the District 
Engineer, Corps of Engineers in Wilmington, NC.
Refer to charted regulation section numbers.