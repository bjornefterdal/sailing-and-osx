NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5FL98M - GRASSY KEY TO BAHIA HONDA KEY

INDEX:
CAUTION - LIMITATIONS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
NOAA VHF-FM WEATHER BROADCASTS
HURRICANES AND TROPICAL STORMS
CHANNEL MARKERS
CAUTION - DREDGED AREAS
CAUTION - TEMPORARY
COLREGS, 80.740 (see note A)
NOTE A
AIDS TO NAVIGATION
CAUTION - SUBMARINE PIPELINES AND CABLES
INTRACOASTAL WATERWAY
POWER CABLES
AUTHORITIES
WARNING - THE PRUDENT MARINER
TIDAL INFORMATION
FIXED BRIDGES
ADMINISTRATION AREA

CAUTION - LIMITATIONS
Limitations on the use of certain other radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Imagery and Mapping Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 4 for important supplemental information.


NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service stations listed below provides continuous marine weather broadcasts.  The range of reception is variable, but for most stations is usually 20 to 40 miles from the antenna site.

Sugarloaf Key, FL 	WXJ-95		162.40 MHz
Teatable Key, FL	WWG-60		162.45 MHz


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels,resulting in submerged debris in unknown locations. Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from charted positions, damaged, sunk extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


CHANNEL MARKERS
Reflectors on daybeacons and buoys along the Intracoastal Waterway are green on the left-hand and red on the right-hand when proceeding southwestward.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated.  See Notice to Mariners.


COLREGS, 80.740 (see note A)
International Regulations for Preventing Collisions at Sea, 1972. The entire area of this chart falls seaward of the COLREGS Demarcation line.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 4.  Additions or revisions to Chapter 2 are published in the Notices to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 7th Coast Guard District in, Miami, FL., or at the Office of the District Engineer, Corps of Engineers in Jacksonville, FL. Refer to charted regulation section numbers.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart.  Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging or trawling. Covered wells may be marked by lighted or unlighted buoys.


INTRACOASTAL WATERWAY AIDS
The U.S. Aids to Navigation System is designed for use with nautical charts and the exact meaning of an aid to navigation may not be clear unless the appropriate chart is consulted. Aids to navigation marking the Intracoastal Waterway exhibit unique yellow symbols to distinguish them from aids marking other waterways. When following the Intracoastal Waterway southward from Norfolk, Virginia, to Cross Bank in Florida Bay, aids with yellow triangles should be kept on the starboard side of the vessel and aids with yellow squares should be kept on the port side of the vessel. A horizontal yellow band provides no lateral information, but simply identifies aids to navigation as marking the Intracoastal Waterway. 


POWER CABLES
Overhead power cables run parallel to U.S. No. 1.  All clearances are greater than those of the charted fixed bridges.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, and U.S.Coast Gaurd.


WARNING - THE PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

 
FIXED BRIDGES
The section of Seven Mile Bridge between Knight Key and Pigeon Key is a fixed bridge of plate girder spans. 


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.

END OF FILE