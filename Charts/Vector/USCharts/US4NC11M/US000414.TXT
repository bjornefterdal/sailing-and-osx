Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 4. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
5th Coast Guard District in Portsmouth, Virginia or at the 
Office of the District Engineer, Corps of Engineers in 
Wilmington, North Carolina. 
Refer to charted regulation section numbers.