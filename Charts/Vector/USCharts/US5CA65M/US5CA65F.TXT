National Marine Sanctuaries are protected areas, administered by NOAA, 
which contain sensitive and diverse natural and cultural resources. 
These areas are particularly sensitive to environmental damage such as 
spills of oil and other hazardous materials, discharges and groundings. 
Exercise particular caution and follow applicable Sanctuary regulations 
when transiting these areas. A full description of Sanctuary regulations 
may be found in 15 CFR 922 and in the U.S. Coast Pilot. A full description 
of the federal regulations governing the Marine Protected Areas located 
within Channel Islands National Marine Sanctuary boundaries may be 
found in 15 CFR 922 and 50 CFR 660. A full description of the state 
regulations governing the Marine Protected Areas located within Channel 
Islands National Marine Sanctuary boundaries may be found in Title 14 
California Code of Regulations (CCR) Section 632.

Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
11th Coast Guard District in Alameda, California or at the

Office of the District Engineer, Corps of Engineers in 
Sacramento, California. 
Refer to charted regulation section numbers.