NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK5H - CHIGNIK AND KUJULIK BAYS

INDEX:
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY
COLREGS, 80.1705 
NOTE A
AIDS TO NAVIGATION
CAUTION - SIGNIFICANT
AUTHORITIES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important supplemental information.


CAUTION - TEMPORARY
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.


COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 9. Additions or 
revisions to Chapter 2 are published in the 
Notice to Mariners. Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District 
Engineer, Corps of Engineers in Anchorage, 
Alaska.
Refer to charted regulation section numbers.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


CAUTION - SIGNIFICANT
Significant changes in depths and shoreline may have 
occurred in the area of this chart as a result of the earthquake 
of March 27, 1964. Tidal observations since the earthquake 
indicate bottom subsidence of �0.06 meters/-0.2 feet in Chignik Bay. Mariners 
are urged to use extreme caution when navigating in the area 
of this chart as the magnitude of change except at this site is 
not known.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floatings aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE