NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3WA03E - COLUMBIA RIVER TO DESTRUCTION ISLAND

INDEX:
AUTHORITIES
WARNING - PRUDENT MARINER
AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION - TEMPORARY CHANGES
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE A
NOTE G
RADAR REFLECTORS 
VESSEL TRANSITING
CAUTION- WILLAPA BAY
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION

NOTES:
AUTHORITIES
Hydrography and topography by the National
Ocean Service, Coast Survey, with additional
data from the Corps of Engineers and U.S.
Coast Guard.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.

POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated.  See
Local Notice to Mariners.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart.  Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed.  Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7.  Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
13th Coast Guard District in Seattle, Washington or at the 
Office of the District Engineer, Corps of Engineers in
Seattle, Washington.
Refer to charted regulation section numbers.

NOTE G
Submerged submarine operations are
conducted at various times in the waters contained.
Proceed with caution. 

RADAR REFLECTORS 
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.

VESSEL TRANSITING
The U.S. Coast Guard and the Pacific States/British Columbia Oil Spill
Task Force endorse a system of voluntary measures and minimum
distances from shore for certain commercial vessels transiting along
the coast anywhere between Cook Inlet, Alaska and San Diego,
California.  See U.S. Coast Pilot 7, Chapter 3 for details.

CAUTION - WILLAPA BAY
Willapa Bay entrance channel is subject to continual
changes.  Buoys "A through D" are nonlateral aids which
are frequently shifted to mark best water at the time of 
servicing.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.
Astoria, OR		KEC-91		162.40 MHz
Neahaknie, OR		WWF-94		162.425 MHz
Tillamook, OR		WWF-95		162.472 MHz
Forks, WA		KXI-27		162.425 MHz

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE
