NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5OH05E - ASHTABULA HARBOR

INDEX:
NOTE A
AIDS TO NAVIGATION
CAUTION - TEMPORARY CHANGES
BRIDGE AND OVERHEAD CABLE CLEARANCES
CAUTION - DREDGED AREAS
CAUTION - LAKE LEVEL FLUCTUATIONS
CAUTION - LIMITATIONS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
RACING BUOYS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
AUTHORITIES
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 6. Additions or revisions to Chapter 2 are pub-
ished in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Com-
mander, 9th Coast Guard District in Cleveland, Ohio or at
the Office of the District Engineer, Corps of Engineers in
Detroit, Michigan.
Refer to charted regulation section numbers.	


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information
concerning aids to navigation.


CAUTION - TEMPORARY CHANGES 
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are
replaced by other types or removed. For details
see U.S. Coast Guard Light List.


BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water
Datum, bridge and overhead clearances are reduced correspondingly. For clearances see U.S.
Coast Pilot 6.


CAUTION - DREDGED AREAS
Improved channels are
subject to shoaling, particularly at the edges.


CAUTION - LAKE LEVEL FLUCTUATIONS
Due to periodic high water conditions in the Great Lakes, some
features charted as visible at Low Water Datum may be submerged,
particularly in the near shore areas. Mariners should proceed with
caution.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted or
unlighted buoys.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Erie, PA	 KEC-58      162.400 MHz
Meadville, PA	 KZZ-32	     162.476 MHz


RACING BUOYS
Racing buoys within the limits of this chart 
are not shown hereon. Information may be 
obtained from the U.S. Coast Guard District 
Offices as racing and other private buoys are 
not all listed in the U.S. Coast Guard Light List.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important 
supplemental information.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, 
with additional data from the Crops of Engineers, Geological Survey, and U.S. Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to
navigation, particularly on floating aids. See U.S. Coast Guard
Light List and U.S. Coast Pilot 6 for details.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE
