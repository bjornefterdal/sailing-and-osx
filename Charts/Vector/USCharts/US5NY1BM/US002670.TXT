NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NY1BE - NEW YORK HARBOR

INDEX:
NOTE A
SUPPLEMENTAL INFORMATION
POLLUTION REPORTS
AIDS TO NAVIGATION
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - MARINERS 
NOAA WEATHER RADIO BROADCASTS
CAUTION - LIMITATIONS
RADAR REFLECTORS
AUTHORITIES
WARNING - PRUDENT MARINER
RACING BUOYS
TIDAL INFORMATION
ADDITIONAL INFORMATION
HURRICANES AND TROPICAL STORMS


NOTES:

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
New York, NY.
Refer to charted regulation section numbers.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 2 for important supplemental information.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


CAUTION - TEMPORARY
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


CAUTION - MARINERS
Mariners are warned to stay clear of the pro-
tective riprap surrounding navigational light.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed 
below provides continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

New York, NY          KWO-35       162.550 MHz



CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S.Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.
 

AUTHORITIES
Hydrography and topography by the National Ocean service, Coast 
survey, with additional data from the Corps of Engineers, Geological 
Survey, U.S.Coast Guard, and National Geospatial-Intelligence Agency.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.s.Coast 
Guard Light List and U.S.Coast pilot for details.


RACING BUOYS
Racing buoys within the limits of this chart 
are not shown hereon. Information may be
obtained from the U.S.Coast Guard District 
Offices as racing and other private buoys are 
not all listed in the U.S.Coast Guard Light List.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


HURRICANES AND TROPICAL STORMS Note:
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.  Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.  Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


END OF FILE


