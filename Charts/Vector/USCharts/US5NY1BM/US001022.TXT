CAUTION Mariners are warned that numerous 
stakes and fishing structures some submerged
may exist in the fish trap areas. Some structures 
are not charted unless known to be permanent.
Fish Traps have been reported in Sandy Hook 
Bay outside the fish trap areas.
