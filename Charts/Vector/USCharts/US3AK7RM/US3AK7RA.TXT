NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3AK7RM - AMUKTA ISLAND TO IGITKIN ISLAND

INDEX:
AUTHORITIES
POLLUTION REPORTS
AIDS TO NAVIGATION
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
RADAR REFLECTORS
NOTE A
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION



NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the U.S. Coast Guard and Geo-
logical Survey


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR
153).


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to 
navigation.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


NOTE A
Navigation regulations are published in
chapter 2, U.S. Coast Pilot 9.Additions or
revisions to chapter 2 are published in the
Notice to Mariners. Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska or at the Office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particu-
larly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for 
details.


TIDAL INFORMATION
No tidal observations are available for the area 
covered by this chart.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE