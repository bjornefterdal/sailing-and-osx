NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5MA34M - CHATHAM HARBOR AND PLEASANT BAY

INDEX:
AUTHORITIES				
AIDS TO NAVIGATION				
NOTE A							
WARNING - THE PRUDENT MARINER	
POLLUTION REPORTS				
SUPPLEMENTAL INFORMATION		
CAUTION - TEMPORARY					
RADAR REFLECTORS			
NOAA WEATHER RADIO BROADCASTS		
COLREGS, 80.145 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION
ADMINISTRATION AREA

NOTES:
AUTHORITIES
Hydrography and Topography by the National Ocean Service, Coast Survey, with additional data from the Geological Survey and the U.S. Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.  


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 1. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Commander, 1st Coast Guard District in Boston, MA or at the Office of the District Engineer, Corps of Engineers in Concord, MA.
Refer to Charted regulation section numbers.  


WARNING - THE PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Cost Pilot for details. 


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 1 for important supplemental information.


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated. See Notice to Mariners.  During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed. For details see U.S. Coast Guard Light List.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed below provides continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Hyannis, MA	KEC-73	         162.55 MHz


COLREGS, 80.145 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.  The entire area of this chart falls seaward of the COLREGS Demarcation Line.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area.  This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone over which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.

END OF FILE