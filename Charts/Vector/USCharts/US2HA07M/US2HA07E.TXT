CAUTION NOTE - MARO REEF:
Much of Maro Reef is covered at all stages of tide. There are no highly visible objects by which a navigator
can determine his position. Recent hydrographic surveys do not exist in this area. The hydrographic surveys 
used for this chart did not achieve full bottom coverage, thus uncharted coral heads may exist. Uncharted 
areas of submerged reef of unknown depth may exist.Extreme caution should be exercised when navigating in this area.


END OF FILE