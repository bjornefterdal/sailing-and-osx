NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5RI10E - BLOCK ISLAND SOUND, POINT JUDITH TO MONTAUK POINT

INDEX:
AUTHORITIES		
AIDS TO NAVIGATION	
NOTE A
PRECAUTIONARY AREA			
WARNING - PRUDENT MARINER
POLLUTION REPORTS	
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
CAUTION - LIMITATIONS			
RADAR REFLECTORS	
SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
COLREGS, 80.145 & 80.150 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES 
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


AIDS TO NAVIGATION 
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.  


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
Concord, MA.
Refer to charted regulations section numbers.  


PRECAUTIONARY AREA
Traffic within the Precautionary Area may consist of vessels operating between
Narragansett Bay and one of the established traffic lanes. Mariners are advised
to exercise extreme care in navigating within the area.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.


POLLUTION REPORTS 
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION 
Consult U.S. Coast Pilot 2 for important 
supplemental information.


CAUTION - TEMPORARY 
Temporary changes or defects in aids to 
navigation are not indicated. See Local
Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


RADAR REFLECTORS 
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


NOAA WEATHER RADIO BROADCASTS 
The NOAA Weather Radio station listed 
below provide continuous weather broadcasts.  
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

New London, CT	  KHB-47	162.550 MHz
Providence, RI	  WXJ-39	162.400 MHz


COLREGS, 80.145 & 80.150 (see note A)
International Regulations for Preventing Collisions of Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE