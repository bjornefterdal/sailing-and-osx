NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NY1IE - SHELTER ISLAND SOUND AND PECONIC BAYS

INDEX:
AUTHORITIES			
AIDS TO NAVIGATION		
NOTE A				
WARNING - PRUDENT MARINER
POLLUTION REPORTS		
SUPPLEMENTAL INFORMATION	
CAUTION - TEMPORARY CHANGES
CAUTION - MARINERS
CAUTION - LIMITATIONS
CAUTION - DREDGED AREAS
CAUTION - CHANGES IN BUOYAGE	
RADAR REFLECTORS	
NOAA WEATHER RADIO BROADCASTS	
SUBMARINE PIPELINES AND CABLES
SMALL CRAFT WARNINGS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:

AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
New York, New York.
Refer to charted regulation section numbers. 


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


POLLUTION REPORTS 
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 2 for important
supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


CAUTION - MARINERS
Mariners are warned to stay clear of the pro-
tective riprap surrounding navigational light 
structures.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and
should be used with caution.


CAUTION - DREDGED AREAS
Improved channels are
subject to shoaling, particularly at the edges.


CAUTION - CHANGES IN BUOYAGE 
Mariners are advised that authorized aids to navigation are being changed to 
conform to maritime standards of the International Association of Lighthouse 
Authorities Maritime Buoyage System, Region B. Significant changes are: black 
port hand buoys to green ; black and white vertically striped buoys to red and white
vertically striped buoys; and lateral lights from white to red or green as appropriate.
Changes to aids to navigation will be announced in the National Geospatial-Intelligence 
Agency weekly Notice to Mariners and the U.S. Coast Guard Local Notice to
Mariners.


RADAR REFLECTORS 
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at
high elevations.

Meridan, CT	  WXJ-42	162.40 MHz
New London, CT	  KHB-47	162.55 MHz
Riverhead, NY	  WXM-80	162.475 MHz


SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


SMALL CRAFT WARNINGS
During the boating season small-craft 
warnings will be displayed from sunrise to 
sunset on New York Marine Police Cruisers 
while underway in the coastal and inland 
waters of Suffolk County, Long Island New 
York.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


END OF FILE
