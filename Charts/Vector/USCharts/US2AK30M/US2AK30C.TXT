
CABLES The symbols for submarine and overhead cables do not differentiate between cables conducting electric power, often at high voltages, and other types of utility cables. Mariners are advised to exercise caution when passing under all overhead cables and to avoid anchoring or conducting seabed operations in the vicinity of submarine cables. The clearance of an overhead cable may differ from its charted value due to changes in atmospheric conditions, water levels and other factors. For additional information, consult Notice No. 16 in the Notices to Mariners, Annual Edition and the appropriate volume of CHS Sailing Directions.

AIDS TO NAVIGATION For additional information concerning aids to navigation, consult larger scale charts, the Pacific Coast List of Lights, Buoys and Fog Signals and Radio Aids to Marine Navigation (Pacific and Western Arctic).

USA DUMPING GROUNDS For information on restricted dumping grounds in waters of the United States of America, the latest National Oceanic and Atmospheric Administration charts must be consulted.
�
USA NAVAL OPERATING AREAS For additional information concerning these areas, see United States of America Local Notice to Mariners.
�
VESSEL TRAFFIC SERVICES For information concerning these services, see Notice to Mariners No. 25 for each year.
�
FIRING PRACTICE AND EXERCISE AREAS For additional information concerning these areas, see Notice to Mariners No. 35 of each year.

EXERCISE AREA WP Surface and air firings may be conducted in any part of WP if appropriate clearances have been obtained. However, the majority of firings are carried out in two areas designated West Coast Firing Area North (WCFA North), and West Coast Firing Area South (WCFA South). Pecked lines enclose these areas. For information concerning firing operations call Tofino Traffic on VHF Channel 74.

� Fisheries and Oceans Canada 2012. Published by the Canadian Hydrographic Service. A licence is required from the Canadian Hydrographic Service (www.charts.gc.ca) to reproduce or distribute this work. 

In addition, this product contains U.S. Government works or data, and is published with the authorization of the Office of Coast Survey. No copyright is claimed by the United States Government under Title 17 U.S.C. with regard to their works or data contained in this product. Therefore no license is required from the U.S. Office of Coast Survey to reproduce or distribute U.S. data shown on this product.

Any international maritime boundary shown in the disputed area is without prejudice to the legal position of the United States or Canada.