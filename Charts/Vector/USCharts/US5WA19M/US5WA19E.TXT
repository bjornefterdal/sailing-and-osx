The Inland Navigational Rules Act of 1980 is in effect for
vessels transiting this area. The seaward boundaries of this
area are the COLREGS demarcation lines. In the area seaward
of the COLREGS demarcation lines, vessels are governed by
COLREGS: International Regulations for Prevention Collisions
at Sea, 1972. The COLREGS demarcation line is defined in
33 CFR 80.1395.

Navigation regulations are published in Chapter 
2, U.S. Coast Pilot 7. Additions or revisions to Chap-
ter 2 are published in the Notice to Mariners. Infor-
mation concerning the regulations may be obtained 
at the Office of the Commander, 13th Coast Guard 
District Seattle, Wash., or at the Office of the District 
Engineer, Corps of Engineers in Seattle, Wash.
Refer to charted regulation section numbers.


