NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5WA19E - PUGET SOUND - ENTRANCE TO HOOD CANAL


INDEX:
NOTE A
NOTE J
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
RADAR REFLECTORS
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS
WARNING - PRUDENT MARINER
CAUTION - IMPROVED CHANNELS
CAUTION - RIPRAP
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
POLLUTION REPORTS
NOAA VHF-FM WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION
REGULATED NAVIGATION AREA


NOTES: 
NOTE A
Navigation regulations are published in Chapter 
2, U.S. Coast Pilot 7. Additions or revisions to Chap-
ter 2 are published in the Notice to Mariners. Infor-
mation concerning the regulations may be obtained 
at the Office of the Commander, 13th Coast Guard 
District Seattle, Wash., or at the Office of the District 
Engineer, Corps of Engineers in Seattle, Wash.
Refer to charted regulation section numbers.

NOTE J
Floating security barriers have been installed at various
U.S. Naval installations throughout Puget Sound.  The barriers
are marked by numerous flashing yellow (Fl Y 2s) Navy maintained
lighted buoys and approximately mark the Restricted Areas
surrounding the facility.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important 
supplemental information.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.

CAUTION - LIMITATIONS
Limitations on the use of certain other radio 
signals as aids to marine navigation can be found in the U.S. 
Coast Guard Light Lists and National Imagery and Mapping
Agency Publication 117.
Radio direction-finder bearings to commercial broad-
casting stations are subject to error and should be used 
with caution.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.

CAUTION - IMPROVED CHANNELS
Improved channels
are subject to shoaling, particularly at the edges.

CAUTION - RIPRAP
Mariners are warned to stay clear of the 
protective riprap surrounding navigational light 
structures.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.

AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, and U.S. 
Coast Guard.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

NOAA VHF-FM WEATHER RADIO BROADCASTS
The National Weather Service stations listed 
below provide continuous marine weather broad-
casts. The range of reception is variable, but for
most stations is usually 20 to 40 miles from the
antenna site. 

Puget Sound, WA      WWG-24         162.425 MHz
Seattle, WA          KHB-60         162.55 MHz

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

REGULATED NAVIGATION AREA
A Regulated Navigation Area has been established by the U.S. Coast Guard.
Please see Chapter 2, U.S. Coast Pilot 7 or 33 CFR 165.1301 and 33 CFR
165.1303.


END OF FILE
