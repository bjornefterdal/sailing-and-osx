Cables

Vessel_Traffic_Services

Current_velocities

CABLES The symbols for submarine and overhead cables do not differentiate between cables conducting electric power, often at high voltages, and other types of utility cables.  Mariners are advised to exercise caution when passing under all overhead cables and to avoid anchoring or conducting seabed operations in the vicinity of submarine cables.  The clearance of an overhead cable may differ from its charted value due to changes in atmospheric conditions, water levels and other factors.  For additional information, consult Notice No. 16 in the Notices to Mariners, Annual Edition and the appropriate volume of CHS Sailing Directions.

VESSEL TRAFFIC SERVICES Traffic Services calling-in point with number; arrow indicates direction of vessel movement.  For additional information concerning these services, see Notice to Mariners No. 25 of each year.

Current velocities represent normal maximum rates at springs.  See Canadian Tide and Current Tables Vol. 7 for current information.

� Fisheries and Oceans Canada 2012. Published by the Canadian Hydrographic Service. A licence is required from the Canadian Hydrographic Service (www.charts.gc.ca) to reproduce or distribute this work. 

In addition, this product contains U.S. Government works or data, and is published with the authorization of the Office of Coast Survey. No copyright is claimed by the United States Government under Title 17 U.S.C. with regard to their works or data contained in this product. Therefore no license is required from the U.S. Office of Coast Survey to reproduce or distribute U.S. data shown on this product.

Any international maritime boundary shown in the disputed area is without prejudice to the legal position of the United States or Canada.