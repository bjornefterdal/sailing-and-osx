NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION
US4AK4SM - REVILLAGIGEDO CHANNEL


INDEX:
AUTHORITIES
NOTE A
AIDS TO NAVIGATION
CAUTION � TEMPORARY CHANGES
NOAA WEATHER RADIO BROADCASTS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
COLREGS, 80.1705 (see note A)
CAUTION - LIMITATIONS
CAUTION - SUBMARINE PIPELINES AND CABLES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION
MARITIME BOUNDARY


NOTES:
AUTHORITIES
Hydrography and Topography by the National Ocean Service, Coast Survey with additional data from the U.S. Coast Guard, and Canadian Hydrographic Service

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 8.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 17th Coast Guard District in Juneau, Alaska, or at the Office of the District Engineer, Corps of Engineers in Anchorage, Alaska.
Refer to charted regulation section numbers.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.
See Canadian List of Lights, Buoys and Fog Signals for information not included in the U.S. Coast Guard Light List.

CAUTION � TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.
Sukkwan I, AK		KZZ-89		162.425 MHz
Zarembo I, AK		KZZ-91		162.450 MHz
Gravina I, AK		KZZ-96		162.525 MHz
Duke I, AK		KZZ-92		162.450 MHz
Ketchikan, AK		WXJ-26		162.550 MHz


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 8 for important supplemental information.

COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972. The entire area of this chart falls seaward of the COLREGS Demarcation Line.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart.  Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling.
Covered wells may be marked by lighted or unlighted buoys.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.

TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

MARITIME BOUNDARY
Any international maritime boundary shown in the disputed area is without prejudice to the legal position of the United States or Canada.


END OF FILE
