NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4MA19E - PORTSMOUTH HARBOR TO BOSTON HARBOR


INDEX:
AUTHORITIES
AIDS TO NAVIGATION		
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
RADAR REFLECTORS
SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
MARINE WEATHER FORECASTS
CAUTION - WARNING CONCERNING LARGE VESSELS
CAUTION - LIMITATIONS
CAUTION - SMALL CRAFT
RULES OF THE ROAD (ABRIDGED)
CAUTION - MARINERS
RACING BUOYS
TIDAL INFORMATION 
ADDITIONAL INFORMATION


NOTES:

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 1. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners.  Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
Concord, MA.
Refer to Code of Federal Regulations section numbers. 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 1 for important supplemental information.

CAUTION
Temporary changes or defects in aids to
navigation are not indicated.  See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed.  For details
see U.S. Coast Guard Light List.

RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.

SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exists, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.

NOAA WEATHER RADIO BROADCASTS 
CITY			STATION		FREQUENCY		BROADCAST TIMES
Portland, ME	    	KDO-95		162.550	MHz		24 hours daily
Boston, MA		KHB-35		162.475 MHz 		24 hours daily
Essex Marine, MA	WNG-574	        162.425 MHz		24 hours daily
Stratham, NH		KZZ-40		162.450 MHz		24 hours daily

MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE	TELEPHONE NUMBER	OFFICE HOURS
Portland (Gray), ME		 (207) 688-3216		7:00 AM - 5:00 PM M-F
 				*(207) 688-3210		24 Hours daily
Boston/Taunton, MA		 (508) 828-2672		8:00 AM - 5:00 PM M-F
				*(508) 822-0634		24 Hours daily
New York/Upton, NY		 (516) 926-0517		9:00 AM - 5:00 PM M-F
						        Recorded forecast only
                                                        other times.

*Recorded

CAUTION - WARNING CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall 
not impede the passage of a vessel that can navigate only 
within a narrow channel or fairway. Large vessels may 
appear to move slowly due to their large size but actually
transit at speeds in excess of 12 knots, requiring a great 
distance in which to maneuver or stop.  A large vessel's 
superstructure may block the wind with the result that 
sailboats and sailboards may unexpectedly find themselves 
unable to maneuver. Bow and stern waves can be hazardous 
to small vessels. Large vessels may not be able to see small 
craft close to their bows.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
 Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.

CAUTION - SMALL CRAFT
Small craft should stay clear of large com-
mercial and government vessels even if small 
craft have the right-of-way.
  All craft should avoid areas where the skin 
divers flag, a red square with a diagonal white 
stripe, is displayed.

RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases. 
Sailing vessels and motorboats less than sixty-five feet in 
length shall not hamper, in a narrow channel, the safe 
passage of a vessel which can navigate only inside that
channel.
A motorboat being overtaken has the right-of-way.
Motorboats approaching head to head or nearly so should 
pass port to port.
When Motorboats approach each other at right angles or 
obliquely, the boat on the right has the right-of-way in most 
cases.
Motorboats must keep to the right in narrow channels, when 
safe and practicable.
Mariners are urged to become familiar with the complete text 
of the Rules of the Road in U.S.  Coast Guard publication 
"Navigation Rules".

CAUTION - MARINERS
Mariners are warned to stay clear of the pro-
tective riprap surrounding navigational light 
structures.

RACING BUOYS
Racing buoys within the limits of this chart
are not shown hereon. Information may be 
obtained from the U.S. Coast Guard District 
Offices as racing and other private buoys are 
not all listed in the U.S. Coast Guard Light List.

TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE