3.6 meters/12 feet from Carrabelle, FL to Brownsville, TX.
The controlling depths are published periodically 
in the U.S. Coast Guard Local Notice to Mariners.
Uncharted shoals may exist in areas which have
not been recently surveyed. Please report shoals
and obstructions at:


The general location of the Waterway is indicated 
by a magentaline. Mariners are advised to follow 
the aids to navigation and avoid charted shoals 
and obstructions.
Mileage distances along the Waterway 
are in Statute Miles,
