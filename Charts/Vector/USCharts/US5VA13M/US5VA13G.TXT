The Inland Navigational Rules Act of 1980 is in effect for vessels transiting this area. The
seaward boundaries of this area are the COLREGS demarcation lines. In the area
seaward of the COLREGS demarcation lines, vessels are governed by COLREGS:
International Regulations for Prevention Collisions at Sea, 1972. The COLREGS
demarcation line is defined in COLREGS 33 CFR 80.510.

Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 3. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
5th Coast Guard District in Portsmouth, Virginia or at the
Office of the District Engineer, Corps of Engineers in
Norfolk, Virginia.
Refer to charted regulation section numbers.
