NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4NY32M - NIAGARA RIVER AND WELLAND CANAL

INDEX:
AIDS TO NAVIGATION
AUTHORITIES
NOTE - A
SUPPLEMENTAL INFORMATION
40 CFR 140
POLLUTION REPORTS
WARNING - PRUDENT MARINER
RACING BUOYS
CAUTION - MARINERS ARE WARNED
CAUTION - SUBMARINE PIPELINES AND CABLES 
CAUTION - LOW WATER DATUM
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
NOAA VHF-FM WEATHER BROADCASTS
CAUTION - LIMITATIONS
BRIDGES AND OVERHEAD CABLE CLEARANCES
ADDITIONAL INFORMATION
COMMENTS REQUEST


NOTES
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List 
for supplemental information concerning aids to navigation. 
See Canadian List of Lights, Buoys, and Fog Signals 
for information not included in the U.S. Coast Guard Light List.

AUTHORITIES
Hydrography and topography by the National Ocean Service, 
Coast Survey, with additional data from the Corps of Engineers, 
Geological Survey, U.S. Coast Guard, and Canadian authorities.

NOTE - A
Navigation regulations are published in Chapter 2, 
U.S. Coast Pilot 6. Additions or revisions to Chapter 2 
are published in the Notices to Mariners. 
Information concerning the regulations may be obtained 
at the Office of the Commander, 9th Coast Guard District 
in Cleveland, Ohio, or at the Office of the District Engineer, 
Corps of Engineers in Buffalo, N.Y.
Refer to charted regulation section numbers.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.

40 CFR 140
Under the Clean Water Act, Section 312, all vessels operating within a No-Discharge 
Zone (NDZ) are completely prohibited from discharging any sewage, treated or 
untreated, into the waters. Commercial vessel sewage shall include graywater. 
All vessels with an installed marine sanitation device (MSD) that are navigating, 
moored, anchored, or docked within a NDZ must have the MSD disabled to prevent 
the overboard discharge of sewage (treated or untreated) or install a holding tank. 
Regulations for the NDZ are contained in the U.S. Coast Pilot. Additional information 
concerning the regulations and requirements may be obtained from the Environmental 
Protection Agency (EPA) web site: 
http://www.epa.gov/owow/oceans/regulatory/vessel_sewage/

POLLUTION REPORTS
Report all spills of oil and hazardous substances 
to the National Response Center via 1-800-424-8802 (toll free), 
or to the nearest U.S. Coast Guard facility 
if telephone communication is impossible (33 CFR 153). 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, 
particularly on floating aids. 
See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.

RACING BUOYS
Racing buoys within the limits of this chart are not shown hereon. 
Information may be obtained from the U.S. Coast Guard District Offices 
as racing and other private buoys 
are not all listed in the U.S. Coast Guard Light List.

CAUTION
Mariners are warned that numerous uncharted stakes and fishing structures, 
some submerged, may exist in the area of this ENC. 
Such structures are not charted unless known to be permanent

CAUTION - SUBMARINE PIPELINES AND CABLES
Gas pipelines and wells contain natural gas under pressure and damage to these installations 
would create an immediate fire  hazard. Vessels anchoring in Lake Erie should do so with caution  
after noting the underwater, and therefore concealed, positions of all oil and gas wells, 
pipelines, submarine cables and other installations.

Additional uncharted submarine pipelines and submarine cables may exist within the area.  
Not all submarine pipelines and submarine cables are required to be buried, and those that 
were originally buried may have become exposed.  Mariners should use extreme caution when 
operating vessels in depths of water comparable to their draft in areas where pipelines and 
cables may exist, and when anchoring, dragging or trawling.
Covered wells may be marked by lighted or unlighted buoys.

CAUTION - LOW WATER DATUM
Due to periodic high water conditions in the Great Lakes, 
some features charted as visible at Low Water Datum may be submerged, 
particularly in the near shore areas. Mariners should proceed with caution.

CAUTION - TEMPORARY CHANGES 
Temporary changes or defects in aids to navigation are not indicated. 
See Local Notice to Mariners.
During some winter months or when endangered by Ice, 
certain aids to navigation are replaced by other types or removed. 
For details see U.S. Coast Guard Light List.

CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.

NOAA VHF-FM WEATHER BROADCASTS
The National Weather Service station listed below 
provides continuous marine weather broadcasts. 
The range of reception is variable, 
but for most stations is usually 20 to 40 miles from the antenna site.

Buffalo, N.Y.	KEB-98	162.55 MHz (Chan, WX-1)

CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation 
can be found in the U.S. Coast Guard Light Lists and 
National Imagery and Mapping Agency Publication 117.
Radio direction - finder bearings to commercial broadcasting stations 
are subject to error and should be used with caution.

BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water Datum, 
bridge and overhead clearances are reduced correspondingly. 
For clearances see U.S. Coast Pilot 6.

ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.noaa.gov

COMMENTS REQUEST
This ENC has been designed to promote safe navigation. 
The Nautical Ocean Service encourages users to submit corrections, 
additions, or comments for improving this ENC to
the Chief, Marine Chart Division, (N/CS2),
National Ocean Service, NOAA
Silver Spring, Maryland 20910-3282.




