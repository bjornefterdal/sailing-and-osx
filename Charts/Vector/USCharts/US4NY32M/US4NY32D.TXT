The Corps of Engineers publishes the controlling depth periodically 
in the U.S. Coast Guard Local Notice to Mariners. 
For further information on channel depths, 
direct inquiries to the Office of the District Engineer, 
Corps of Engineers, Buffalo, NY.
http://www.lrb.usace.army.mil/Library/MapsandCharts.aspx