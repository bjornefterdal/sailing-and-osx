Numerous uncharted gas and oil well structures, pipes, piles, 
and stakes exist within the obstruction area indicated
by dashed magenta lines. Uncharted structures may exist 
outside the obstruction area.