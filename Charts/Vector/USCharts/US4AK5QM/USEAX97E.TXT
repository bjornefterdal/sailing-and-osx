NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK5QE - UGANIK AND UYAK BAYS

INDEX:

SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
NOTE A
CAUTION - TEMPORARY
NOAA WEATHER RADIO BROADCASTS 
POLLUTION REPORTS
COLREGS, 80.1705 (see note A)
CAUTION - SIGNIFICANT
AUTHORITIES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 8.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 17th Coast Guard District in Juneau, Alaska, or at the Office of the District Engineer, Corps of Engineers in Anchorage, Alaska.
Refer to charted regulation section numbers.


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


NOAA WEATHER RADIO BROADCASTS    
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.    

Raspberry I, AK 		KZZ-90 		162.425 MHz 
Pillar Mt, AK 			WNG-531 	162.525 MHz    
Kodiak, AK 			WXJ-78 		162.55 	MHz
Sitkinak Dome, AK		WNG-718		162.500 MHz
Cape Gull, AK			WNG-529		162.50	MHz


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


CAUTION - SIGNIFICANT
Significant changes in depths and shoreline may have occurred in the area of this chart as a result of the earthquake of March 27, 1964.  Tidal observations since the earthquake indicate bottom subsidence of  -3.7 feet at Uganik Bay.  Mariners are urged to use extreme caution when navigating in the area of this chart as the magnitude of change except at this site is not known.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey with additional data from the U.S. Coast Guard and National Geospatial-Intelligence Agency.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these
aids has been omitted.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE
