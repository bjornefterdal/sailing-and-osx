NATIONAL MARINE SANCTUARIES NOTE
National Marine Sanctuaries are protected areas, administered by NOAA
which contain abundant and diverse natural resources such as marine
mammals, seabirds, fishes, and tidepool invertebrates. These areas are
particularly sensitive to environmental damage such as spills of oil and other
hazardous materials, discharges, and groundings. Exercise particular caution
and follow applicable Sanctuary regulations when transiting these areas to
avoid environmental impacts. A full description of Sanctuary regulations may
be found in 15 CFR Part 922 and in Coast Pilot.

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
11th Coast Guard District in Alameda, California or at the
Office of the District Engineer, Corps of Engineers in
Los Angeles, California.
Refer to charted regulation section numbers.
