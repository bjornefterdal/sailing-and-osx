NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION


US5CA78E - SAN CLEMENTE ISLAND

INDEX:
AUTHORITIES
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
NOTE A
RADAR REFLECTORS
CAUTION - TEMPORARY CHANGES
NOTE C
WARNING - PRUDENT MARINER
AIDS TO NAVIGATION
ADMINISTRATION AREA
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the Corps of Engineers, and U.S.
Coast Guard.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR
153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important supplemental information. 


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7.  Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners.  Information concerning the
regulations may be obtained at the Office of the Commander,
11th Coast Guard District in Alameda, California or at the
Office of the District Engineer, Corps of Engineers in
Los Angeles, California.
Refer to charted regulation section numbers.


RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation.  Individual radar
reflector identification on these aids has been
omitted from this chart.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


NOTE C
Submerged submarine operations are
conducted at various times in the waters
contained on this cell. Proceed with caution.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area.
This area covers land, internal waters, and territorial sea.  The territorial sea is a
maritime zone which the United States exercises sovereignty extending to the airspace as
well as to its bed and subsoil.  For more information, please refer to the Coast Pilot


TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.gov.


END OF FILE
