Navigation regulations are published in 
Chapter 2, Coast Pilot 7, or weekly Notice 
to Mariners which include new or revised 
regulations.  Additional Information may be 
obtained from the Superintendent, Lake Mead 
National Recreational Area, National Park 
Service.
Refer to section numbers shown with area 
designation.