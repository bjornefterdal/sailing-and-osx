NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5CA99E - LAKE MEAD

INDEX:
SOUNDING DATUM
LAKE LEVELS, WATER DEPTHS AND ELEVATIONS
AUTHORITIES
NOTE A
CAUTION
WARNING - PRUDENT MARINER
CAUTION
MARINE WEATHER INFORMATION
RULES OF THE ROAD (ABRIDGED)
POLLUTION REPORTS
RADAR REFLECTORS
REEF MARKERS (CAUTION! ALL REEFS ARE NOT MARKED)
AIDS TO NAVIGATION
HOOVER DAM
ADMINISTRATION
TIDAL INFORMATION
ADDITIONAL INFORMATION

NOTES:

SOUNDING DATUM
Soundings refer to a normal lake level elevation which is 1160 feet
above Mean Sea Level.


LAKE LEVELS, WATER DEPTHS AND ELEVATIONS
The 1100 foot elevation (60 foot depth curve) represents a low lake level.
The 1160 foot contour represents the normal lake level.
The 1200 foor contour represents a high lake level.
The areas between the coastline and the 60 foot depth contour represents
elevations between 1100 and 1160 feet above Mean Sea Level.
The marsh areas represent elevations between 1160 and 1200 feet above
Mean Sea Level.
Contour values indicate elevations in feet above Mean Sea Level.


AUTHORITIES
Surveys by the Geological Survey, Soil Conservation, and the 
National Ocean Service, Coast Survey.


NOTE A
Navigation regulations are published in 
Chapter 2, Coast Pilot 7, or weekly Notice 
to Mariners which include new or revised 
regulations.  Additional  Information may be 
obtained from the Superintendent, Lake Mead 
National Recreational Area, National Park 
Service.
Refer to section numbers shown with area 
designation.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any 
single aid to navigation, particularly on floating 
aids.  See U.S. Coast Guard Light List and U.S. 
Coast Pilot for general information, and 
Superintendent, Lake Mead National 
Recreational area, National Park Service, for 
specific details.


CAUTION
Frequent changes in aids to navigation can 
be expected because of fluctuation in lake level.  
Current information may be obtained at the Park 
Headquarters or Ranger Office.


MARINE WEATHER INFORMATION
The National Park Service communications center makes twice daily 
weather broadcasts of weather conditions in the Lake Mead area.  The 
broadcasts are on marine VHF station KOJ 719 channel 22A (157.1 
MHz) located at 35�58'40"N., 114�50'15"W.  With prior notice of 
the broadcasts being made on marine safety and emergency channel 
16 (156.8 MHz) so that vessels may switch to channel 22A and listen 
to the broadcasts.


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases. Sailing 
vessels and motorboats less than sixty-five feet in length, shall not 
hamper, in a narrow channel, the safe passage of a vessel which 
can navigate only inside that channel.
A motorboat being overtaken has the right-of-way.
Motorboats approaching head to head or nearly so should pass 
port to port.
When Motorboats approach each other at right angles or 
obliquely, the boat on the right has the right-of-way in most 
cases.
Motorboats must keep to the right in narrow channels, when safe 
and practicable.
Mariners are urged to become familiar with the complete text of 
the Rules of the Road in U.S. Coast Guard publication 
"Navigation Rules".
For emergencies call KOJ719 (National Park Service) on marine 
band channel 16.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Park 
Service (702) 293-8932.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


REEF MARKERS (CAUTION! ALL REEFS ARE NOT MARKED)
Numerous reefs, many of which are submerged, are marked by spar
buoys. These are painted white with orange bands at top and bottom and
an orange diamond. Buoys mark the edges of reefs.


AIDS TO NAVIGATION
Aids to Navigation on Lake Mead are maintained by the National Park Service.
Lights are two types, fixed and movable. Fixed lights are permanently located above
any probable lake level. Movable lights are located when the lake height requires.


HOOVER DAM
The boulder Canyon Project Act, passed in 1928, authorized the Bureau
of Reclamation, U.S. Department of the Interior, to construct Hoover Dam. Work began in 
1931, and the structure was dedicated in 1935. It is the tallest dam in the Western
Hemisphere, rising 726.4 feet from the base rock to the roadway on top. Its crest is 1,244 feet
long.


ADMINISTRATION
Lake Mead National Recreation Area, established October 8, 1964, is administered by the
National Park Service, U.S. Department of the Interior.
The National Park System, of which this area is a unit, is dedicated to conserving the
scenic, scientific, historic, and recreational heritage of the United States for the benefit and
enjoyment of its people.
A Superintendent, whose address is 601 Nevada Highway, Boulder City, Nevada 89005,
is in immediate charge.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE
