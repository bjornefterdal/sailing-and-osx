The channel DRVAL1 reflects the Corps of Engineers project depth.  
The Corps of Engineers publishes the controlling depth periodically 
in the U.S. Coast Guard Local Notice to Mariners.  For further 
information on channel depths, direct inquires to the office of the 
District Engineer, Corps of Engineers, Detroit, Michigan.                                                            