Numerous uncharted and dangerous sub-
merged boulders exit in the eastern portion of
Cook Inlet. Mariners should use extreme
caution in this area.