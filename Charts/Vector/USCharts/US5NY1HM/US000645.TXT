Navigation Regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub-
lished in the Notices to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, Mass., or at the 
Office of the District Engineer, Corps of Engineers in New 
York, N.Y.
Refer to charted regulation section numbers.




















