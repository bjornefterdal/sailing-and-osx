A 2-mile-wide restricted area extends from the northern limits of the
Narragansett Bay Approach traffic separation zone to 41�24'42". This
restricted area within the precautionary area will only be closed to vessel
traffic during periods of daylight and optimum weather conditions for
torpedo range use. Consult Chapter 6, U.S. Coast Pilot 2, for additional
information.
