NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK9OE - SKULL CLIFF AND VICINITY

INDEX:

AUTHORITIES
AIDS TO NAVIGATION
NOTE A
WARNING - PRUDENT MARINER
CAUTION - LIMITATIONS
CAUTION - TEMPORARY CHANGES
CAUTION - DEPTHS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
COLREGS, 80.1705 (see note A)
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION

NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, 
Coast Survey.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.  

NOTE A
Navigation regulations are published in
Chapter 2, U.S. Coast Pilot 9.  Additions or
revisions to Chapter 2 are published in the
Notice to Mariners.  Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska, or at the Office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners

CAUTION - DEPTHS
Depths may vary as much as 6 feet due to 
iceberg groundings.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication is impossible (33 CFR 
153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.


COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts.  
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Barrow, AK	KZZ-53		162.550 MHz

TIDAL INFORMATION  
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


END OF FILE