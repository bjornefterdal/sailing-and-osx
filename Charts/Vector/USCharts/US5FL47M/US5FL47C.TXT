For the protection of wildlife, all keys in Florida Bay
portion of Everglades National Park are closed to landing 
except those marked as designated camping areas.  A 
backcountry use permit is required for overnight camping 
and can be obtained at park Ranger Stations.
The killing, collecting, or molesting of animals, the 
collecting of plants, and waterskiing are prohibited by 
Federal Regulation.
