NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US514974 - ASHLAND AND WASHBURN HARBORS

INDEX:
PLANE OF REFERENCE
AIDS TO NAVIGATION
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION
RADAR REFLECTORS
CAUTION
CAUTION
CAUTION
ADDITIONAL INFORMATION
TIDAL INFORMATION


PLANE OF REFERENCE (Low Water Datum)..........601.1 ft.: Referred to mean water level at Rimouski, Quebec, International Great Lakes Datum (1985).

AIDS TO NAVIGATION: Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

SYMBOLS AND ABBREVATIONS: For complete list of symbols and abbreviations see Chart no. 1.

AUTHORITIES: Hydrography and topography by the National Ocean Service, Geological Survey, and U.S. Coast Guard. 

NOTE A: Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 9th Coast Guard District in Cleveland, Ohio or at the Office of the District Engineer, Corps of Engineers in Detroit, Michigan.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER: The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids.  See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.

POLLUTION REPORTS: Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: Consult U.S. Coast Pilot 6 for important supplemental information.

CAUTION: Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed.  For details see U.S. Coast Guard Light List.

CAUTION: Due to periodic high water conditions in the Great Lakes, some features charted as visible at Low Water Datum may be submerged, particularly in the near shore areas.  Mariners should proceed with caution.

RADAR REFLECTORS: Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector identification on these aids has been omitted.

CAUTION: Mariners are warned that numerous uncharted stakes and fishing structures, some submerged, may exist in the
area of this chart. Such structures are not charted unless known to be permanent.

CAUTION: Improved channels shown by broken lines are subject to shoaling, particulaly at the edges.

CAUTION: Limitations on the use of certain other radio signals as aids to marine navigation can be found in the U.S. 
Coast Guard Light Lists and National Geospatial-Intelligence Agency.
Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with 
caution.

ADDITIONAL INFORMATION:
Additional information can be obtained at www.nauticalcharts.noaa.gov

TIDAL INFORMATION:
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov



END OF FILE