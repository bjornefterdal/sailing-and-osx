PRECAUTIONARY AREA 
Traffic within the Precautionary Area consists of vessels maneuvering on various courses. 
Vessels transiting the Precautionary Area should, when possible, keep the centerline of the area to port 
providing for a counterclockwise movement of vessels within the area. Mariners are advised to use extreme 
caution when navigating within this area.