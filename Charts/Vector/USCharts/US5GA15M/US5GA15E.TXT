Most features, including bathymetry, are omitted in this area.
The minimal depiction of detail in this area does not support safe navigation.
Use only with local knowledge.