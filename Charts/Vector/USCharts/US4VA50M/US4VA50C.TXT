DELAWARE BAY APPROACH TRAFFIC SEPARATION SYSTEM (33 CFR 167.170 - 167.174.  
The Traffic Separation Scheme is RECOMMENDED for use by 
all vessels. They have been designed to aid in the prevention of collisions 
at the approaches to Delaware Bay, but are not intended in any way to 
supersede or alter the applicable Rules of the Road. Separation zones are 
intended to separate inbound and outbound traffic and to be free of ship 
traffic. Separation zones should not be used except for crossing Purposes, 
When crossing traffic lanes and separation zones use extreme caution. 

Vessels arriving at the Delaware Bay Entrance are advised to use the 
Delaware sea-lane, (Southeastern Approach), or the Five-Fathom sea-lane 
(Eastern Approach). 


The controlling depth within the Five Fathom Bank to Cape Henlopen
inbound traffic lane is 12.1 meters/40 feet. The Marine Advisory Committee recommends
that vessels with drafts of 10.6 meters/35 feet or greater use the Delaware to Cape
Henlopen inbound traffic lane.
The controlling depth within the Cape Henlopen to Five Fathom Bank
outbound traffic lane is 13.1 meters/ 43 feet.


Upon entering the appropriate sea-lane, vessels are advised to contact 
the voluntary vessel traffic information service through the Delaware 
Pilot traffic tower on VHF-FM channel 14.
Inbound towing traffic should contact the tower when off of McCrie 
Shoal Lighted Gong Buoy 2MS. Outbound vessels are requested to contact 
the tower when passing the Brown Shoal lighted buoy "WR9", or Tanker 
Anchorage Approach lighted buoy "A".


Traffic within the Precautionary Area may consist of vessels 
operating between Delaware Bay and one of the established 
traffic lanes. Mariners are advised to exercise extreme care 
in navigating within this area. The Pilot Boarding Area, 
located within the precautionary area, is bounded 
by lighted buoys"3HC", "DF", "5", "6".