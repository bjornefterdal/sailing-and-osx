Both high speed and traditional ferries operate in
the San Francisco Bay. Mariners are cautioned that
high speed craft move very rapidly and may transit
waterways at angles to the normal direction of
traffic. Ferries may deviate from these routes if
necessary. Mariners should exercise caution when
transiting between the origin or terminus of a
charted ferry route and the actual ferry docking
facility. Go to www.sfmx.org for additional
information on the Ferry Traffic Routing Protocol.
