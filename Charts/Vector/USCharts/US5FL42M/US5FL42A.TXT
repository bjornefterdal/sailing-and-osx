NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION 

US5FL42M - FORT MYERS TO CHARLOTTE HARBOR AND WIGGINS PASS

INDEX:
AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
CAUTION  TEMPORARY CHANGES
WARNING  PRUDENT MARINER
ADDITIONAL INFORMATION
AUTHORITIES
NOTE A
SUPPLEMENTAL INFORMATION
RADAR REFLECTORS
CAUTION  SMALL CRAFT
CAUTION  WARNING CONCERNING LARGE VESSELS
CAUTION  TEMPORARY CHANGES
CAUTION  DREDGED AREAS
RULES OF THE ROAD (ABRIDGED)
NOAA WEATHER RADIO BROADCASTS
MARINE WEATHER FORECASTS
MARINE WEATHER FORECASTS (NATIONAL WEATHER SERVICE)
BROADCASTS OF MARINE WEATHER 
HURRICANES AND TROPICAL STORMS
SUBMARINE PIPELINES AND CABLES
INTRACOASTAL WATERWAY AIDS
OKEECHOBEE WATERWAY AIDS
TIDAL INFORMATION
ADMINISTRATION AREA
ACKNOWLEDMENT


NOTES:


AIDS TO NAVIGATION 
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION  USE OF RADIO SIGNALS (LIMITATIONS) 
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


CAUTION  TEMPORARY CHANGES 
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


WARNING  PRUDENT MARINER 
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION 
Additional information can be obtained at www.nauticalcharts.noaa.gov.


AUTHORITIES 
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.


NOTE A: 
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 7th Coast Guard District in Miami, Florida, or at the Office of the District Engineer, Corps of Engineers in Jacksonville, Florida.
Refer to Code of Federal Regulations section numbers. 


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION  SMALL CRAFT
Small craft should stay clear of large commercial and government vessels even if small craft have right-of-way.
All craft should avoid areas where the skin divers flag, a red square with a diagonal white stripe, is displayed.


CAUTION  WARNING CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall not impede the passage of a vessel that can navigate only within a narrow channel or fairway.  Large vessels may appear to move slowly due to their large size but actually transit at speeds in excess of 12 knots, requiring a great distance in which to maneuver or stop. A large vessel's superstructure may block the wind with the result that sailboats and sailboards may unexpectedly find themselves unable to maneuver. Bow and stern waves can be hazardous to small vessels. Large vessels may not be able to see small craft close to their bows.


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases. Sailing vessels and motorboats less than sixty-five feet in length, shall not hamper, in a narrow channel, the safe passage of a vessel which can navigate only inside that channel.
A motorboat being overtaken has the right-of-way.
Motorboats approaching head to head or nearly so should pass port to port.
When motorboats approach each other at right angles or obliquely, the boat on the right has the right-of-way in most cases.
Motorboats must keep to the right in narrow channels, when safe and practicable.
Mariners are urged to become familiar with the complete text of the Rules of the Road in U.S. Coast Guard publication "Navigation Rules".


NOAA WEATHER RADIO BROADCASTS
CITY 		  STATION		FREQ. MHz	BROADCAST TIMES
Fort Myers,       FLWXK-83		162.475		24 Hours daily
Srarsota, FL  	  WWG-59		162.400		24 Hours daily
Naples, FL        WWG-92		162.525		24 Hours daily


MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE	TELEPHONE NUMBERS 	OFFICE HOURS
Melbourne, FL			(321) 255-0212	 	8:00 AM-4:00 PM (Mon-Fri)
Miami, FL		 	(305) 229-4522	 	24 Hours daily
Key West, FL			(305) 295-1316	 	24 Hours daily
Tampa, FL			*(813) 645-2506   	8:00 AM-4:00 PM (Mon-Fri)

*Recorded


MARINE WEATHER FORECASTS BY RADIO DIRECTLY FROM NATIONAL WEATHER SERVICE
CITY	    	STATION 	 FREQ.	   AM-LOCAL TIME       PM-LOCAL TIME	  DAY
Key West.	WKIZ		 1500 kHz  5:25, 7:15, 11:15   12:15, 5:15, 6:15  Daily
Key West.	WKWF   		 1600kHz   5:25, 7:15, 11:15   12:15, 5:15, 6:15  Daily


BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS 
BY MARINE RADIOTELEPHONE STATIONS
CITY	    	STATION 	FREQ.	   	DAILY BROADCAST-EST	  SPECIAL WARNINGS
Petersburg	NMA-21		*2670 kHz	9:20 AM & 10:20 PM   	  On receipt
				+157.1 MHz	8:00 AM & 6:00 PM         On receipt

+Preceded by announcement on 2182 kHz		Distress calls for small craft are made on 2182 kHz or

*Preceded by announcement on 156.8 MHz		channel 16 (156.80 MHz) VHF.


HURRICANES AND TROPICAL STORMS 
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations. Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved. Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


CAUTION  SUBMARINE PIPELINES AND CABLES 
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


INTRACOASTAL WATERWAY AIDS
The U.S. Aids to Navigation System is designed for use with nautical charts, and the exact meaning of an aid to navigation may not be clear unless the appropriate chart is consulted. Aids to navigation marking the Intracoastal Waterway exhibit; unique yellow symbols to distinguish them from aids marking other waterways.
When following the Intracoastal Waterway westward form the Caloosahatchee River to Anclote, FL, aids with yellow triangles should be kept on the starboard side of the vessel and aids with yellow squares should be kept on the port side of the vessel.
A horizontal yellow band provides no lateral information, but simply identifies aids to navigation as marking the Intracoastal Waterway.


OKEECHOBEE WATERWAY AIDS
The U.S. Aids to Navigation System is designed for use with nautical charts and the exact meaning of an aid to navigation may not be clear unless the appropriate chart is consulted.
Aids to navigation marking the Okeechobee Waterway exhibit unique yellow symbols to distinguish them from aids marking other waterways.
When following the Okeechobee Waterway westward from St. Lucie inlet to Fort Myers, Florida, aids with yellow triangles should be kept on the starboard side of the vessel and aids with yellow squares should be kept on the port side of the vessel.
A horizontal yellow band provides no lateral information, but simply identifies aids to navigation as marking the Okeechobee Waterway.


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil.  For more information, please refer to the Coast Pilot.


ACKNOWLEDMENT
The National Ocean Service acknowledges the exceptional cooperation received from members of the Cape Coral, Sanibel-Captiva and San Carlos Bay Power Squadrons, District 22, United States Power Squadrons for continually providing essential information for revising this chart.


END OF FILE
