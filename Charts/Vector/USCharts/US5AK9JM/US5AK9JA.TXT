NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK9JM - CAPE HALKETT AND VICINITY

INDEX:
AUTHORITIES			
AIDS TO NAVIGATION
SUPPLEMENTAL INFORMATION			
NOTE A						
WARNING - PRUDENT MARINER		
POLLUTION REPORTS			
CAUTION - TEMPORARY CHANGES
CAUTION - MARINERS
CAUTION - DEPTHS	
COLREGS, 80.1705 (see note A)
SUBSISTENCE WHALING IN THE BEAUFORT SEA
RACON
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES
Hydrography (from surveys of 1951-53) and Topography by the National 
Ocean Service, Coast Survey, with additional data from the State of Alaska, 
Geological Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.

NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 9, Additions or 
revisions to Chapter 2 are published in the 
Notice to Mariners. Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District
Engineer, Corps of Engineers in Anchorage
Alaska.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely 
on any single aid to navigation, particularly 
on floating aids. See U.S. Coast Guard Light 
List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners.


CAUTION - MARINERS
Mariners are advised that in the shallow waters of 
the Beaufort Sea, water levels are strongly influenced by 
meteorological conditions.  Strong offshore winds can 
produce water depths up to 0.8 meters (2.6 feet) less 
than those shown.

CAUTION - DEPTHS
Depths may vary as much as 6 feet due to iceberg groundings.

COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.

SUBSISTENCE WHALING IN THE BEAUFORT SEA
Mariners should be aware that Alaskan Natives engage in 
subsistence whaling in the Beaufort Sea from August 15 
to October 31. Vessel operators are requested to contact 
the Alaska Eskimo Whaling Commission at (907) 852-2392, 
or aewcdir@barrow.com prior to entering this area for infor-
mation about the location and avoidance of traditional Native 
hunting parties.

RACON
Radar Transponder Beacons, or RACONS, are 
activated by radars operating on the X-Band, frequencies 
9300 to 9450 MHz and, when activated, will emit an 
international morse code character which will be visible 
on the radar screen that activated the RACON. The 
effective range of the RACONS will be from 11 to 27 miles.
The RACONS will be maintained seasonally from 
1 July to 15 September.

RADAR REFLECTORS
Radar reflectors have been paced on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

END OF FILE