DANIA CUT-OFF CANAL
In Dania Cut-off Canal, the lowest reported depths were 0.6 meters/2 feet to the U.S. 1 Highway Bridge; thence 1.5 meters/5 feet to a point in 26�03'35"n, 80�08'06"w.
Fixed overhead crossings have reported minimum clearances: Horizontal Clearance 8.8 meters/29 feet; Vertical Clearance 3 meters/10 feet.
