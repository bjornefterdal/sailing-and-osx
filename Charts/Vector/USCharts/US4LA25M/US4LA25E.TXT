Uncharted platforms, gas and oil well structures, 
pipes, piles and stakes exist within the obstruction
areas.
Additionally, uncharted platforms, gas and oil well
structures, pipes, piles and stakes can exist
outside the outlined obstruction areas, and within
the limits of this chart.
