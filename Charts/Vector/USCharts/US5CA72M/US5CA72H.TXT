Floating security barriers have been installed at 
various U.S. Naval installations within San Diego 
Bay. The barriers are marked by numerous quick 
flashing yellow (Q Y) lighted buoys and positioned 
within the Security Zones surrounding the facility.