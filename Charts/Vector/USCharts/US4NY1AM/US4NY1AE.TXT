Area is open to unrestricted surface navigation but 
all vessels are cautioned neither to anchor, dredge, 
trawl, lay cables, bottom nor conduct any other simi-
lar type of operation because of residual danger from 
mines on the bottom.
