SAILING DIRECTIONS: 
Bearings of sailing courses are true and distances given thereon are in statute and nautical miles
between points of departure.  The true bearing between any two points on this chart may be determined
by connecting the two points with a straight line and measuring the angle of its intersection with a
meridian line.

Note: Sailing courses and limits are recommended by the Lake Carriers Association and the
Canadian Shipowners Association.
