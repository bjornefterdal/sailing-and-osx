NATIONAL GEOSPATIAL-INTELLIGENCE AGENCY
US509860 - Panama Canal - PUERTO CRISTOBAL
 
INDEX:
GLOSSARY
POSITIONS
TIDAL INFORMATION
PILOTAGE
MAXIMUM DRAFT OF VESSELS
REGULATIONS AND PROVISIONS
DATUM NOTE
ADDITONAL INFORMATION
CAUTION 1
CAUTION 2
CAUTION 3

NOTES: 
GLOSSARY:
Bahia ... bay
Canal ... channel
Isla(s), Islote ... island(s)
Punta ... cape, point
Puerto ... harbor
Rio ... stream

POSITIONS:
This ENC cell has been positioned on WGS-84 datum through the use of Rectified Ortho-photographs.

TIDAL INFORMATION:
Height above datum of soundings (MLLW) in meters; Cristobal, Mean Higher High Water 0.3; Mean Lower High Water 0.1; Mean Higher Low Water 0.0; Mean Lower Low Water 0.0.

PILOTAGE:
Pilotage is compulsory for the Panama Canal. The Pilot(s) assigned to a vessel shall have control of the vessel's navigation and movement. Except as provided by Canal Maritime Regulations or when exempted by the Canal Operations Captain or his designee, no vessel shall pass through the canal, enter or leave a terminal port, or maneuver within Canal operating waters without having a Panama Canal Authority pilot on board. This section applies from buoy 1 (9:21:18 N, 79:55:06 W) in Cristobal Harbor south to buoys 1 and 2, Pacific Entrance Channel.

MAXIMUM DRAFT OF VESSELS:
The maximum draft of vessels transiting the canal depends in part on the depth of water in Gatun Lake. Since the depth of Gatun Lake varies with the seasons, the latest information on draft restrictions should be consulted before a deep draft passage.

REGULATIONS AND PROVISIONS:
The regulations and provisions concerning vessel transits through the Panama Canal are contained in the Regulation for Navigation in Canal Waters, published by the Panama Canal Authority.

DATUM NOTE:
Mean Lake Level (MLL) is a Precise Level Datum (PLD); the level surface to which all heights or elevations is referred. For the Panama Canal, the 0.00 PLD adopted was mean sea level as determined at pre-construction time. Atlantic Mean Low Water (MLW) equals -0.12 meters PLD; Pacific Mean Low Water Springs (MLWS) equals -2.32 meters PLD. Gatun Mean Lake Level equals 25.91 meters PLD; and Miraflores Mean Lake Level equals 16.46 meters PLD.  

ADDITONAL INFORMATION:
Mariners are encouraged to log on to the Panama Canal Web site at www.pancanal.com for additional information on history, news, and canal operations.

CAUTION 1:
Temporary changes or defects in aids to navigation are not indicated in this cell.

CAUTION 2:
This cell is based upon the latest available information. Mariners are warned that canal depths, channel limits, lock restrictions, area boundaries and aids to navigation may be temporarily moved, displaced or changed without notice due to the ongoing program of maintenance, modernization and improvements.

CAUTION 3:
Depths in the harbor, beyond the 10 meter contour, are generally 0.5-2 meters less than charted.

END OF FILE
