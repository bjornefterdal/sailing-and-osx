NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5CA75E - SANTA BARBARA ISLAND

INDEX:
AIDS TO NAVAGATION
SUPPLEMENTAL INFORMATION
NOTE A
WARNING - PRUDENT MARINER
AUTHORITIES
CAUTION - TEMPORARY CHANGES
POLLUTION REPORTS
TIDAL INFORMATION
ADDITIONAL INFORMATION
ADMINISTRATION AREA

NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important 
supplemental information.

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
11th Coast Guard District in Alameda, California or at the 
Office of the District Engineer, Corps of Engineers in 
Los Angeles, California.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the U.S. Coast Guard, Geological 
Survey, and National Imagery and Mapping Agency.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners. 

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov. 
 
ADDITIONAL INFORMATION 
Additional information can be obtained at www.nauticalcharts.noaa.gov

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. 
This area covers land, internal waters, and territorial sea.  The territorial sea is a 
maritime zone which the United States exercises sovereignty extending to the 
airspace as well as to its bed and subsoil.  For more information, please refer to the 
Coast Pilot.

END OF THE FILE