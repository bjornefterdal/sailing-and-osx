NOTE D - NEW RIVER
The controlling depth at mean lower low water from the Intracoastal Waterway to Jacksonville, NC was reported at 1.5 meters/5 feet for a width of 21.3 meters/70 feet.
July 1993-Feb 2002.
