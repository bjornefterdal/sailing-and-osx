Mariners should be aware of a tidal bore beginning in the vicinity of Goose Bay that
heads northward up Knik Arm at a velocity of 2-3 knots and an average height of 0.4 meters/18
inches.  In addition, it is noted that this area is highly susceptible to shifting mud flats and
continual change.