NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3SP40E - APPROACHES TO JOHNSTON ATOLL

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
AIDS TO NAVIGATION
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION
RADAR REFLECTORS
SUBMARINE PIPELINES AND CABLES

SYMBOLS AND ABBREVIATIONS: For Symbols and Abbreviations see Chart No. 1.

AUTHORITIES: Hydrography and topography by the National Geospatial-Intelligence Agency, with additional
data from the National Ocean Service, Coast Survey and U.S. Coast Guard.

AIDS TO NAVIGATION: Consult U.S. Coast Guard Light List for supplemental information concerning aids to 
navigation.

WARNING � PRUDENT MARINER: The prudent mariner will not rely solely on any single aid to navigation, 
particularly on floating aids.  See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS: Report all spills of oil and hazardous substances to the National Response Center 
via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication 
is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: Consult National Geospatial-Intelligence Agency Hydrographic Center H.O. 
Pub. 126 for important supplemental information.

CAUTION: Temporary changes or defects in aids to navigation are not indicated on this chart. See Local
Notice to Mariners.

CAUTION: Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast 
Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.Radio direction-finder bearings
to commercial broadcasting stations are subject to error and should be used with caution.

RADAR REFLECTORS: Radar reflectors have been placed on many floating aids to navigation.  Individual radar 
reflector identification on these aids has been omitted.

SUBMARINE PIPELINES AND CABLES: Additional uncharted submarine pipelines and submarine cables may exist within
the area.  Not all submarine pipelines and submarine cables are required to be buried, and those that were 
originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in 
depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, 
dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


END OF FILE