PROHIBITED AREA
Johnston Atoll is a National Wildlife Refuge. The administration of the atoll is split between 
the U.S. Air Force and the U.S. Fish and Wildlife Service (USFWS). Entry onto the atoll is 
prohibited unless authorized by a USFWS permit.