NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5WA20E - NEAH BAY

INDEX:
AIDS TO NAVIGATION
NOTE A
NOTE F
SUPPLEMENTAL INFORMATION
CAUTION -TEMPORARY
NOAA WEATHER RADIO BROADCASTS
POLLUTION REPORTS
COLREGS, 80.1385 (SEE NOTE A)
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS
REGULATED NAVIGATION AREA


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Com-
mander, 13th Coast Guard District in Seattle, WA, or at the
Office of the District Engineer, Corps of Engineers in Seattle,
WA.
Refer to charted regulation section numbers.


NOTE F
Submerged submarine operations are conducted at various times in the waters contained on this chart. Proceed with caution. 

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important sup-
plemental information.


CAUTION - TEMPORARY
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at
high elevations.

Neah Bay, WA	  KIH-36	162.55 MHz


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR
153).


COLREGS, 80.1385 (SEE NOTE A)
International Regulations for Preventing Collisions at Sea, 1972.  
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging or trawling.
Covered wells may be marked by lighted or
unlighted buoys.  


AUTHORITIES
Hydrography and topography by the National
Ocean Service, Coast Survey, with additional
data from the Corps of Engineers and U.S.
Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to
navigation, particularly on floating aids. See U.S. Coast
Guard Light List and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to
http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identificaton on these aids has been
omitted.


REGULATED NAVIGATION AREA
A Regulated Navigation Area has been established by the U.S. Coast Guard.
Please see Chapter 2, U.S. Coast Pilot 7 or 33 CFR 165.1301 and 33 CFR
165.1303.


END OF FILE