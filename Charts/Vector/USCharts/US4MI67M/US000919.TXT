NOTE D
Mariners are warned that numerous uncharted stakes and fishing structures, 
some submerged, may exist in the area of this chart. Such structures are 
not charted unless known to be permanent.