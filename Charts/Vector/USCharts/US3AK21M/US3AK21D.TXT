Unusual currents may be encountered 
in the area east of Seal Rocks. 
Currents in this area usually run East to West, 
regardless of the tide. 
When the wind is blowing from the East and the tide is ebbing, 
there is a strong set in the direction of Seal Rocks. 
Mariners are urged to navigate the area with caution.