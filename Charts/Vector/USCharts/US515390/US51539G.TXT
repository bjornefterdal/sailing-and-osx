AREA NO.5; LOCATION CHANNEL SIDE:  Pena Buena Vista, W; BETWEEN BUOYS: 40-42; BOTTOM TYPE:  Mud; WATER DEPTH:  12.20 meters; GOOD FOR VESSEL SIZE:  Medium�

Vessel Size: Small - Up to 91.4m long with a draft up to 5.49m; Medium - From 91.44m to 152.40m long with a draft from 5.49m to 9.14m; Large - Over 152.40m with a draft of 9.14m or over