AREA NO.6; LOCATION CHANNEL SIDE:  Buena Vista, W; BETWEEN BUOYS:  44a-46; BOTTOM TYPE:  Clay-Mud; WATER DEPTH:  12.20 meters; GOOD FOR VESSEL SIZE:  Large�

Vessel Size: Small - Up to 91.4m long with a draft up to 5.49m; Medium - From 91.44m to 152.40m long with a draft from 5.49m to 9.14m; Large - Over 152.40m with a draft of 9.14m or over