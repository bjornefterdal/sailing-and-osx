NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION
US5LA16M - ALCASIEU RIVER AND APPROACHES

INDEX:
NOTE A
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
CAUTION - TEMPORARY
CAUTION - DREDGED AREAS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
WARNING - PRUDENT MARINER
RACING BUOYS
SMALL CRAFT WARNINGS
POLLUTION REPORTS
AUTHORITIES
CAUTION - LIMITATIONS
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS


NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 8th Coast Guard District in New Orleans, LA, or at the Office of the District Engineer, Corps of Engineers in New Orleans, LA.
Refer to charted regulation section numbers.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

CAUTION - TEMPORARY 
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.

CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and sub-marine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provides continuous weather broadcasts. 
The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.
Lake Charles, LA           KHB-42           162.40 MHz

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

RACING BUOYS
Racing buoys within the limits of this chart are not shown hereon. Information may be obtained from the U.S. Coast Guard District Offices as racing and other private buoys are not all listed in the U.S. Coast Guard Light List.

SMALL CRAFT WARNINGS
During the boating season small-craft warnings will be displayed from sunrise to sunset on Maryland Marine Police Cruisers while underway in Maryland waters of the Chesapeake Bay and tributaries.

POLLUTION REPORTS
Report all spills of oil and hazardous sub-stances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers and U.S. Coast Guard.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial - Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


END OF FILE
