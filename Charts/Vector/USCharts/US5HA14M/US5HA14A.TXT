NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5HA14E - WEST COAST OF HAWAI'I, HONOKOHAU HARBOR AND KEAUHOU BAY

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
NOTE B
WARNING �PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION
CAUTION
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
SUBMARINE PIPELINES AND CABLES
ADDITIONAL INFORMATION
TIDAL INFORMATION
ADMINISTRATION AREA


SYMBOLS AND ABBREVIATIONS: 
For Symbols and Abbreviations see Chart no. 1.

AUTHORITIES: 
Hydrography and topography by the National Ocean Service, Coast Survey with additional data from the Corps of Engineers, and U.S. Coast Guard.

AIDS TO NAVIGATION: 
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.  

NOTE A: 
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office of the District Engineer, Corps of Engineers in Honolulu, Hawaii.
Refer to Code of Federal Regulations section numbers.

NOTE B:
Submerged submarine operations are conducted at various times in the waters contained on this chart. Proceed with caution. 

WARNING �PRUDENT MARINER: 
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS: 
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: 
Consult U.S. Coast Pilot 7 for important supplemental information.

CAUTION: 
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.

CAUTION: 
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.

CAUTION: 
Improved channels are subject to shoaling, particularly at the edges.

RADAR REFLECTORS: 
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted.

STORM WARNINGS: 
The National Weather Service displays storm warnings at the following approximate locations:
Kailua-Kona, Kona Inn (19�38.6' - 156�00.0')
Mahukona (20�11.2' - 155�54.2')

NOAA WEATHER RADIO BROADCASTS: 
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.
Kulani Cone, HI		KBA-99		162.55 MHz
South Point, HI		KBA-99		162.55 MHz
Mt Haleakala, HI	KBA-99		162.40 MHz

SUBMARINE PIPELINES AND CABLES: 
Additional uncharted submarine pipelines and submarine cables may exist within the area.  Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exists, and when anchoring, dragging, or trawling.  
Covered wells may be marked by lighted or unlighted buoys.

ADDITIONAL INFORMATION:
Additional information can be obtained at nauticalcharts.noaa.gov

TIDAL INFORMATION:
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADMINISTRATION AREA:
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea. The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. For more information, please refer to the Coast Pilot.


END OF FILE