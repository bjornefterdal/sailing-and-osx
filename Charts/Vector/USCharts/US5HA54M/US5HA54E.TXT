NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5HA54E - PEARL HARBOR

INDEX:
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION
CAUTION - LIMITATIONS
POLLUTION REPORTS
CAUTION - TEMPORARY
CAUTION - IMPROVED CHANNELS
NOAA WEATHER RADIO BROADCASTS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE B
SMALL ARMS FIRING AREA
NOTE A
NOTE C - SUBMERGED SUBMARINE
AUTHORITIES
Administration Area


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information
concerning aids to navigation.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation,
particularly on floating aids.
See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to: 
http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


CAUTION - LIMITATIONS
Limitations on the use of certain other radio signals as aids to marine 
navigation can be found in the U.S. Coast Guard Light Lists and
National Geospatial-Intelligence Agency   publication 117.
Radio direction-finder bearings to commercial broadcasting stations are
subject to error and should be used with caution.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard
facility if telephone communication is impossible (33 CFR 153).

  
CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated.
See Local Notice to Mariners.


CAUTION - IMPROVED CHANNELS
Improved channels are subject to shoaling, particularly at the edges.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 nautical miles from the antenna site,
but can be as much as 100 nautical miles for stations at high elevations.

Hawaii Kai,HI 	KBA-99 		162.40 MHz
Mt Kaala, HI	KBA-99		162.55 MHz


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within
the area of this chart. Not all submarine pipelines and submarine cables are
required to be buried, and those that were originally buried may have become
exposed.
Mariners should use extreme caution when operating vessels in depths of water
comparable to their draft in areas where pipelines and cables may exist, and
when anchoring, dragging, or trawling. 
Covered wells may be marked by lighted or unlighted buoys.


NOTE B
The indicated area at Pearl Harbor is a Naval Defense Sea Area and is
closed to the public.  
Only ships or other craft authorized by the Secretary of the Navy shall
be navigated in this area.


SMALL ARMS FIRING AREA
Area closed to navigation 0600-1700 daily including Saturday, Sunday
and at other times upon notification.


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.
Additions or revisions to Chapter 2 are published in the Notice to Mariners.
Information concerning the regulations may be obtained at the Office of the
Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office
of the District Engineer,Corps of Engineers in Honolulu, Hawaii.
Refer to charted regulation section numbers.


NOTE C - SUBMERGED SUBMARINE
Submerged submarine operations are conducted at various times in the
waters contained on this chart.
Proceed with caution.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey,
with additional data from the Corps of Engineers, Geological Survey,
U.S. Coast Guard, and the Department of the Navy.


Administration Area
The entire extent of this ENC cell falls within the limits of an Administration Area. This area  
covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which 
the United States exercises sovereignty extending to the airspace as well as to its bed and 
subsoil.  For more information, please refer to the Coast Pilot. 


END OF FILE