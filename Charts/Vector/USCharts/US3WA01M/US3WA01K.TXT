Aides � La Navigation

C�bles

Les Vitesses des Courants

Services de Trafic Maritime

Services de Trafic Maritime Coop�ratif

Zones STM

AIDES � LA NAVIGATION Pour plus de renseignements concernant les aides � la navigation, consulter les cartes � plus grande �chelle et le Livre des feux, des bou�es et des signaux de brume C�te Pacifique et Aides radio � la navigation maritime (Pacifique et l'Arctique de l'ouest).

C�BLES Les signes conventionnels des c�bles sous- marins et a�riens ne diff�rencient pas les c�bles conducteurs d'�lectricit�, souvent � haute tension, des autres c�bles de service.  Les navigateurs prendront garde en passant sous tous les c�bles a�riens et �viteront de jeter l'ancre ou d'effectuer des op�rations de fond � proximit� des c�bles sous-marins.  La hauteur libre d'un c�ble a�rien peut varier de sa valeur cartographi�e � cause des changements dans les conditions atmosph�riques, les niveaux d'eau et autres facteurs.  Pour plus de renseignements, consulter l'Avis aux navigateurs n� 16 de chaque ann�e et le volume appropri� des Instructions nautiques du SHC.

LES VITESSES DES COURANTS Les vitesses du courant repr�sentent les vitesses maximales normales en vive-eau.  Les vitesses du courant indiqu�es dans les eaux des �tats-Unis d'Am�rique sont la vitesse maximum moyenne.

Pour des renseignements sur les courants, consulter les Tables des mar�es et courants du Canada, Vol.5 et Vol.6.

SERVICES DE TRAFIC MARITIME Point d'appel num�rot� de Services de trafic maritime (STM); la fl�che indique la direction du mouvement du navire; ce navire doit faire un rapport.  Trafic de Victoria ou Tofino: pour plus de renseignements, voir les Aides radio � la navigation maritime (Pacifique et l'Arctique de l'ouest), 3e partie et contacter le Centre de Services de communications et de trafic maritimes (SCTM) ou le Centre de Services du trafic maritime de Puget Sound (PSVTS, Seattle Traffic).

SERVICES DE TRAFIC MARITIME COOP�RATIF Les gardes c�ti�res du Canada et des �tats-Unis ont �tabli un Syst�me commun de trafic maritime (CVTS) obligatoire pour la r�gion de Juan de Fuca.  Pour plus de renseignements, voir les Aides radio � la navigation maritime (ARNM) (Pacifique et l'Arctique de l'ouest), 3e partie, le volume pertinent des Instructions nautiques du SHC, ainsi que le chapitre 2 du United States Coast Pilot 7.

ZONES STM Limites des zones de Services de trafic maritime de Vancouver, Tofino ou Seattle.  Les navires concern�s doivent �tre � l'�coute de la fr� quence de secteur appropri�e et doivent envoyer un compte rendu � chaque point d'appel, ainsi qu'� l'entr�e et � la sortie de ces zones.

