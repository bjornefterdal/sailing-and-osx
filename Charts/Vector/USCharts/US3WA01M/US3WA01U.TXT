      ANCRAGES SCIENTIFIQUES

Des capteurs acoustiques, consistant en un ensemble d'instruments flottant au-dessus de l'ancrage en b�ton auquel ils sont reli�s, sont positionn�s en lignes distantes de quelque 1 km.  Les instruments, submerg�s dans des eaux de moins de 150m de profondeur, sont � moins de 5m du fond marin.  Les instruments, submerg�s dans des eaux de plus de 150m de profondeur, sont � environ 150m de la surface.

