C�BLES Les signes conventionnels des c�bles sous-marins et a�riens ne diff�rencient pas les c�bles conducteurs d'�lectricit�, souvent � haute tension, des autres c�bles de service.  Les navigateurs prendront garde en passant sous tous les c�bles a�riens et �viteront de jeter l'ancre ou d'effectuer des op�rations de fond � proximit� des c�bles sous-marins.  La hauteur libre d'un c�ble a�rien peut varier de sa valeur cartographi�e � cause des changements dans les conditions atmosph�riques, les niveaux d'eau et autres facteurs.  Pour plus de renseignements, consulter l'avis n�16 des Avis aux navigateurs, �dition annuelle et le volume appropri� des Instructions nautiques du SHC.

AIDES � LA NAVIGATION Pour plus de renseignements concernant les aides � la navigation, consulter les cartes � plus grande �chelle et le Livre des feux, des bou�es et des signaux de brume C�te Pacifique et Aides radio � la navigation maritime (Pacifique et l'Arctique de l'ouest).

ZONES DE D�BLAIS DES �-UA Pour des renseignements sur les zones de d�blais o� il existe des restrictions dans les eaux des �tats-Unis d'Am�rique, consulter les cartes les plus r�centes du National Oceanic and Atmospheric Administration.

LES VITESSES DES COURANTS Les vitesses du courant repr�sentent les vitesses maximales normales en vive-eau.  Les vitesses du courant indiqu�es dans les eaux des �tats-Unis d'Am�rique sont la vitesse maximum moyenne.  Pour des renseignements sur les courants, consulter les Tables des mar�es et courants du Canada, Vol. 5.

