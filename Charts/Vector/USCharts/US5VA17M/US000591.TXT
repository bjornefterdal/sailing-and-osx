The project depth from Norfolk to Pamlico River, 
NC, via Virginia Cut, is 3.6 meters/12 feet; from Norfolk to 
Albemarle Sound, via Dismal Swamp Canal, 2.7 meters/9 feet. 
The controlling depths are published periodically 
in the U.S. Coast Guard Local Notice to Mariners.
