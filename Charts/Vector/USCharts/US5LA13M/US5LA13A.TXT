NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5LA13M - WAX LAKE OUTLET TO FORKED ISLAND

INDEX:
INTRACOASTAL WATERWAY - GIWW PROJECT DEPTH / DISTANCES
INTRACOASTAL WATERWAY (ALTERNATE ROUTE) MORGAN CITY TO PORT ALLEN
INTRACOASTAL WATERWAY (LANDSIDE ROUTE) MORGAN CITY TO PORT ALLEN
INTRACOASTAL WATERWAY - AIDS
ATCHAFALAYA RIVER ROUTE
VERMILION RIVER
ACADIANA NAVIGATION CHANNEL
NOTE A
AIDS TO NAVIGATION
CAUTION - TEMPORARY
RADAR REFLECTORS
WARNING - PRUDENT MARINER
SUPPLEMENTAL INFORMATION
CAUTION - IMPROVED CHANNELS
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
POLLUTION REPORTS
CAUTION - WARNINGS CONCERNING LARGE VESSELS
RULES OF THE ROAD (ABRIDGED)
CAUTION - SMALL CRAFT
PUBLIC BOATING INSTRUCTION PROGRAMS
MARINE WEATHER FORECASTS
NOAA WEATHER RADIO BROADCASTS
BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS BY MARINE RADIOTELEPHONE STATIONS
HURRICANES AND TROPICAL STORMS
TIDAL INFORMATION
NOTE S
MINERAL DEVELOPMENT STRUCTURES
ADDITIONAL INFORMATION
CAUTION - LIMITATIONS


NOTES:
INTRACOASTAL WATERWAY - GIWW PROJECT DEPTH / DISTANCES
12 feet/3.6 meters Carrabelle, FL to Brownsville, TX.
The controlling depths are published period-
ically in the U.S. Coast Guard Local Notice to
Mariners.
Mileage distances shown along the Waterway
are in Statute Miles, based on zero at Harvey
Lock, LA.
Tables for converting Statute Miles to Inter-
national Nautical Miles are given in U.S. Coast
Pilot 5.

INTRACOASTAL WATERWAY (ALTERNATE ROUTE) MORGAN CITY TO PORT ALLEN
The Project Depth is 3.6 meters/12 feet meters from Morgan City, Louisiana to Port
Allen, Louisiana.
The controlling depths are published periodically in the U.S. Coast Guard
Local Notice to Mariners.
Mileage distances shown along the Waterway are in Statute Miles, based on zero at the
junction with the Gulf Intracoastal Waterway at Morgan City.
Tables for converting Statute Miles to International Nautical Miles are
given in U.S. Coast Pilot 5.

INTRACOASTAL WATERWAY (LANDSIDE ROUTE) MORGAN CITY TO PORT ALLEN
The controlling depth was 2.1 meters/7 feet Sep. 1994 to its junction with the 
Alternate Route in the Lower Grand River.
The controlling depths are published periodically in the U.S. Coast Guard
Local Notice to Mariners.
Mileage distances shown along the Waterway are in Statute Miles, based on zero at the
junction of Bayou Boeuf and the Gulf Intracoastal Waterway.
Tables for converting Statute Miles to International Nautical Miles are
given in U.S. Coast Pilot 5.

INTRACOASTAL WATERWAY - AIDS
The U.S. Aids to Navigation System is de-
signed for use with nautical charts, and the exact
meaning of an aid to navigation may not be clear
unless the appropriate chart is consulted.
Aids to navigation marking the Intracoastal
Waterway exhibit unique yellow symbols to
distinguish them from aids marking other water-
ways.
When following the Intracoastal Waterway
westward from Carrabelle, FL to Brownsville, TX, 
aids with yellow triangles should be kept on the
starboard side of the vessel and aids with yellow
squares should be kept on the port side of the
vessel.  
A horizontal yellow band provides no lateral
information, but simply identifies aids to navi-
gation as marking the Intracoastal Waterway.

ATCHAFALAYA RIVER ROUTE
The Navigation Project is 3.6 meters/12 feet deep by
38.1 meters/125 feet wide from the Mississippi River via
Old River, Atchafalaya River, Grand Lake and
Sixmile Lake at Morgan City, LA.
The controlling depths are published
periodically in Navigation Bulletins issued by
the New Orleans District Corps of Engineers,
New Orleans, LA.
Buoys are not charted.

VERMILION RIVER
The controlling depths were 2.7 meters/9 feet on centerline 
from the Intracoastal Waterway to the Perry Bridge; thence 
1.8 meters/6 feet on centerline to the Broussard Bridge; 
thence 0.6 meters/2 feet on centerline to the Ambassador 
Caffery Bridge; thence shoal to bare to the Pinhook Bridge. 
Use of local knowledge is recommended. 
					Apr 2015

ACADIANA NAVIGATION CHANNEL
The channel is privately maintained with a
controlling depth of 3.0 meters/10 feet reported from
the Gulf Intracoastal Waterway Depth to the
main entrance of the port. Depths along
the edge of the channel may be subject to
shoaling.
			     October 2002

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 5. Additions or revisions to Chapter 2 are pub-
lished in Notices to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
8th Coast Guard District in New Orleans, LA, or at the Office
of the District Engineer, Corps of Engineers in New Orleans,
LA.
Refer to charted regulation section numbers.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.

CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


CAUTION - IMPROVED CHANNELS
Improved channels are
subject to shoaling, particularly at the edges.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the Corps of Engineers, Geological
Survey, and U.S. Coast Guard.

POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).

CAUTION - WARNINGS CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall
not impede the passage of a vessel that can navigate only
within a narrow channel or fairway. Large vessels may
appear to move slowly due to their large size but actually
transit at speeds in excess of 12 knots, requiring a great
distance in which to maneuver or stop. A large vessel's
superstructure may block the wind with the result that
sailboats and sailboards may unexpectedly find themselves
unable to maneuver. Bow and stern waves can be hazardous
to small vessels. Large vessels may not be able to see small
craft close to their bows.

RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases.
Sailing vessels and motorboats less than 19.8 meters/65 feet in
length shall not hamper, in a narrow channel, the safe
passage of a vessel which can navigate only inside that
channel.
A motorboat being overtaken has the right-of-way.
Motorboats approaching head to head or nearly so should
pass port to port.
When motorboats approach each other at right angles or
obliquely, the boat on the right has the right-of-way in most
cases.
Motorboats must keep to the right in narrow channels when
safe and practicable.
Mariners are urged to become familiar with the complete text
of the Rules of the Road in U.S. Coast Guard publication
"Navigation Rules".

CAUTION - SMALL CRAFT
Small craft should stay clear of large com-
mercial and government vessels even if small
craft have the right-of-way.
All craft should avoid areas where the skin
divers flag, a red square with a diagonal white
stripe, is displayed.

PUBLIC BOATING INSTRUCTION PROGRAMS
The United States Power Squadrons (USPS) and U.S. Coast Guard Auxiliary
(USCGAUX), national organizations of boatmen, conduct extensive boating in-
struction programs in communities throughout the United States. For information
regarding these educational courses, contact the following sources:
USPS - Local Squadron Commander or USPS Headquarters, 1504 Blue Ridge
Road, Raleigh, NC 27607, 888-367-8777
USCGAUX - COMMANDER (OAX), Eighth Coast Guard District, Hale Boggs
Federal Building, Suite 1126, 500 Poydras Street, New Orleans, LA 70130
800-524-8835 Or USCG Headquarters, Office of the Chief Director (G-OCX), 2100
Second Street, SW, Washington, DC 20593

MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE 
        CITY                    TELEPHONE NUMBER          OFFICE HOURS

Lake Charles, LA                 (337) 477-5285           24 hours daily
                                *(337) 439-0000

*Recording (24 hours daily)

NOAA WEATHER RADIO BROADCASTS
CITY                 STATION        FREQ. (MHz)      BROADCAST TIMES
New Orleans, La      KHB-43         162.55          24 hours daily
Baton Rouge, LA      KHB-46         162.40          24 hours daily
Morgan City, LA      KIH-23         162.475         24 hours daily
Lafayette, LA        WXK-80         162.55          24 hours daily

BROADCASTS OF MARINE WEATHER FORECASTS AND WARNINGS
 BY MARINE RADIOTELEPHONE STATIONS
CITY                 STATION        FREQ.        BROADCAST TIMES                  SPECIAL WARNING
New Orleans, La       NMG           2670 kHz     4:35, 6:35, 10:35 & 11:50 AM       On receipt
                    (USCG)                       4:35 & 11:50 PM
                                    157.1 MHz    4:50 & 10:50 AM 4:35 PM            On receipt
Grand Isle, LA       NMG-15         157.1 MHz    4:35 & 10:35 AM 4:35 PM            On receipt
Berwick, LA          NMG-37         157.1 MHz    4:00 & 10:00 AM 4:00 PM            On receipt

Distress calls for small craft are made on 2182 kHz or
channel 16 (156.80 MHz) VHF.

HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause
considerable damage to marine structures, aids to navigation and moored
vessels, resulting in submerged debris in unknown locations.
Charted soundings, channel depths and shoreline may not reflect actual
conditions following these storms. Fixed aids to navigation may have been 
damaged or destroyed. Buoys may have been moved from their charted
positions, damaged, sunk, extinguished or otherwise made inoperative.
Mariners should not rely upon the position or operation of an aid to
navigation. Wrecks and submerged obstructions may have been displaced
from charted locations. Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to
report aids to navigation discrepancies and hazards to navigation to the
nearest United States Coast Guard unit.
  
TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-
ops.nos.noaa.gov

NOTE S
Regulations for Ocean Dumping Sites are
contained in 40 CFR, Parts 220-229. Additional
information concerning the regulations and re-
quirements for use of the sites may be obtained
from the Environmental Protection Agency (EPA).
See U.S. Coast Pilots appendix for addresses of
EPA offices. Dumping subsequent to the survey
dates may have reduced the depths shown.

MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals
are required for fixed mineral development
structures shown, subject to ap-
proval by the District Commander, U.S. Coast
Guard (33 CFR 67).

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

END OF FILE
