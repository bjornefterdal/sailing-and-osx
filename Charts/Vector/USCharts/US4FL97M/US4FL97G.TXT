Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 4 & 5. Additions or revisions to Chapter 2 are 
published in the Notices to Mariners. Information concerning
the regulations may be obtained at the Office of the Com-
mander, 7th Coast Guard District in Miami, Florida, or at
the Office of the District Engineer, Corps of Engineers in 
Jacksonville, Florida.
Refer to charted regulation section numbers.