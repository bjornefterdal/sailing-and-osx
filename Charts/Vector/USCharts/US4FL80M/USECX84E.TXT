NOTE B
Launch debris may fall within these areas. See Notice to Mariners or contact the Coast 
Guard for launch hazard areas specific to each launch and the times 
they will be in effect.

