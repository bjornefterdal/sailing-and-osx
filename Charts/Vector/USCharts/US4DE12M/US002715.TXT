NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4DE12E - DELAWARE BAY, NEW JERSEY - DELAWARE

INDEX:
AIDS TO NAVIGATION
CAUTION - TEMPORARY
RACING BUOYS
CAUTION - LIMITATIONS
WARNING - PRUDENT MARINER 
AUTHORITIES
NOTE A
CAUTION - NUMEROUS
CAUTION - DREDGED AREAS
CAUTION - RIPRAP
NOAA WEATHER RADIO BROADCASTS
SUPPLEMENTAL INFORMATION
POLLUTION REPORTS
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE S
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION
HURRICANES AND TROPICAL STORMS


NOTES:
AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

                                 
CAUTION - TEMPORARY
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.


RACING BUOYS
Racing Buoys within the limits 
are not shown hereon. Information may be 
obtained from the U.S. Coast Guard District 
Offices as racing and other private buoys are 
not all listed in the U.S. Coast Guard Light List.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


WARNING - PRUDENT MARINER 
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.

                                  
NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 3. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
5th Coast Guard District in Portsmouth, Virginia or at the 
office of the District Engineer, Corps of Engineers in 
Philadelphia, Pennsylvania.
Refer to charted regulation section numbers.


CAUTION - NUMEROUS
Numerous uncharted duck blinds, stakes, piles,
signs and pipes, some submerged, may exist in the
area.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


CAUTION - RIPRAP
Mariners are warned to stay clear of the pro-
tective riprap surrounding navigational light 
structures.

 
NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations. 

Atlantic City, NJ     KHB-38    162.40 MHz
Salisbury, MD         KEC-92    162.475 MHz
Lewes, DE             WXJ-94    162.55 MHz
Sudlersville, MD      WXK-97	162.50 MHz	

                      
SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 3 for important 
supplemental information.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


NOTE S
Regulations for Ocean Dumping Sites are contained in 40 CFR, Parts 220-229. 
Additional information concerning the regulations and requirements for use of the 
sites may be obtained from the Environmental Protection Agency (EPA). See 
U.S. Coast Pilots appendix for addresses of EPA offices. Dumping subsequent to 
the survey dates may have reduced the depths shown.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.
Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


END OF FILE