SAN FRANCISCO-OAKLAND BAY BRIDGE
(Privately maintained aids)
The piers are lettered on the chart for reference.
Lights:
Span EF. A fixed red light on each side of bridge marking the
NE limit of the navigable channel.
