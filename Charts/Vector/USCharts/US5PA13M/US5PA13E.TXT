NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5PA13E - DELAWARE RIVER, PHILADELPHIA TO TRENTON

INDEX:
NOTE A
CAUTION - SUBMARINE PIPELINES AND CABLES
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION - TEMPORARY
CAUTION - IMPROVED CHANNELS
NOAA WEATHER RADIO BROADCASTS
AUTHORITIES
WARNING - PRUDENT MARINER
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION

NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 3.
Additions or revisions to Chapter 2 are published in the Notice to Mariners.
Information concerning the regulations may be obtained at the Office of the
Commander, 5th Coast Guard District in Portsmouth, Virginia or at the 
Office of the District Engineer, Corps of Engineers in Philadelphia, Pennsylvania.
Refer to charted regulation section numbers.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within
the area of this chart. Not all submarine pipelines and sub marine cables are
required to be buried, and those that were originally buried may have become exposed.
Mariners should use extreme caution when operating vessels in depths of water comparable
to their draft in areas where pipelines and cables may exist, and when anchoring,
dragging, or trawling.
Covered wells may be marked by lighted or unlighted buoys.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 3 for important supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning
aids to navigation. 


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone
communication is impossible (33 CFR 153).


CAUTION - TEMPORARY
Temporary changes or defects in aids to navigation are not indicated.
See Local Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation
are replaced by other types or removed.
For details see U.S. Coast Guard Light List.


CAUTION - IMPROVED CHANNELS
Improved channels are subject to shoaling, particularly at the edges.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provides continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Philadelphia, PA	KIH-28		162.475 MHz


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation,
particularly on floating aids.
See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation.
Individual radar reflector identification on these aids has been omitted
from this chart.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


END OF FILE