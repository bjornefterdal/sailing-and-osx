NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION
US5CA90E - ST GEORGE REEF

INDEX:
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
CAUTION - CHANGES IN BUOYAGE
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
CAUTION - LIMITATIONS
ADDITIONAL INFORMATION
TIDAL INFORMATION
ADMINISTRATION AREA

NOTES:

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
11th Coast Guard District in Alameda, California or at the 
Office of the District Engineer, Corps of Engineers in 
San Francisco, California.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid 
to navigation, particularly on floating aids. See U.S. Coast 
Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication is impossible (33 CFR 
153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important 
supplemental information.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.

CAUTION - CHANGES IN BUOYAGE
Mariners are advised that authorized aids to navigation are being changed to 
conform to maritime standards of the International Association of Lighthouse 
Authorities Maritime Buoyage System, Region B, Significant changes are: black 
port hand buoys to green; black and white vertically striped buoys to red and white 
vertically striped buoys; and lateral lights from white to red or green as appropriate. 
Changes to aids to navigation will be announced in the National Geospatial-Intelligence 
Agency weekly Notice to Mariners and the U.S. Coast Guard Local Notice to 
Mariners. 

RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed 
below provides continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations. 

Crescent City, CA		KIH-37	              162.550 MHz  WX1


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov 


TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. 
This area covers land, internal waters, and territorial sea.  The territorial sea is 
a maritime zone which the United States exercises sovereignty extending to the 
airspace as well as to its bed and subsoil.  For more information, please 
refer to the Coast Pilot.


END OF FILE