The U.S. Coast Guard and the Pacific States/British Columbia Oil Spill 
Task Force endorse a system of voluntary measures and minimum 
distances from shore for certain commercial vessels transiting along 
the coast anywhere between Cook Inlet, Alaska and San Diego, 
California.  See U.S. Coast Pilot 7, Chapter 3 for details.