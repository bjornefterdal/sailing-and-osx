NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION


US3OR03E - TRINIDAD HEAD TO CAPE BLANCO



INDEX:
AUTHORITIES

WARNING - PRUDENT MARINER

AIDS TO NAVIGATION

POLLUTION REPORTS

RADAR REFLECTORS
CAUTION - TEMPORARY CHANGES
CAUTION - LIMITATIONS

NOAA WEATHER RADIO BROADCASTS

NOTE A
TIDAL INFORMATION

ADDITIONAL INFORMATION



NOTES:
AUTHORITIES

Hydrography and Topography by the National 
Ocean Service, Coast Survey with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.





WARNING - PRUDENT MARINER

The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids.  See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.





AIDS TO NAVIGATION

Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.





POLLUTION REPORTS

Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.





CAUTION - TEMPORARY
 CHANGES
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners.





CAUTION - LIMITATIONS

Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.

Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.





NOAA WEATHER RADIO BROADCASTS

The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts.
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.


Port Orford, OR	        	WNG-596	162.425 MHz

Brookings, OR		KIH-37		162.550 MHz


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
13th Coast Guard District in Seattle, Washington or at the
Office of the District Engineer, Corps of Engineers in 
Seattle, Washington.
Refer to charted regulation section numbers.




TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov





ADDITIONAL INFORMATION

Additional information can be obtained at www.nauticalcharts.gov.




END OF FILE