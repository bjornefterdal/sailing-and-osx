PARTICULARLY SENSITIVE SEA AREA
A PSSA is an environmentally sensitive area in which and around which mariners should exercise extreme caution. 
See U.S. Coast Pilot volume 7 for information regarding this area.

PAPAHANAUMOKUAKEA MARINE NATIONAL MONUMENT - MIDWAY ISLANDS AND KURE ATOLL SPECIAL PRESERVATION AREA
The Papahanaumokuakea Marine National Monument and the Midway Islands and Kure Atoll Special Preservation Areas,
are protected areas. See 50 CFR 404 or Chapter 2, U.S. Coast Pilot 7.


