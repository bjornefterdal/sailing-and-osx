LAKE WASHINGTON SHIP CANAL
Traffic Lights 207.750 (see note A)
Red (stop) green (go) lights are maintained on
the guide pier below the Burlington Northern R.R.
bridge for the guidance of vessels going through
the large lock.

SALT WATER BARRIER
Depth over the salt water barrier varies. Available
depth over barrier is posted on signs on the
guide piers.
