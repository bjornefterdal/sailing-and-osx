NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5WA26E - HOOD CANAL, SOUTH POINT TO QUATSAP POINT INCLUDING DABOB BAY

INDEX:
POLLUTION REPORTS
AIDS TO NAVIGATION
CAUTION - TEMPORARY
CAUTION - SUBMARINE PIPELINES AND CABLES
NOTE C
NOTE B - NAVY-MAINTAINED WARNING LIGHTS
NOTE H
NOAA WEATHER RADIO BROADCASTS
SUPPLEMENTAL INFORMATION
COLREGS, 80.1395 (see note A)
WARNING - PRUDENT MARINER
NOTE A
AUTHORITIES
RADAR REFLECTORS
TIDAL INFORMATION
ADDITIONAL INFORMATION
REGULATED NAVIGATION AREA


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.

CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.

CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

NOTE C
Floating security barriers have been installed at 
various U.S. Naval installations throughout Puget 
Sound.  The barriers are marked by numerous 
flashing yellow (Fl Y 2s) Navy maintained lighted 
buoys and approximately mark the Restricted Areas 
surrounding the facility. 

NOTE B - NAVY-MAINTAINED WARNING LIGHTS
Yellow or alternating white and yellow
-Proceed with caution.
-Range operations are in progress but no torpedoes or
 testing is occurring.
-Be prepared to shutdown engines when lights change to red.
Red or alternating white and red
-Range operations are in progress and submarine torpedo
 and/or sound testing are occurring.
-Stop engines until red beacons have been shut off, showing
 test is completed.
-Follow the advice of Naval Guard Boats when in or near the
 range area.
Operational Periods
-Typically, boat passage is permitted between tests when
 the yellow beacons are operating.
-Normally, tests and torpedo runs are confined to periods
 of less than 30 minute durations.
-Submarine operations can occur for longer periods.

NOTE H
The U.S. Coast Guard operates a mandatory Vessel Traffic 
Services (VTS) system in the Puget Sound area.  Vessel 
operating procedures and designated radiotelephone 
frequencies are published in 33 CFR 161, the U.S. Coast 
Pilot, and/or the VTS User's Manual. The entire area of 
the chart falls within the Vessel Traffic Services (VTS) 
system.
 

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Puget Sound, WA	  WWG-24	162.425 MHz
Seattle, WA	        KHB-60	162.55  MHz

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important
supplemental information.

COLREGS, 80.1395 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of the chart falls seaward of the COLREGS Demarcation Line.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the
regulations may be obtained at the Office of the Commander,
13th Coast Guard District in Seattle, Washington or at the
Office of the District Engineer, Corps of Engineers in
Seattle, Washington.
Refer to charted regulation section numbers.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey with additional data from the Corps of Engineers, Geological
Survey, U.S. Coast Guard, and National Geospatial-Intelligence Agency.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to
http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at WWW.nauticalcharts.gov

REGULATED NAVIGATION AREA
A Regulated Navigation Area has been established by the U.S. Coast Guard.
Please see Chapter 2, U.S. Coast Pilot 7 or 33 CFR 165.1301 and 33 CFR
165.1303.


END OF FILE
