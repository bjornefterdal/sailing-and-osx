Navigation regulations are published in Chapter 2, U.S. 
Coast Pilots 1, 2 & 3. Additions or revisions to Chapter 2 
are published in the Notice to Mariners. Information con-
cerning the regulations may be obtained at the Office of the
Commander, 1st Coast Guard District in Boston, MA, and 
5th Coast Guard District in Portsmouth, VA, or at the Office 
of the District Engineer, Corps of Engineers in Concord, MA, 
or the office of the District Engineer, Corps of Engineers in 
New York, NY.
Refer to charted regulation section numbers.