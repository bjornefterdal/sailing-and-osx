NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NY61M - MORRISTOWN TO BUTTERNUT BAY

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION
CAUTION	
CAUTION	
CAUTION
CAUTION
RADAR REFLECTORS	
SUBMARINE PIPELINES AND CABLES
BRIDGE AND OVERHEAD CABLE CLEARANCES
RACING BUOYS
NOAA WEATHER RADIO BROADCASTS

SYMBOLS AND ABBREVIATIONS:
For complete list of symbols and abbreviations, see Chart no. 1.	

AUTHORITIES:
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, U.S. Coast Guard, and Canadian authorities.

AIDS TO NAVIGATION:
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.  See Canadian List of Lights, Buoys and Fog Signals for information not included in the U.S. Coast Guard Light List.

NOTE A:
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 9th Coast Guard District in Cleveland, Ohio or at the Office of the District Engineer, Corps of Engineers in Buffalo, New York. Refer to Code of Federal Regulations section numbers.  

WARNING � PRUDENT MARINER:
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids.  See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.

POLLUTION REPORTS:
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION:
Consult U.S. Coast Pilot 6 for important supplemental information. Additional Information can be obtained at nauticalcharts.noaa.gov.

CAUTION:
Due to periodic high water conditions in the Great Lakes, some features as visible at Low Water Datum may be submerged, particularly in the near shore areas.  Mariners should proceed with caution.

CAUTION:
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners. During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed.  For details see U.S. Coast Guard Light List.

CAUTION:
SPEED REGULATIONS.  See U.S. Rules and Regulations for U.S. waters, 33 CFR Part 401, carried in the Seaway Handbook.

CAUTION:
Improved channels are subject to shoaling, particularly at the edges.

RADAR REFLECTORS:
Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector identification on these aids has been omitted.

SUBMARINE PIPELINES AND CABLES:
Additional uncharted submarine pipelines and submarine cables may exist within the area.  Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.

BRIDGE AND OVERHEAD CABLE CLEARANCES:
When the water surface is above Low Water Datum, bridge and overhead clearances are reduced correspondingly.  For clearances see U.S. Coast Pilot 6.

RACING BUOYS:
Racing buoys within the limits are not shown hereon.  Information may be obtained from the U.S. Coast Guard District Offices as racing and other private buoys are not all listed in the U.S. Coast Guard Light List.

NOAA WEATHER RADIO BROADCASTS:
The NOAA Weather Radio station listed below provides continuous weather broadcasts.  The reception range is typically 20 to 40 miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Watertown, NY	WXN-68	162.475 MHz


END OF FILE