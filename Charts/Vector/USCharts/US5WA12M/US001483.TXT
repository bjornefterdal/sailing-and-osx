Mariners are cautioned that the Washington State
Ferries may deviate from the published standard routes
due to inclement weather, traffic conditions, navigational
hazards or other emergency conditions.
