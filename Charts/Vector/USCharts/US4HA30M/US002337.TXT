NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION


US4HA30E - CHANNELS BETWEEN O'AHU, MOLOKA'I, AND LANA'I


INDEX:

SYMBOLS AND ABBREVIATIONS

AUTHORITIES

AIDS TO NAVIGATION

NOTE A

NOTE B
SUBMARINE PIPELINES AND CABLES

WARNING � PRUDENT MARINER

POLLUTION REPORTS
SUPPLEMENTAL INFORMATION

CAUTION

CAUTION

RADAR REFLECTORS

NOAA WEATHER RADIO BROADCASTS

COLREGS

NOTE X(TERRITORIAL SEA)



SYMBOLS AND ABBREVIATIONS: For Symbols and Abbreviations see Chart No. 1.



AUTHORITIES: 
Hydrography and topography by the National Ocean Service, Coast Survey, with additional 
data from the U.S. Coast Guard, Geological Survey, and National Geospatial-Intelligence Agency.



AIDS TO NAVIGATION: 
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


NOTE A: 
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or 
revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning 
the regulations may be obtained at the Office of the Commander, 14th Coast Guard District 
in Honolulu, Hawaii or at the Office of the District Engineer, Corps of Engineers in Honolulu, 
Hawaii.
Refer to Code of Federal Regulations section numbers.

NOTE B:
submerged submarine operations are conducted at various times in the waters contained on
this chart.  Proceed with caution. 



SUBMARINE PIPELINES AND CABLES: 
Additional uncharted submarine pipelines and submarine cables may exist within the area. 
Not all submarine pipelines and sub-marine cables are required to be buried, and those that 
were originally buried may have come exposed. Mariners should use extreme caution when operating 
vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, 
and when anchoring, dragging or trawling.
Covered wells may be marked by lighted or unlighted buoys.



WARNING � PRUDENT MARINER: 
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids.  
See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS: Report all spills 
of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to 
the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).



SUPPLEMENTAL INFORMATION: 
Consult U.S. Coast Pilot 7 for important supplemental information.



CAUTION: Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.



CAUTION:  
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast 
Guard Light Lists and National Geospatial-Intelligence Agency Publication 117.
Radio direction-finder 
bearings to commercial broadcasting stations are subject to error and should be used with caution.



RADAR REFLECTORS: 
Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector 
identification on these aids has been omitted from this chart.



NOAA WEATHER RADIO BROADCASTS: 
he NOAA Weather Radio stations listed below provide continuous weather broadcasts.  
The reception range is typically 20 to 40 nautical miles from the antenna site, but 
can be as much as 100 nautical miles for stations at high elevations.


Mt Kaala, HI	  KBA-99	162.55 MHz

Hawaii Kai, HI	  KBA-99	162.40 MHz

Mt Haleakala, HI  KBA-99	162.40 MHz



COLREGS: 
International Regulations for Preventing Collisions at sea, 1972.

The entire area falls seaward of the COLREGS Demarcation Line.

NOTE X(TERRITORIAL SEA): 
Within the 12-nautical mile Territorial Sea established by Presidential Proclamation, 
some Federal laws apply. The Three Nautical Mile Line, previously identified as the outer 
limit of the territorial sea, is retained as it continues to depict the jurisdictional l
imit of other laws. The 9-nautical mile Nautical Resource Boundary off the Gulf coast of 
Florida, Texas and Puerto Rico, and the Three Federal fisheries jurisdiction and the outer 
limit of the jurisdiction of the states. The 24-nautical mile Contiguous Zone and the 
200-nautical mile Exclusive Economic Zone were established by Presidential Proclamation. 
Unless fixed by treaty for the U.S. Supreme Court, these amaritime limits are subject to 
modification. 






END OF FILE