NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4AK13M - COOK INLET, ANCHOR POINT TO KALGIN ISLAND


INDEX:
NOTE A
CAUTION - TEMPORARY CHANGES
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
AUTHORITIES
CAUTION - LIMITATIONS
CAUTION - THE COOK INLET
CAUTION
RADAR REFLECTORS
MINERAL DEVELOPMENT STRUCTURES
POLLUTION REPORTS
NOAA WEATHER RADIO BROADCASTS
CAUTION - SUBMARINE PIPELINES AND CABLES
WARNING - PRUDENT MARINER
COLREGS, 80.1705 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION



NOTES:
NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 9. Additions or 
revisions to Chapter 2 are published in the 
Notice to Mariners. Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District 
Engineer, Corps of Engineers in Anchorage, 
Alaska.
Refer to charted regulation section numbers.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


CAUTION - THE COOK INLET
The Cook Inlet area is affected by land uplift due to forces such as post-
seismic crustal rebound.  As a result, the tidal datums including mean lower 
low water, the plane of reference used for depth soundings, have changed 
throughout this region.  Tidal datums were updated in 1999 and depths of 
11.5 fathoms or less on this chart were adjusted accordingly to account for this 
uplift.  As the uplift rates can only be estimated and areas continue to 
rise, depths may be shoaler than charted. Mariners are urged to exercise 
caution.


CAUTION
Oil exploration and production operations 
are being conducted in the waters of Cook Inlet.
Drilling vessels and movable and permanent 
platforms are being used.  Only permanent 
platforms are charted.  Mariners are urged to 
exercise caution when transiting the area.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals
are required for fixed mineral development 
structures, subject to approval 
by the District Commander, U.S. Coast 
Guard (33 CFR 67).


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be as
much as 100 nautical miles for stations at
high elevations.
	
Bede Mt,AK      WNG-528     162.450 MHz
Raspberry I,AK  KZZ-90      162.425 MHz 
Ninilchik,AK    KZZ-97      162.550 MHz
Soldotna, AK    WWG-39      162.475 MHz
Homer,AK        WXJ-24      162.400 MHz
	


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of 
this chart.  Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed.  Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where 
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


COLREGS, 80.1705 (see note A)
International Regulations for Preventing
Collisions at Sea, 1972.
The Entire area of this chart falls seaward of
the COLREGS Demarcation Line.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to 
http://co-ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE