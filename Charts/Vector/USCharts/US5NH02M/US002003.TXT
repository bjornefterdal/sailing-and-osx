The clean Vessel Act was created to provide an
alternative to the illegal overboard disposal of boater
sewage. All recreational vessels must have access to
pumpout stations funded under this act. The Act makes
grants available to the states on a competitive basis for
the construction and maintenance of pumpout and dump
stations. States are allowed to sub-grant to marinas.
Phone Number: (603) 433-5050
