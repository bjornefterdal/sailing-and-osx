The project depth is 3.6 meters/12 feet from New Orleans, La., to Aransas Pass, TX.
The controlling depths are published periodically in the U.S. Coast Guard Local Notice to Mariners.