NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NY31M - LOWER NIAGARA RIVER
RNC Chart 14816


INDEX:


NOTE A
NOTE Z
CAUTION  TEMPORARY
CAUTION
CAUTION  LIMITATIONS
WARNING  PRUDENT MARINER
NOAA WEATHER RADIO BROADCASTS
PLANE OF REFERENCE (Low Water Datum)
BRIDGE AND OVERHEAD CABLE CLEARANCES
POLLUTION REPORTS
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
AIDS TO NAVIGATION
RACING BUOYS
RADAR REFLECTORS
SUPPLEMENTAL INFORMATION
ADDITIONAL INFORMATION


NOTES:


NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6.  
Additions or revisions to Chapter 2 are published in the Notices to Mariners.  
Information concerning the regulations may be obtained at the Office of the 
Commander, 9th Coast Guard District in Cleveland, Ohio, or at the Office of 
the District Engineer, Corps of Engineers in Buffalo, New York.
Refer to charted regulation section numbers.


NOTE Z  NO-DISCHARGE ZONE
40 CFR 140
This chart falls entirely within the limits of a No-Discharge Zone (NDZ). 
Under the Clean Water Act, Section 312, all vessels operating within a No-Discharge 
Zone (NDZ) are completely prohibited from discharging any sewage, treated or 
untreated, into the waters. Commercial vessel sewage shall include graywater. 
All vessels with an installed marine sanitation device (MSD) that are navigating, 
moored, anchored, or docked within a NDZ must have the MSD disabled to prevent 
the overboard discharge of sewage (treated or untreated) or install a holding tank. 
Regulations for the NDZ are contained in the U.S. Coast Pilot. Additional information 
concerning the regulations and requirements may be obtained from the Environmental 
Protection Agency (EPA) web site: 
http://www.epa.gov/owow/oceans/regulatory/vessel_sewage/


CAUTION  TEMPORARY
Temporary changes or defects in aids to navigation are not indicated.  
See Local Notice to Mariners.
During some winter months or when endangered by ice, certain aids to navigation 
are replaced by other types or removed.  For details see U.S. Coast Guard Light List.


CAUTION
Due to periodic high water conditions in the Great Lakes, some features charted 
as visible at Low Water Datum may be submerged, particularly in the near shore areas. 
Mariners should proceed with caution.


CAUTION  LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found 
in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence 
Agency Publication 117.
Radio direction-finder bearings to commercial broadcasting stations are subject to 
error and should be used with caution.


WARNING  PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, 
particularly on floating aids.  
See U.S. Coast Guard Light List and U.S. Coast Pilot 6 for details.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather 
broadcasts. The reception range is typically 37 to 74 km / 20 to 40 nautical 
miles from the antenna site, but can be as much as 186 km / 100 nautical 
miles for stations at high elevations.

Buffalo, New York		KEB-98		162.55 MHz (Chan. WX-1)


PLANE OF REFERENCE (Low Water Datum)
Depths are referred to the sloping surface of the river when Lake Ontario is at 
elevation 243.3 feet. Referred to mean water level at Rimouski, Quebec, 
International Great Lakes Datum (1985).


BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water Datum, bridge and overhead 
clearances are reduced correspondingly.
For clearances see U.S. Coast Pilot 6.


POLLUTION REPORTS: 
Report all spills of oil and hazardous substances to the National Response Center 
via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if 
telephone communication is impossible (33 CFR 153).


SYMBOLS AND ABBREVIATIONS: 
For complete list of symbols and abbreviations see Chart no. 1.


AUTHORITIES: 
Hydrography and Topography by the National Ocean Service, Coast Survey, 
with additional data from the Corps of Engineers, Geological Survey, 
U.S. Coast Guard, and Canadian authorities.


AIDS TO NAVIGATION: 
Consult U.S. Coast Guard Light List for supplemental information concerning 
aids to navigation. See Canadian List of Lights, Buoys and Fog Signals for 
information not included in the U.S. Coast Guard Light List.


RACING BUOYS: 
Racing buoys information may be obtained from the U.S. Coast Guard District Offices 
as racing and other private buoys are not all listed in the U.S. Coast Guard Light List.


RADAR REFLECTORS: 
Radar reflectors have been placed on many floating aids to navigation. 
Individual radar reflector identification on these aids has been omitted.


SUPPLEMENTAL INFORMATION: 
Consult U.S. Coast Pilot 6 for important supplemental information.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE