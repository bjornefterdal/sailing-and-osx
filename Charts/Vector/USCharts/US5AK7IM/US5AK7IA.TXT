NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK7IM - KANAGA PASS AND APPROACHES

INDEX:
AUTHORITIES 
POLLUTION REPORTS
WARNING - PRUDENT MARINER
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
AIDS TO NAVIGATION
COLREGS, 80.1705 (see note A)
NOTE A
ADMINISTRATION AREA
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS

NOTES:
AUTHORITIES 
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the U.S. Coast Guard and the State
of Alaska.

POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids.  See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


COLREGS, 80.1705 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.  
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 9.  Additions or 
revisions to Chapter 2 are published in the 
Notices to Mariners.  Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District 
Engineer, Corps of Engineers in Anchorage, 
Alaska.
Refer to charted regulation section numbers.

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. 
This area covers land, internal waters, and territorial sea.  The territorial sea is 
a maritime zone which the United States exercises sovereignty extending to the 
airspace as well as to its bed and subsoil.  For more information, please 
refer to the Coast Pilot.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.

END OF FILE
