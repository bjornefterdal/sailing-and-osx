NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5LA51M - MISSISSIPPI RIVER NEW ORLEANS TO BATON ROUGE

INDEX:


AIDS TO NAVIGATION
POLLUTION REPORTS
CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
SUPPLEMENTAL INFORMATION
CAUTION  TEMPORARY CHANGES
WARNING  PRUDENT MARINER
ADDITIONAL INFORMATION
AUTHORITIES
NOTE A
AUTHORITIES
SUPPLEMENTAL INFORMATION
CAUTION  SUBMARINE PIPELINES AND CABLES
CAUTION  DREDGED AREAS
CAUTION  GAS AND OIL WELL STRUCTURES
RULES OF THE ROAD (ABRIDGED)
CAUTION  WARNINGS CONCERNING LARGE VESSELS
CAUTION  SMALL CRAFT
CAUTION  FLOATING DEBRIS
HURRICANES AND TROPICAL STORMS
MINERAL DEVELOPMENT STRUCTURES
RADAR REFLECTORS
PUBLIC BOATING INSTRUCTION PROGRAMS
NOAA WEATHER RADIO BROADCASTS
BROADCASTS OF MARINE WEATHER FORECASTS AND WARINGS BY MARINE RADIOTELEPHONE STATIONS
MARINE WEATHER FORECASTS
RULES OF THE ROAD (ABRIDGED)
CAUTION  WATER TURBULENCE
MISSISSIPPI RIVER BUOYS
TIDAL INFORMATION
VESSEL TRAFFIC SERVICES (VTS)
SOUNDING AND NATURAL COASTLINE DATUM
ADMINISTRATION AREA


NOTES:


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


CAUTION  USE OF RADIO SIGNALS (LIMITATIONS)
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S. Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.


CAUTION  TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.


WARNING  PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 8th Coast Guard District in New Orleans, LA, or at the Office of the District Engineer, Corps of Engineers in New Orleans, LA.
Refer to charted regulation section numbers.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.


CAUTION  SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


CAUTION  DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION  GAS AND OIL WELL STRUCTURES
Uncharted platforms, gas and oil well structures, pipes, piles and stakes can exist within the limits of this chart.


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases. Sailing vessels and motorboats less than sixty-five feet in length shall not hamper, in a narrow channel, the safe passage of a vessel which can navigate only inside that channel. A motorboat being overtaken has the right-of-way. Motorboats approaching head to head or nearly so should pass port to port. When motorboats approach each other at right angles or obliquely, the boat on the right has the right-of-way in most cases. Motorboats must keep to the right in narrow channels when safe and practicable. Mariners are urged to become familiar with the complete text of the Rules of the Road in U.S. Coast Guard publication "Navigation Rules".


CAUTION  WARNINGS CONCERNING LARGE VESSELS
The "Rules of the Road" state that recreational boats shall not impede the passage of a vessel that can navigate only within a narrow channel or fairway. Large vessels may appear to move slowly due to their large size but actually transit at speeds in excess of 12 knots, requiring a great distance in which to maneuver or stop. A large vessel's superstructure may block the wind with the result that sailboats and sailboards may unexpectedly find themselves unable to maneuver. Bow and stern waves can be hazardous to small vessels. Large vessels may not be able to see small craft close to their bows.

CAUTION  SMALL CRAFT
Small craft should stay clear of large commercial and government vessels even if small craft have the right-of-way.


CAUTION  FLOATING DEBRIS
Mariners are warned that logs and other floating debris are constant dangers to navigation. Night travel by small craft is not recommended because of the hazard of floating obstructions.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations. Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved. Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals are required for fixed mineral development structures shown, subject to approval by the District Commander, U.S. Coast Guard (33 CFR 67).


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.

PUBLIC BOATING INSTRUCTION PROGRAMS
The United States Power Squadrons (USPS) and U.S Coast Guard Auxiliary (USCGAUX), national organizations of boatmen, conduct extensive boating instruction programs in communities throughout the United States. For information regarding these educational courses, contact the following 
sources: USPS - Local Squadron Commander or USPS Headquarters, 1504 Blue Ridge Road, Raleigh, NC 27607, 888-367-8777 USCGAUX - COMMANDER(OAX), Eighth Coast Guard District, Hale Boggs Federal Building, Suite 1126, 500 Poydras Street, New Orleans, LA 70130, 800-524-8835 or USCG Headquarters, Office of the Chief Director (G-OCX), 2100 Second Street, SW, Washington, DC 20593.


NOAA WEATHER RADIO BROADCASTS
CITY                  STATION    FREQ. (MHz)    BROADCAST TIMES
New Orleans, LA.      KHB-43     162.550        24 hours daily
Baton Rouge, LA.      KHB-46     162.400        24 hours daily
Morgan City, LA.      KIH-23     162.475        24 hours daily


BROADCASTS OF MARINE WEATHER FORECASTS AND WARINGS BY MARINERADIOTELEPHONE STATIONS

CITY	                  STATION     KHz	  BROADCAST TIMES-CST	            SPECIAL WARNING
New Orleans, LA.          NMG         2670        4:35, 6:35, 10:35 & 11:50 AM      * On receipt
				                  3:50, 4:35 & 11:50 PM

				      157.1 MHz   4:50, 10:50 AM, 4:50 PM           * On receipt

Grande Isle, LA.          NMG-15      157.1 MHz   4:35 & 10:35 AM, 4:35 PM

* Preceded by announcement on 2182 KHz and 156.8 MHz
Distress calls for small craft are made on 2182 KHz or channel 16 (156.80 MHz) VHF.


MARINE WEATHER FORECASTS
NATIONAL WEATHER SERVICE
CITY			       TELEPHONE NUMBERS	       OFFICE HOURS
New Orleans, LA		       (504) 522-7330                  8:00 AM-4:00 PM (Mon.-Fri.)
			       *(504) 465-9215			
*Recording (24 hours daily)


RULES OF THE ROAD (ABRIDGED)
Motorless craft have the right-of-way in almost all cases.
Sailing vessels and motorboats less than sixty-five feet in length shall not hamper, in a narrow channel, the safe passage of a vessel which can navigate only inside that channel. A motorboat being overtaken has the right-of-way. Motorboats approaching head to head or nearly so should 
pass port to port. When motorboats approach each other at right angles or obliquely, the boat on the right has the right-of-way in most cases.
Motorboats must keep to the right in narrow channels when safe and practicable. Mariners are urged to become familiar with the complete text of the Rules of the Road in U.S. Coast Guard publication "Navigation Rules".


CAUTION  WATER TURBULENCE
Small craft operators are warned to beware of severe water turbulence caused by large vessels traversing narrow waterways.


MISSISSIPPI RIVER BUOYS
Due to frequently changing river stages and river currents, which often necessitate the repositioning, discontinuance, and establishment of floating aids to navigation, many buoys maintained by the U.S. Coast Guard are not shown, with the exception of the Huey P. Long Bridge approach buoys and the lighted wreck buoy "WR4" at mile 115.4. Consult the U.S. Coast Guard Light List (Vol IV, Gulf of Mexico) and the Local Notice to Mariners, for additional information.


TIDAL INFORMATION
Near real time water level data, predictions and weather data are available via the Internet at http://tidesandcurrents.noaa.gov. Annual predictions of the rise and fall of the tides are available in printed form from private sector printers.


VESSEL TRAFFIC SERVICES (VTS)
The U.S. Coast Guard operates a mandatory Vessel Traffic Services (VTS) system in the Lower Mississippi River. Vessel operating procedures and designated radiotelephone frequencies are published in 33 CFR 161, The U.S. Coast Pilot, and/or the VTS User's Manual. Mariners should consult these sources for applicable rules and reporting requirements. Although mandatory VTS participation is limited to the navigable waters of the United States, certain vessels are encouraged or may be required, as a condition of port entry, to report beyond this area to facilitate advance vessel traffic management within the VTS area.


SOUNDING AND COASTLINE DATUM
Soundings and coastlines refer to Low water Reference Plane (LWRP), related to Mean Sea Level, established by the Corps of Engineers. (See profile in external picture file)


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea. �The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil. �For more information, please refer to the Coast Pilot.

END OF FILE 
