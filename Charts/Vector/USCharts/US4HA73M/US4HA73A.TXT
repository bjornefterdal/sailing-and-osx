NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4HA73E - MARO REEF

INDEX:
SYMBOLS AND ABBREVIATIONS
AUTHORITIES
NOTE A
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
COLREGS, 80.1410
CAUTION
CAUTION NOTE - MARO REEF
TIDAL INFORMATION

SYMBOLS AND ABBREVIATIONS: For Symbols and Abbreviations, see Chart No. 1.

AUTHORITIES: Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from 
the U.S. Coast Guard and National Imagery and Mapping Agency.

NOTE A: Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or revisions to 
Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained 
at the Office of the Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office of the 
District Engineer, Corps of Engineers in Honolulu, Hawaii.Refer to Code of Federal Regulations section numbers.

POLLUTION REPORTS: Report all spills of oil and hazardous substances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is 
impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION: Consult U.S. Coast Pilot 7 for important supplemental information.

COLREGS, 80.1410: International Regulations for Preventing Collisions of Sea, 1972. The entire area falls
seaward of the COLREGS Demarcation Line.

CAUTION: Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners.

TIDAL INFORMATION:
No tidal information available Oct. 2002.


END OF FILE