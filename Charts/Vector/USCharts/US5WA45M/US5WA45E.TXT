NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

18424(US5WA45E) - BELLINGHAM BAY

INDEX:
AIDS TO NAVIGATION
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - LIMITATIONS
CANADIAN WEATHER RADIO BROADCAST
COLREGS, 80.1390 (see note A)
TIDAL INFORMATION
ADDITIONAL INFORMATION
NOTE A
NOTE B
CAUTION
WARNING
AUTHORITIES
RADAR REFLECTORS
REGULATED NAVIGATION AREA


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


	   POLLUTION REPORTS
  Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important 
supplemental information.


CAUTION - TEMPORARY
Temporary changes or defects in aids to 
navigation are not indicated.  See 
Local Notice to Mariners.


CAUTION - SUBMARINE PIPELINES AND CABLES
 Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart.  Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed.  Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
 Covered wells may be marked by lighted or 
unlighted buoys.


	  CAUTION - LIMITATIONS
  Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
  Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


CANADIAN WEATHER RADIO BROADCASTS
The National Weather Service station listed 
below provides continuous marine weather broad-
casts. The range of reception is variable, but for 
most stations is usually 20 to 40 miles from the 
antenna site.

Vancouver, B.C.		CFA-240		162.40 MHz


		COLREGS, 80.1390 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Addtional information can be obtained at nauticalcharts.noaa.gov.


			NOTE A
  Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 7.  Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners.  Information concerning the 
regulations may be obtained at the Office of the Commander, 
13th Coast Guard District in Seattle, Washington, or at the 
Office of the District Engineer, Corps of Engineers in 
Seattle, Washington.
  Refer to charted regulation section numbers.


NOTE B 
Mariners are cautioned that the Whatcom County Ferry 
and/or local government may deviate from the published 
standard routes due to inclement weather, traffic conditions, 
navigational hazards or other emergency conditions.  Standard 
ferry routes within the waters of the San Juan Islands are 
not displayed on this chart.


CAUTION - DERDGED AREAS
Improved channels are subject to shoaling, particularly 
at the edges. 


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids.  See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


AUTHORITIES
Hydrography and Topography by the National 
Ocean Service, Coast Survey, with additional 
data from the U.S. Coast Guard, Geological
Survey, and National Geospatial-Intelligence
Agency.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.

REGULATED NAVIGATION AREA
A Regulated Navigation Area has been established by the U.S. Coast Guard.
Please see Chapter 2, U.S. Coast Pilot 7 or 33 CFR 165.1301 and 33 CFR
165.1303.


END OF FILE