The U.S. Coast Guard operates a mandatory
Vessel Traffic Service (VTS) system in Puget
Sound.  Vessel operating procedures and
designated radiotelephone frequencies are
published in 33 CFR 161, the U.S. Coast Pilot,
and/or the VTS User's Manual.  The entire area
of this chart falls within the Vessel Traffic
Service (VTS) system.
