The Little Creek Exclusion Zone is a subset of an emergency 
restricted area. No vessel or persons may enter this area 
without permission of the Commanding Officer/Officer-in-charge 
of the Little Creek Amphibious Base. Vessels or persons may 
transit other portions of the restricted area at any time, but are 
subject to inspections from designated law enforcement patrols.