NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION
US5FL16M - SAINT MARKS RIVER AND APPROACHES

INDEX:
AUTHORITIES	
AIDS TO NAVIGATION		
NOTE A				
WARNING � PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION	
CAUTION - TEMPORARY CHANGES				
RADAR REFLECTORS	
NOAA WEATHER RADIO BROADCASTS	
HURRICANES AND TROPICAL STORMS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast Survey, with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 5. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 8th Coast Guard District in New Orleans, LA or at the Office of the District Engineer, Corps of Engineers in Mobile, AL. 
Refer to charted regulation section numbers.

WARNING � PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important supplemental information.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.
Tallahassee, FL		KIH-24		              162.40 MHz
East Point, FL		WWF-86		162.50 MHz

HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.
Charted soundings, channel depths and shoreline may not reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.
Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.

TIDAL INFORMATION 
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE
