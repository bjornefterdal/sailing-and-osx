AREAS TO BE AVOIDED
All ships, except those bound to an from ports on one of the islands
within the areas, engaged in the trade of carrying cargo, including 
but not limited to tankers an other bulk carriers and barges, should 
avoid these areas. (MSC, IMO 59/33 Annex 21).
