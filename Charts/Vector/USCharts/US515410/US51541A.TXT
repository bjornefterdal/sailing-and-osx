NATIONAL GEOSPATIAL-INTELLIGENCE AGENCY
US515410 - APPROACHES TO BALBOA
 
INDEX:
GLOSSARY
POSITIONS
ADDITONAL INFORMATION
PILOTAGE
MAXIMUM DRAFT OF VESSELS
REGULATIONS AND PROVISIONS
CAUTION 1
CAUTION 2
CAUTION 3

NOTES: 
GLOSSARY:
Arrecife ... reef
Bahia ... bay
bordada ... reach
Cerro ... hill
Esclusas ... locks
Isla ... island
Lago ... lake
Morro ... island
Piedra ... reef rock
Playa ... beach
Puerto ... harbor
Punta ... point
Rio ... stream
Roca(s) ... rocks(s)

POSITIONS:
This ENC cell has been positioned on WGS-84 datum through the use of rectified Ortho-photo of 2009.

ADDITONAL INFORMATION:
Mariners are encouraged to log on to the Panama Canal Web site at www.pancanal.com for additional information on history, news and canal operations.

PILOTAGE:
Pilotage is compulsory for the Panama Canal. The Pilot(s) assigned to a vessel shall have control of the vessel's navigation and movement. See Regulation for Navigation in Canal Waters, Chapter V, published by the Panama Canal Authority, for more information.

MAXIMUM DRAFT OF VESSELS:
The maximum draft of vessels transiting the canal depends in part on the depth of water in Gatun Lake. Since the depth of Gatun Lake varies with the seasons, the latest information on draft restrictions should be consulted before a deep draft passage.

REGULATIONS AND PROVISIONS:
The regulations and provisions concerning vessel transits through the Panama Canal are contained in the Regulation for Navigation in Canal Waters, published by the Panama Canal Authority.

CAUTION 1:
Many factors combine to make the Atlantic and Pacific terminus of the Panama Canal a difficult area to navigate safely. Of particular concern for mariners will be the frequent vessel movements to and from the Canal and anchorages, rain squalls, reducing visibility, and background lights at night making it difficult to identify aids to navigation and the navigation lights of other vessels.

CAUTION 2:
This cell is based upon the latest available information. Mariners are warned that canal depths, channel limits, lock restrictions, area boundaries and aids to navigation may be temporarily moved, displaced or changed without notice due to the ongoing program of maintenance, modernization and improvements.

CAUTION 3:
Temporary changes or defects in aids to navigation are not indicated in this cell.

END OF FILE
