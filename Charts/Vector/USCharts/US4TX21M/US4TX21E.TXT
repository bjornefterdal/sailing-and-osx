NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US4TX21E - ARANSAS PASS TO BAFFIN BAY

INDEX: 
AIDS TO NAVIGATION 
CAUTION - TEMPORARY
WARNING - PRUDENT MARINER
RACING BUOYS
CAUTION- MARINERS
CAUTION � LIMITATIONS
NOTE A
NOAA WEATHER RADIO BROADCASTS
SUPPLEMENTAL INFORMATION
POLLUTION REPORTS
NOTE S
MINERAL DEVELOPMENT STRUCTURES
GAS AND OIL WELL STRUCTURES
CAUTION - SUBMARINE PIPELINES AND CABLES
AUTHORITIES
CAUTION - DREDGED AREAS
HURRICANES AND TROPICAL STORMS
ADDITIONAL INFORMATION
TIDAL INFORMATION
RADAR REFLECTORS


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


RACING BUOYS
Racing Buoys within the limits of this chart are not shown hereon.
Information may be obtained from the U.S. Coast Guard District 
Offices as racing and other privately maintained buoys are not
all listed in the U.S. Coast Guard Light List.


CAUTION- MARINERS
Mariners are warned to stay clear of the protective
riprap surrounding navigational light structures.


CAUTION � LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.


NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 5. Additions or revisions to Chapter 2 are published
in the Notices to Mariners.  Information concerning the
regulations may be obtained at the Office of the Commander,
8th Coast Guard District in New Orleans, LA, or at the Office
of the District Engineer, Corps of Engineers in Galveston, TX.
Refer to charted regulation section numbers.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provide continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Corpus Christi, TX    KHB-41          162.55 MHz
Riviera, TX           WNG-609 	      162.525 MHz


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 5 for important
supplemental information.


POLLUTION REPORTS
Report all spills of oil and hazardous substances
to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication
is impossible (33 CFR 153).


NOTE S
Regulations for Ocean Dumping Sites are
contained in 40 CFR, Parts 220-229.  Additional
information concerning the regulations and
requirements for use of the sites may be obtained
from the Environmental Protection Agency (EPA).
See U.S. Coast Pilots appendix for addresses of
EPA offices. Dumping subsequent to the survey
dates may have reduced the depths shown.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals
are required for fixed mineral development
structures shown, subject to approval by the
District Commander, U.S. Coast Guard (33 CFR 67).


GAS AND OIL WELL STRUCTURES
Gas and oil well structures exist within the area of this chart.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exist, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey with additional data from the Corps of Engineers, Geological 
Survey, and U.S. Coast Guard.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly 
at the edges.


RULES OF THE ROAD
The "Rules of the Road" state that recreational boats shall 
not impede the passage of a vessel that can navigate only within 
a narrow channel or fairway. Large vessels may appear to move slowly 
due to their large size but actually transit at speeds in excess 
of 12 knots, requiring a great distance in which to maneuver or stop. 
A large vessel's superstructure may block the wind with the result that 
sailboats and sailboards may unexpectedly find themselves unable to 
maneuver. Bow and stern waves can be hazardous to small vessels. 
Large vessels may not be able to see small craft close to their bows. 
Covered wells may be marked by lighted or unlighted buoys.


Small craft should stay clear of large commercial and government 
vessels even if small craft have the right-of-way. All craft should 
avoid areas where the skin divers flag, a red square with a diagonal 
white stripe, is displayed.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may
cause considerable damage to marine structures, aids to
navigation and moored vessels, resulting in submerged debris
in unknown locations.
Charted soundings, channel depths and shoreline may not
reflect actual conditions following these storms. Fixed aids to
navigation may have been damaged or destroyed. Buoys may
have been moved from their charted positions, damaged, sunk,
extinguished or otherwise made inoperative. Mariners should
not rely upon the position or operation of an aid to navigation.
Wrecks and submerged obstructions may have been displaced
from charted locations. Pipelines may have become uncovered
or moved.
Mariners are urged to exercise extreme caution and are
requested to report aids to navigation discrepancies and
hazards to navigation to the nearest United States Coast Guard
unit.


ADDITIONAL INFORMATION
Additional information can be obtained at WWW.nauticalcharts.noaa gov.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to
http://co-ops.nos.noaa.gov.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.


END OF FILE
