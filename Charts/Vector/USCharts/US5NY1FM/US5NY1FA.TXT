NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NY1FE - HARLEM RIVER

INDEX:
AUTHORITIES
NOAA WEATHER RADIO BROADCASTS
NOTE A
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
AIDS TO NAVIGATION
CAUTION - TEMPORARY CHANGES
CAUTION - SUBMARINE PIPELINES AND CABLES
WARNING - PRUDENT MARINER
NOTE B
CAUTION - MARINERS
RADAR REFLECTORS
TIDAL INFORMATION
ADMINISTRATION AREA
ADDITIONAL INFORMATION



NOTES:
AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, U.S. Coast  
Guard, and National Geospatial-Intelligence Agency.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed
below provides continuous weather broadcasts. 
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations. 

New York, NY	KWO-35		162.550 MHz


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub- 
lished in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Com- 
mander, 1st Coast Guard District in Boston, MA or at the
Office of the District Engineer, Corps of Engineers in 
New York, N.Y.
Refer to charted regulation section numbers.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or
to the nearest U.S. Coast Guard facility if telephone com- 
munication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 2 for important
supplemental information.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are
replaced by other types or removed. For details
see U.S. Coast Guard Light List.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.s. Coast Pilot for details.


NOTE B
The U.S. Coast Guard operates a mandatory Vessel Traffic
Services (VTS) system in the New York Bay and sur-
rounding areas. Vessel operating procedures and designated 
radiotelephone frequencies are published in 33 CFR 161, the 
U.S. Coast Pilot, and/ or the VTS User's Manual. Mariners 
should consult these souces for applicable rules and 
reporting requirements. Although mandatory VTS parti-
cipation is limited to the navigable waters of the United 
States, certain vessels are encouraged or may be required,
as a condition of port entry, to report beyond this area to 
facilitate vessel traffic managment within the VTS area.


CAUTION - MARINERS
Mariners are warned to stay clear of the pro-
tective riprap surrounding navigational light
structures.


RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil.  For more information, please refer to the Coast Pilot. 


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


END OF FILE