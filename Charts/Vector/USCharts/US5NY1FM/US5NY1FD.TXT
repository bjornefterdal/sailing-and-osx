Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 2. Additions or revisions to Chapter 2 are pub- 
lished in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Com- 
mander, 1st Coast Guard District in Boston, MA or at the
Office of the District Engineer, Corps of Engineers in 
New York, N.Y.
Refer to charted regulation section numbers.


Safety and Security Zones: 33 CFR 165.169. The Coast Guard has established permanent
safety and security zones within 25 yards of each of the following locations: bridge
piers and abutments, overhead power cable towers, piers, tunnel ventilators, commercial
waterfront facilities and 25 yards from the outboard side of any docked commercial
vessel at a commercial facility. The Captain of the Port will notify the maritime
community of periods during which these safety and security zones will be enforced in
accordance with methods identified in 33 CFR 165.7. No person or vessel may enter or
remain in a prescribed safety or security zone at any time without the permission of the
Captain of The Port of New York.