NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5MI64E - SAGINAW RIVER

INDEX:
NOTE A
AUTHORITIES
SYMBOLS AND ABBREVIATIONS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
BRIDGE AND OVERHEAD CABLE CLEARANCES
WARNING - PRUDENT MARINER
NOAA VHF-FM WEATHER BROADCASTS
AIDS TO NAVIGATION
CAUTION - SUBMARINE PIPELINES AND CABLES
CAUTION - LIMITATIONS
CAUTION - TEMPORARY CHANGES
CAUTION - IMPROVED CHANNELS
CAUTION
CAUTION - MARINERS
SAILING DIRECTIONS
SAILING COURSES
TIDAL INFORMATION
ADDITIONAL INFORMATION
RADAR REFLECTORS
POTABLE WATER INTAKE(PWI)



NOTES:
NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 6. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 9th Coast Guard District in Cleveland, Ohio or at the Office of the District Engineer, Corps of Engineers in 
Detroit, Michigan.
Refer to charted regulation section numbers.


AUTHORITIES
Hydrography and Topography by the National Ocean Service, Coast 
Survey, with additional data from the Corps of Engineers, Geological Survey and U.S. Coast Guard.


SYMBOLS AND ABBREVIATIONS
For complete list of symbols and abbreviations see Chart No. 1.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National 
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 6 for important supplemental information.


BRIDGE AND OVERHEAD CABLE CLEARANCES
When the water surface is above Low Water Datum, bridge and overhead clearances are reduced correspondingly. For clearances see U.S.Coast Pilot 6.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio station listed below provides continuous weather broadcasts. The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for station at 
high elevations.

Clio, MI		      KIH-29		162.400 MHz
West Branch, MI		KXI-33		162.450 MHZ


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area of this chart. Not all submarine pipelines and submarine cables are required to be buried, and those that were originally buried may have become exposed. Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as aids to marine navigation can be found in the U.S.Coast Guard Light Lists and National Geospatial-Intelligence Agency Publication 117. Radio direction-finder bearings to commercial broadcasting stations are subject to error and should be used with caution.



CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See 
Local Notice to Mariners. During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed. For details see U.S. Coast Guard Light List.


CAUTION - IMPROVED CHANNELS
Improved channels are subject to shoaling, particularly at the edges.


CAUTION
Due to periodic high water conditions in the Great Lakes, some features charted as visible at Low Water Datum may be submerged, particularly in the near shore areas. Mariners should proceed with caution.


CAUTION - MARINERS
Mariners are warned that numerous uncharted stakes and fishing structures, some submerged, may exist in the area of this chart. Such structures are not charted unless known to be permanent.


SAILING DIRECTIONS
Bearings of sailing courses are true and distances given thereon are in statute miles between points of departure.

SAILING COURSES
Sailing courses and limits are recommended by the Lake Carriers Association and Canadian Ship owners Association


TIDAL INFORMATION
For tidal information see the NOS tide table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at nauticalcharts.noaa.gov.


RADAR REFLECTORS
Radar Reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.

POTABLE WATER INTAKE(PWI)
Vessels operating in fresh water lakes or rivers shall not discharge sewage, or ballast, or bilge water within such areas adjacent to domestic water intakes as are designated by the Commissioner of Food and Drugs (21 CFR 1250.93). Consult U.S. Coast Pilot 6 for important supplemental information.

END OF FILE
