NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5HA62M - PORT ALLEN

INDEX:
AUTHORITIES
AIDS TO NAVIGATION
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
SUBMERGED OPERATIONS
CAUTION � TEMPORARY CHANGES
CAUTION � INSHORE WATERS
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION
ADMINISTRATION AREA


AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast and Geodetic Survey with additional data from the Corps of Engineers, Geological Survey and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for supplemental information concerning aids to navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. Coast Pilot 7.  Additions or revisions to Chapter 2 are published in the Notice to Mariners.  Information concerning the regulations may be obtained at the Office of the Commander, 14th Coast Guard District in Honolulu, Hawaii or at the Office of the District Engineer, Corps of Engineers in Honolulu, Hawaii. Refer to Code of Federal Regulations section numbers. 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 7 for important supplemental information.

SUBMERGED OPERATIONS
Submerged submarine operations are conducted at various times within the waters contained on this chart. Proceed with caution.

CAUTION � TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated.  See Local Notice to Mariners.

CAUTION � INSHORE WATERS
Mariners are urged to exercise extreme caution when transiting inshore waters due to changes caused by the hurricane of November 1982.

RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation.  Individual radar reflector identification on these aids has been omitted.

NOAA WEATHER RADIO BROADCASTS: The NOAA Weather Radio stations listed below provide continuous weather broadcasts.  The reception range is typically 20 to 40 nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

Kokee, HI		KBA-99		162.40 MHz

TIDAL INFORMATION:
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.

ADDITIONAL INFORMATION:
Additional information can be obtained at www.nauticalcharts.noaa.gov

ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil.  For more information, please refer to the Coast Pilot. 

END OF FILE
