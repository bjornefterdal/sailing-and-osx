NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5NJ12M - RARITAN RIVER

INDEX:
AIDS TO NAVIGATION
AUTHORITIES
CAUTION - TEMPORARY CHANGES
CAUTION - OBSTRUCTIONS
CAUTION - MARINERS
CAUTION - DREDGED AREAS
COMMENTS
NOAA WEATHER RADIO BROADCASTS
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
VESSEL TRAFFIC SERVICE
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION
NOTE A
RADAR REFLECTORS
CAUTION - SUBMARINE PIPELINES AND CABLES
HURRICANES AND TROPICAL STORMS
ADMINISTRATION AREA

NOTES:

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List forsupplemental information concerning aids to navigation.
 

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast survey, with additional data from the Corps of Engineers, Geological Survey, and U.S. Coast Guard.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to navigation are not indicated. See Local Notice to Mariners. During some winter months or when endangered by ice, certain aids to navigation are replaced by other types or removed.  For details see U.S. Coast Guard Light List.


CAUTION - OBSTRUCTIONS
Fixed and floating obstructions, some submerged, may exist within the bridge construction area. Mariners are advised to proceed with caution.


CAUTION - MARINERS	
Mariners are warned to stay clear of the protective riprap surrounding navigational light structures.


CAUTION - DREDGED AREAS
Improved channels are subject to shoaling, particularly at the edges.


COMMENTS
This nautical chart has been designed to promote safe navigation.  The National Ocean Service encourages users to submit corrections, additions, or comments for improving this chart to the Chief, Marine Chart Division (N/CS2), National Ocean Service, NOAA, Silver Spring, Maryland, 20910-3282


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather broadcasts. The reception range is typically 20 to 40
nautical miles from the antenna site, but can be as much as 100 nautical miles for stations at high elevations.

New York, NY        KWO-35   162.550 MHz


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility if telephone communication is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 2 for important supplemental information.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on any single aid to navigation, particularly on floating aids. See U.S. Coast Guard Light List and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


NOTE A
Navigation regulations are published in Chapter 2, U.S.Coast Pilot 2. Additions or revisions to Chapter 2 are published in the Notice to Mariners. Information concerning the regulations may be obtained at the Office of the Commander, 1st Coast Guard District in Boston, MA or at the Office of the District Engineer, Corps of Engineers in New York, NY.

Refer to charted regulation section numbers.


RADAR REFLECTORS
Radar reflectors have been placed on many floating aids to navigation. Individual radar reflector identification on these aids has been omitted from this chart.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and submarine cables may exist within the area. Not all submarine pipelines and sub-
marine cables are required to be buried, and those that were originally buried may have become exposed.  Mariners should use extreme caution when operating vessels in depths of water comparable to their draft in areas where pipelines and cables may exist, and when anchoring, dragging, or trawling. Covered wells may be marked by lighted or unlighted buoys.


HURRICANES AND TROPICAL STORMS
Hurricanes, tropical storms and other major storms may cause considerable damage to marine structures, aids to navigation and moored vessels, resulting in submerged debris in unknown locations.  Charted soundings, channel depths and shoreline may not
reflect actual conditions following these storms. Fixed aids to navigation may have been damaged or destroyed. Buoys may have been moved from their charted positions, damaged, sunk, extinguished or otherwise made inoperative. Mariners should not rely upon the position or operation of an aid to navigation. Wrecks and submerged obstructions may have been displaced from charted locations. Pipelines may have become uncovered or moved.  

Mariners are urged to exercise extreme caution and are requested to report aids to navigation discrepancies and hazards to navigation to the nearest United States Coast Guard unit.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an Administration Area. This area covers land, internal waters, and territorial sea.  The territorial sea is a maritime zone which the United States exercises sovereignty extending to the airspace as well as to its bed and subsoil.  For more information, please refer to the Coast Pilot.


END OF FILE
