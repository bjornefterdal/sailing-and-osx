NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION 

US5AL14E - MOBILE BAY, EAST FOWL RIVER TO DEER RIVER POINT

INDEX:
AUTHORITIES
POLLUTION REPORTS
CAUTION - LIMITATIONS
SUPPLEMENTAL INFOMATION
NOTE A
CAUTION - TEMPORARY
NOAA WEATHER RADIO BROADCASTS
RACING BUOYS
CAUTION - DREDGED AREAS
WARNING - PRUDENT MARINER
AIDS TO NAVIGATION
RADAR REFLECTORS
MINERAL DEVELOPMENT STRUCTURES
CAUTION -HURRICANES AND TROPICAL STORMS
TIDAL INFORMATION
ADDITIONAL INFOMATION



AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional data 
from the Corps of Engineers, Geological Survey, 
U.S. Coast Guard, and National Geospatial-
intelligence Agency.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802(toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


SUPPLEMENTAL INFORMATION
Consult U.S.Coast Pilot 5 for important 
supplemental information.


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 5. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. information concerning 
the regulations may be obtained at the Office of the Com-
mander, 7th Coast Guard District in Miami, FL, and 8th 
Coast Guard District in New Orleans, LA, or at the Office 
of the District Engineer, Corps of Engineers in Mobile, AL.
Refer to charted regulation section numbers.


CAUTION - TEMPOARY
Temporary changes or defects in aids to 
navigation are not indicated. see 
Local Notice to Mariners.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed below provide continuous weather 
broadcasts. The reception range is typically 37 to 74 km / 20 to 40 nautical 
miles from the antenna site, but can be as much as 186 km / 100 nautical 
miles for stations at high elevations.

Mobile, AL       KEC-61       162.55 MHz
Pensacola, FL    KEC-86       162.40 MHz


RACING BUOYS
Racing buoys within the limits of this chart 
are not shown hereon. Information may be 
obtained from the U.S. Coast Guard District 
Offices as racing and other private buoys are 
not all listed in the U.S. Coast Guard Light List.


CAUTION - DREDGED AREAS
Improved channels are 
subject to shoaling, particularly at the edges.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly or 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


MINERAL DEVELOPMENT STRUCTURES
Obstruction lights and sound (fog) signals 
are required for fixed mineral development 
structures, subject to approval by the District 
Commander, U.S. Coast Guard (33 CFR 67).


CAUTION - HURRICANES AND TROPICAL STORMS
Hurricanes and tropical storms disturb objects on the sea floor and cause considerable 
damage to offshore structures, aids to navigation and moored vessels resulting in 
extensive debris being submerged in unknown locations.  Wrecks and submerged 
obstructions may have been displaced from charted locations, and pipelines may 
have become uncovered or moved due to the force of storm surge. Aids to navigation 
may not be reliable immediately following such storms. Mariners are urged to exercise 
extreme caution and are requested to report aids to navigation discrepancies and 
hazards to navigation to the nearest United States Coast Guard Unit.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


TIDAL INFORMATION
For tidal information, see the NOS tide table publication or go to http://co-ops.nos.noaa.gov.


END OF FILE