When the water surface is above Low Water Datum, bridge 
and overhead clearances 
are reduced correspondingly.  
For clearance see U.S. Coast Pilot 6.