NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5ME37M - WINTER HARBOR

INDEX:
AUTHORITIES	
AIDS TO NAVIGATION
NOTE A
WARNING - PRUDENT MARINER
POLLUTION REPORTS
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY CHANGES
RADAR REFLECTORS
NOAA WEATHER RADIO BROADCASTS
COLREGS demaracation line 80.105
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast 
Survey, with additional data from the Geological Survey, and U.S.
Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.

NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 1. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the 
Office of the District Engineer, Corps of Engineers in 
Concord, MA.
Refer to charted regulation section numbers. 

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the 
National Response Center via 1-800-424-8802 (toll free), or 
to the nearest U.S. Coast Guard facility if telephone com-
munication is impossible (33 CFR 153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 1 for important 
supplemental information.

CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details 
see U.S. Coast Guard Light List.

RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts. 
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be 
as much as 100 nautical miles for stations at 
high elevations.

Ellsworth, ME		KEC-93		162.40  MHz
Jonesboro Marine, ME	WNG-543	        162.450 MHz

COLREGS demaracation line 80.105
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of theis chart falls seaward of the COLREGS Demarcation Line.

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.

END OF FILE