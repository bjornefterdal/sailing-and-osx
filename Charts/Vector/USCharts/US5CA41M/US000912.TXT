BENICIA-MARTINEZ HIGHWAY BRIDGE
Privately maintained lights marking this bridge are shown as follows:
An occulting red light, with 3 fixed white lights in a vertical line above,
on each side of the span over the middle of Suisun Pt. Reach. The
occulting red lights change to occulting green when the lift span of the
adjacent railroad bridge is fully open.
A fixed green light on each side of the span over the middle of the
channel between Piers 6 and 7.
Fixed red lights mark Piers 4 through 12 and fixed green lights mark
the center of the spans.