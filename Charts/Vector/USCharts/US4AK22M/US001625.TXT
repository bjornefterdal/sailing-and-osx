Offshore Vessel Traffic Management Recommendations
Based on the West Coast Offshore Vessel Traffic Risk Management Project, which was 
co-sponsored by the Pacific States/British Columbia Oil Spill Task Force and U.S. Guard 
Pacific Area, it is recommended that, where no other traffic management areas exist such 
as Traffic Separation Schemes, Vessel Traffic Services, or recommended routes, vessels 300 
gross tons or larger transiting along the coast anywhere between Cook Inlet and San Diego 
should voluntarily stay a minimum distance of 25 nautical miles offshore. It is also 
recommended that tank ships laden with persistent petroleum products and transiting along 
the coast between Cook Inlet and San Diego should voluntarily stay a minimum distance of 50 
nautical miles offshore. Vessels transiting short distances between adjacent ports should 
seek routing guidance as needed from the local Captain of the Port or VTS authority for 
that area. This recommendation is intended to reduce the potential for vessel groundings 
and resulting oil spills in the event of a vessel casualty.