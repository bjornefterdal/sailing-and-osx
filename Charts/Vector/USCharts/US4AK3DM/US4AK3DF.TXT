                        Whale Waters

Special restrictions to navigation apply in this area May 15 - Sep. 30 to
protect humpback whales. Vessel speed is restricted to 20 knots through-
the-water and may be reduced to 13 knots through-the-water during times of
high whale occurrence. Vessels over 18 feet in length transiting the area are
required to navigate mid-channel or a distance of 1 nautical mile from shore.
Motor vessels are prohibited from operating within 0.25 nautical miles of
whales and may not alter course for the purpose of approaching a whale
which is within 0.5 nautical miles. Contact KWM-20 Bartlett Cove when
entering Glacier Bay for information on speed limits and other restric-
tions that may apply.
A full description of the regulation may be found in the U.S. Coast Pilot.
