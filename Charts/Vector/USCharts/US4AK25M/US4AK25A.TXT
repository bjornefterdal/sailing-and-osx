Hinchinbrook Entrance Sub-surface Mooring
"HE1" is 238 meters / 781 feet in length, with 
the uppermost buoy at 47.2 meters / 155 feet 
below the surface. Oceanographic instruments 
measuring currents are transmitting at 300khz. 
Deployed on 4/22/08. Moorings are scheduled to be 
recovered, serviced and re-deployed in September 2008.
