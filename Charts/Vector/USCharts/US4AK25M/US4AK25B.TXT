Hinchinbrook Entrance Sub-surface Mooring 
"HE3" is 166.7 meters / 547 feet in length, with 
the uppermost buoy at 51.2 meters / 168 feet 
below the surface. Oceanographic instruments 
measuring currents are transmitting at 300khz. 
Deployed on 4/22/08. Moorings are scheduled to be 
recovered, serviced and re-deployed in September 2008.
