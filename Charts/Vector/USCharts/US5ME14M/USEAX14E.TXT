NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5ME14E - DAMARISCOTTA, SHEEPSCOT AND KENNEBEC RIVERS

INDEX:
AUTHORITIES		
AIDS TO NAVIGATION	
NOTE A			
WARNING - PRUDENT MARINER
POLLUTION REPORTS	
SUPPLEMENTAL INFORMATION
CAUTION - TEMPORARY
CAUTION - LIMITATIONS		
RADAR REFLECTORS	
RACING BUOYS
SUBMARINE PIPELINES AND CABLES
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION

NOTES:	

AUTHORITIES
Hydrography and topography by the National
Ocean Service, Coast Survey, with additional
data from the Corps of Engineers, Geological
Survey, and U.S. Coast Guard.

AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.  

NOTE A
Navigation regulations are published in Chapter 2, U.S.
Coast Pilot 1. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA or at the
Office of the District Engineer, Corps of Engineers in
Concord, MA.
Refer to charted regulation section numbers.

WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids. See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.

POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National
Response Center via 1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication is impossible (33 CFR-
153).

SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 1 for important
supplemental information.

CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are
replaced by other types or removed. For details
see U.S. Coast Guard Light List.

CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.

RADAR REFLECTORS
Radar reflectors have been placed on many
floating aids to navigation. Individual radar
reflector identification on these aids has been
omitted from this chart.

RACING BUOYS
Racing buoys within the limits of this chart
are not shown hereon. Information may be
obtained from the U.S. Coast Guard District
Offices as racing and other private buoys are
not all listed in the U.S. Coast Guard Light List.

SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and
submarine cables may exist within the area of
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and
those that were originally buried may have
become exposed. Mariners should use extreme
caution when operating vessels in depths of
water comparable to their draft in areas where
pipelines and cables may exists, and when
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or
unlighted buoys.

NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provides continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Portland, ME	KDO-95	       162.55 MHz
Dresden, ME	WXM-60	       162.475 MHz

TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-
ops.nos.noaa.gov.

ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov

END OF FILE