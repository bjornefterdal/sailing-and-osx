NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5MA30E - PROVINCETOWN HARBOR

INDEX:
AUTHORITIES			
AIDS TO NAVIGATION		
NOTE A				
WARNING - PRUDENT MARINER	
POLLUTION REPORTS		
SUPPLEMENTAL INFORMATION	
CAUTION - TEMPORARY CHANGES			
CAUTION	- LIMITATIONS					
RADAR REFLECTORS		
COLREGS, 80.135 and 80.145 (see note A)
NOAA WEATHER RADIO BROADCASTS
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:

AUTHORITIES
Hydrography and topography by the National 
Ocean Service, Coast Survey, with additional 
data from the Corps of Engineers and U.S. 
Coast Guard.


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


NOTE A
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilots 1 & 2. Additions or revisions to Chapter 2 are 
published in the Notice to Mariners. Information con-
cerning the regulations may be obtained at the Office of 
the Commander, 1st Coast Guard District in Boston, MA 
or at the Office of the District Engineer, Corps of Eng-
ineers in Concord, MA.
Refer to charted regulation section numbers.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on 
any single aid to navigation, particularly on 
floating aids. See U.S. Coast Guard Light List 
and U.S. Coast Pilot for details.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S. 
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilots 1 & 2 for important 
supplemental information.


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are 
replaced by other types or removed. For details
see U.S. Coast Guard Light List.


CAUTION - LIMITATIONS
Limitations on the use of radio signals as 
aids to marine navigation can be found in the 
U.S. Coast Guard Light Lists and National 
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial 
broadcasting stations are subject to error and 
should be used with caution.


RADAR REFLECTORS
Radar reflectors have been placed on many 
floating aids to navigation. Individual radar 
reflector identification on these aids has been 
omitted from this chart.


COLREGS, 80.135 and 80.145 (see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area falls seaward of the COLREGS Demarcation Line.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed 
below provide continuous weather broadcasts.  
The reception range is typically 20 to 40 
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations. 

Hyannis, MA	KEG-73		162.550 MHz
Boston, MA	KHB-35		162.475 MHz


TIDAL INFORMATION
For tidal information see the NOS tide table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.	


END OF FILE
