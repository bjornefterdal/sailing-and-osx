33 CFR 165.153 REGULATED NAVIGATION AREA and LIMITED ACCESS AREA.

Item (1 of 9):
In the vicinity of Naval Submarine Base New London and Lower Thames River. Unless 
authorized by the Captain of the Port (COTP), vessels of 300 gross tons or more may not 
proceed at a speed in excess of 8 knots in the Thames River from New London Harbor Channel 
Buoys 7 and 8 north through the upper limit of the Naval Submarine Base New London 
Restricted Area, as that area is specified in 33 CFR 334.75(a). The U.S. Navy and other 
Federal, State and municipal agencies may assist the U.S. Coast Guard in the enforcement 
of this rule. 

Item (2 of 9):
	Enhanced Communications. Vessels of 300 gross tons or more, all vessels engaged in 
towing barges, must security calls on marine band or Very High Frequency (VHF) radio channel
16 upon approach of the following locations: (i)Inbound approach to Cerberus Shoal and, 
(ii) Outbound approach to Race Rock Light.

Item (3 of 9):
	All vessels operating within the RNA, that are bound for a port or place located in
the United States or that must transit the internal waters of the United States, must be 
inspected to the satisfaction of the U.S. Coast Guard, before entering waters within three 
nautical miles from the territorial sea baseline. Vessels awaiting inspection will be 
required to anchor in the manner directed by the COTP. This section does not apply to 
vessels operating exclusively within the Long Island Sound Marine Inspection and COTP Zone, 
vessels on a singe voyage which depart and return to the same port or place within the RNA,
all towing vessels engaged in coastwise trade, vessels in innocent passage not bound for a 
port or place subject to the jurisdiction of the United States, and all vessels not engaged
in commercial service whose last port of call was in the United States. Vessels requiring 
inspection by the COTP may contact the COTP via marine band or Very High Frequency (VHF) 
channel 16 telephone (203)468-4401, faxsimile at (203)468-4418,or letter address to Captain 
of the Port, Long Island Sound, 120 Woodward Ave, New Haven, CT 06512.

Item (4 of 9):
	All vessels operating within the RNA that are bound for a port or place located in 
the United States or that must transit the internal waters of the United States, must 
obtain authorization from the Captain of the Port (COTP) before entering waters within 
three nautical miles from the territorial sea baseline. Vessels awaiting COTP authorization
to enter waters three nautical miles from the territorial sea baseline will be required to 
anchor in a manner directed by the COTP. This section does not apply to vessels operating 
exclusively within the Long Island Sound Marine Inspection Zone, vessels on a single voyage 
which depart from and return to the same port or place within the RNA, all towing vessels 
engaged in coastwise trade, vessels in innocent passage not bound for a port or place 
subject to the jurisdiction of the United States, and all vessels not engaged in commercial
service whose last port of call was in the United States. Vessels may request authorization 
of the COTP by contacting the COTP via marine band or Very High Frequency (VHF) channel 16,
telephone at (203)468-4401, facsimile at (203)468-4418, or letter addressed to Captain of 
the Port, Long Island Sound, 120 Woodward Ave, New Haven, CT 06512.

Item (5 of 9):
	Vessels over 1600 gross tons operating in the RNA within three nautical miles from 
the territorial sea baseline, that are bound for a port or place located in the United States
or must transit the internal waters of the United States must receive authorization on from 
the COTP prior to the transiting or any intentional vessel movements including, but not 
limited to, shifting berths, departing anchorage, or getting underway from a mooring. This 
section does not apply to vessels in innocent passage not bound for a port or place subject 
to the jurisdiction of the United States.

Item (6 of 9):
	Ferry vessels. Vessels of 300 gross tons or more, are prohibited from entering all 
waters within a 1097.2-meters/1200-yard radius of any ferry vessel transiting any portion 
of the Long Island Sound Marine Inspection and COTP Zone, without first obtaining the 
express prior authorization of the ferry vessel licensed operator, licensed master, COTP, 
or the designated COTP on-scene patrol.

Item (7 of 9):
	Vessels engaged in commercial service. No vessel may enter within a 
914.4-meter/100-yard radius of any vessel engaged in commercial service while that vessel 
is transiting, moored or berthed in any portion of Long Island Sound Marine Inspection and 
COTP Zone, without the express prior authorization of the vessel's licensed operator, 
licensed master, COTP, or the designated COTP on-scene representative.

Item (8 of 9):
	Bridge foundations. Any vessel operating beneath a bridge, must make a direct, 
immediate and expeditious passage beneath the bridge, while remaining within the navigable 
channel. No vessel may stop, moor, anchor or loiter beneath the bridge at any time. No 
vessel may approach within a 22.8-meter/25-yard radius of any bridge foundation, support, 
stanchion, pier or abutment, except as required for the direct, immediate and expeditious 
transit beneath a bridge.

Item (9 of 9):
	This section does not relieve any vessel from compliance with applicable navigation 
rules.


NOTE A:
Navigation regulations are published in Chapter 2, U.S. 
Coast Pilots 2 and 3. Additions or revisions to Chapter 2 are 
published in the Notice to Mariners. Information concerning 
the regulations may be obtained at the Office of the Com-
mander, 1st Coast Guard District in Boston, MA, and 5th 
Coast Guard District in Portsmouth, VA, or at the Office of 
the District Engineer, Corps of Engineers, in New York, NY. 
Refer to charted regulation section numbers.