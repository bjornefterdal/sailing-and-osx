For bascule bridges, whose spans do not 
open to a full upright or vertical position, unlimited 
vertical clearances is not available for the entire 
charted horizontal clearance.