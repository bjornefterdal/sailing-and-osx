Navigation regulations are published in Chapter 2, U.S. 
Coast Pilot 7. Additions or revisions to Chapter 2 are pub-
lished in the Notice to Mariners. Information concerning the 
regulations may be obtained at the Office of the Commander, 
13th Coast Guard District in Seattle, Washington or at the 
Office of the District Engineer, Corps of Engineers in 
Seattle, Washington.
Refer to charted regulation section numbers.