NOAA ENC�

NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US5AK10M - UNAKWIK INLET TO ESTHER PASSAGE AND COLLEGE FIORD

INDEX:
NOTE A
ADMINISTRATION AREA
SUPPLEMENTAL INFORMATION
POLLUTION REPORTS
CAUTION - TEMPORARY CHANGES
NOAA WEATHER RADIO BROADCASTS
AIDS TO NAVIGATION
CAUTION - SUBMARINE PIPELINES AND CABLES
WARNING - PRUDENT MARINER
AUTHORITIES
COLREGS, 80.1705(see note A)
FISHING AND HUNTING STRUCTURES
TIDAL INFORMATION
ADDITIONAL INFORMATION


NOTES:
NOTE A
Navigation regulations are published in
Chapter 2, U.S. Coast Pilot 9. Additions or
revisions to Chapter 2 are published in the
Notice to Mariners. Information concerning
the regulations may be obtained at the Office
of the Commander, 17th Coast Guard District
in Juneau, Alaska or at the Office of the District
Engineer, Corps of Engineers in Anchorage,
Alaska.
Refer to charted regulation section numbers.


ADMINISTRATION AREA
The entire extent of this ENC cell falls within the limits of an
Administration Area. This area covers land, internal waters, and
territorial sea. The territorial sea is a maritime zone which the United
States exercises sovereignty extending to the airspace as well as to its
bed and subsoil. For more information, please refer to the Coast Pilot.


SUPPLEMENTAL INFORMATION
Consult U.S. Coast Pilot 9 for important 
supplemental information.


POLLUTION REPORTS
Report all spills of oil and hazardous substances to the National Response
Center via 1-800-424-8802 (toll free), or to the nearest U.S. Coast Guard facility
if telephone communication is impossible (33 CFR 153).


CAUTION - TEMPORARY CHANGES
Temporary changes or defects in aids to 
navigation are not indicated. See 
Local Notice to Mariners.
During some winter months or when endan-
gered by ice, certain aids to navigation are
replaced by other types or removed.  For details 
see U.S. Coast Guard Light List.


NOAA WEATHER RADIO BROADCASTS
The NOAA Weather Radio stations listed
below provides continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.
	
Rugged I, AK		WNG-526		162.425 MHz
Naked I, AK		WNG-530		162.500 MHz
Point Pigot, AK		KZZ-93		162.450 MHz
Cape Hinchinbrook	WNG-532		162.525 MHz
Potato Point, Ak	WNG-527		162.425 MHz
Wasilla, AK		KZZ-98		162.400 MHz 
Valdez,  AK		WXJ-63		162.550 MHz
Cordova, AK		WXJ-79		162.400 MHz
Whittier, AK		KXI-29		162.400 MHz
East Point, Ak		WNG-530		162.500 MHz


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for 
supplemental information concerning aids to 
navigation.


CAUTION - SUBMARINE PIPELINES AND CABLES
Additional uncharted submarine pipelines and 
submarine cables may exist within the area of 
this chart. Not all submarine pipelines and sub-
marine cables are required to be buried, and 
those that were originally buried may have 
become exposed. Mariners should use extreme 
caution when operating vessels in depths of 
water comparable to their draft in areas where 
pipelines and cables may exist, and when 
anchoring, dragging, or trawling.
Covered wells may be marked by lighted or 
unlighted buoys.


WARNING - PRUDENT MARINER 
The prudent mariner will not rely solely on any single aid
to navigation, particularly on floating aids. See U.S. Coast
Guard Light List and U.S. Coast Pilot for details.


AUTHORITIES 
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the U.S. Coast Guard, Geological
Survey, and National Geospatial-Intelligence Agency.


COLREGS, 80.1705(see note A)
International Regulations for Preventing Collisions at Sea, 1972.
The entire area of this chart falls seaward of the COLREGS Demarcation Line.


FISHING AND HUNTING STRUCTURES
Uncharted fish and wildlife harvesting devices
and structures such as fish traps, pound nets,
crab traps, and duck blinds, some submerged,
may exist in the area of this chart, particularly in
the near shore area. Mariners should proceed
with caution. 


TIDAL INFORMATION
For tidal information see the NOS Tide Table publication or go to http://co-ops.nos.noaa.gov.


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov.


END OF FILE


