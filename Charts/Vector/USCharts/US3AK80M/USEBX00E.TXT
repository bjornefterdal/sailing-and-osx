NATIONAL OCEANIC AND ATMOSPHERIC ADMINISTRATION

US3AK80E - NORTON SOUND TO BERING STRAIT

INDEX:
NOTE A
NOTE B - FLOATING AIDS
AIDS TO NAVIGATION
CAUTION - TEMPORARY
POLLUTION REPORTS
NOAA WEATHER RADIOBROADCASTS
NOTE C
AUTHORITIES
WARNING - PRUDENT MARINER
TIDAL INFORMATION
ADDITIONAL INFORMATION
CAUTION - LIMITATIONS

NOTES:
NOTE A
Navigation regulations are published in 
Chapter 2, U.S. Coast Pilot 9. Additions or
revisions to Chapter 2 are published in the 
Notices to Mariners.  Information concerning 
the regulations may be obtained at the Office 
of the Commander, 17th Coast Guard District 
in Juneau, Alaska, or at the Office of the District 
Engineer, Corps of Engineers in Anchorage, 
Alaska.
Refer to charted regulation section numbers.


NOTE B - FLOATING AIDS
Aids maintained for periods indicated to
mark the channels to following places:
Koyuk River (July 1 to Oct. 1)
Unalakeet River (June 1 to Nov. 1)


AIDS TO NAVIGATION
Consult U.S. Coast Guard Light List for
supplemental information concerning aids to
navigation.


CAUTION - TEMPORARY
Temporary changes or defects in aids to
navigation are not indicated. See
Local Notice to Mariners.


POLLUTION REPORTS
Report all spills of oil and hazardous sub-
stances to the National Response Center via 
1-800-424-8802 (toll free), or to the nearest U.S.
Coast Guard facility if telephone communication 
is impossible (33 CFR 153).


NOAA WEATHER RADIOBROADCASTS
The NOAA Weather Radio station listed
below provides continuous weather broadcasts.
The reception range is typically 20 to 40
nautical miles from the antenna site, but can be
as much as 100 nautical miles for stations at
high elevations.

Nome, AK   WXJ-62          162.55 MHz


NOTE C
Maritime boundary provisionally applied pending formal exchange of instruments
of ratification. According to Article 3 of the Agreement Between the United States
of America and Russia on the Maritime Boundary, signed June 1,1990:
1. In any area east of the maritime boundary that lies within 200
nautical miles of the baseline from which the breadth of the territorial sea
of Russia is measured but beyond 200 nautical miles of the baselines
from which the breadth of the territorial sea of the United States is mea-
sured (eastern special area), Russia agrees that henceforth the United
States may exercise the sovereign rights and jurisdiction derived from
exclusive economic zone jurisdiction that Russia would otherwise be en-
titled to exercise under international law in the absence of the agreement
of the Parties on the maritime boundary.
3. to the extent that either Party exercises the sovereign rights or
jurisdiction in the special area or areas on its side of the maritime boun-
dary as provided for in this Article, such exercise of sovereign rights or
jurisdiction derives from the agreement of the Parties and does not
constitute an extension of its exclusive economic zone. To this end, each
Party shall take the necessary steps to ensure that any exercise on its
part of such rights or jurisdiction in the special area or areas on its side
of the maritime boundary shall be so characterized in its relevant laws,
regulations, and charts."
 

AUTHORITIES
Hydrography and topography by the National Ocean Service, Coast
Survey, with additional data from the Corps of Engineers, Geological
Survey, U.S. Coast Guard and National Geospatial-Intelligence Agency.


WARNING - PRUDENT MARINER
The prudent mariner will not rely solely on
any single aid to navigation, particularly on
floating aids.  See U.S. Coast Guard Light List
and U.S. Coast Pilot for details.


TIDAL INFORMATION
For tidal information see the NOS tide table publication or go to http://co-
ops.nos.noaa.gov


ADDITIONAL INFORMATION
Additional information can be obtained at www.nauticalcharts.noaa.gov


CAUTION - LIMITATIONS
Limitations on the use of radio signals as
aids to marine navigation can be found in the
U.S. Coast Guard Light Lists and National
Geospatial-Intelligence Agency Publication 117.
Radio direction-finder bearings to commercial
broadcasting stations are subject to error and
should be used with caution.
 

END OF FILE
