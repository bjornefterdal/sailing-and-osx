# Seilbåt og OSX

Clone repo - installer OpenCPN
Innstillinger i OpenCPN, pek kartmappe til downloads folder i repo.

## Kartplotter

OpenCPN
http://opencpn.org/


## Kart

Pilot Charts:
http://opencpn.org/ocpn/pilotcharts

High resolution world maps GSHHS
http://www.zygrib.org/getfile.php?file=zyGrib_maps2.4_mac.dmg

## Værfax

zyGrib - GRIB File Viewer 
http://www.zygrib.org/#section_mac_intel

###Informasjon
http://www.blackcatsystems.com/software/multimode/fax.html

##NMEA til USB
http://www.amazon.com/Digital-Yacht-Adapter-Cable-Software/dp/B00J62CNK8